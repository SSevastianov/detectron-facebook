from detectron2.engine import DefaultPredictor

import argparse
import logging
import os
from collections import OrderedDict
import torch
from fvcore.common.file_io import PathManager
from fvcore.nn.precise_bn import get_bn_modules
from torch.nn.parallel import DistributedDataParallel
import time

import detectron2.data.transforms as T
from detectron2.checkpoint import DetectionCheckpointer
from detectron2.data import (
    MetadataCatalog,
    build_detection_test_loader,
    build_detection_train_loader,
)
from detectron2.evaluation import (
    DatasetEvaluator,
    inference_on_dataset,
    print_csv_format,
    verify_results,
)
from detectron2.modeling import build_model
from detectron2.solver import build_lr_scheduler, build_optimizer
from detectron2.utils import comm
from detectron2.utils.collect_env import collect_env_info
from detectron2.utils.env import seed_all_rng
from detectron2.utils.events import CommonMetricPrinter, JSONWriter, TensorboardXWriter
from detectron2.utils.logger import setup_logger

class YoadPredictor:
	"""
	Create a simple end-to-end predictor with the given config.
	The predictor takes an BGR image, resizes it to the specified resolution,
	runs the model and produces a dict of predictions.

	Attributes:
		metadata (Metadata): the metadata of the underlying dataset, obtained from
			cfg.DATASETS.TEST.
	"""

	def __init__(self, cfg):
		self.cfg = cfg.clone()  # cfg can be modified by model
		self.model = build_model(self.cfg)
		self.model.eval()
		self.metadata = MetadataCatalog.get(cfg.DATASETS.TEST[0])

		checkpointer = DetectionCheckpointer(self.model)
		checkpointer.load(cfg.MODEL.WEIGHTS)

		self.transform_gen = T.ResizeShortestEdge(
			[cfg.INPUT.MIN_SIZE_TEST, cfg.INPUT.MIN_SIZE_TEST], cfg.INPUT.MAX_SIZE_TEST
		)

		self.input_format = cfg.INPUT.FORMAT
		assert self.input_format in ["RGB", "BGR"], self.input_format

	@torch.no_grad()
	def __call__(self, original_image):
		"""
		Args:
			original_image (np.ndarray): an image of shape (H, W, C) (in BGR order).

		Returns:
			predictions (dict): the output of the model
		"""
		# Apply pre-processing to image.
		if self.input_format == "RGB":
			# whether the model expects BGR inputs or RGB
			original_image = original_image[:, :, ::-1]
		height, width = original_image.shape[:2]
		image = self.transform_gen.get_transform(original_image).apply_image(original_image)
		image = torch.as_tensor(image.astype("float32").transpose(2, 0, 1))

		inputs = {"image": image, "height": height, "width": width}

		s = time.time()
		predictions = self.model([inputs])[0]
		e = time.time()
		print('inference', e-s)
		return predictions