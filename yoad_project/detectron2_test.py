# Some basic setup
# Setup detectron2 logger
from detectron2.utils.logger import setup_logger
setup_logger()

# import some common libraries
import numpy as np
import cv2
import glob
import time
import torch
import random

# import some common detectron2 utilities
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog
import YoadPredictor

torch.cuda.set_device(0)

cfg = get_cfg()
cfg.merge_from_file("/home/work/detectron2_repo/configs/COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.65  # set threshold for this model
# Find a model from detectron2's model zoo. You can either use the https://dl.fbaipublicfiles.... url, or use the following shorthand
cfg.MODEL.WEIGHTS = "detectron2://COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x/137849600/model_final_f10217.pkl"
# predictor = DefaultPredictor(cfg)
predictor = YoadPredictor(cfg)

random.seed(44)
for img_path in imgs_path[:100]: #sample(imgs_path, 1):
	img_base_name = img_path.split('/')[-1]
	im = cv2.imread(img_path)

	start = time.time()
	with torch.no_grad():
		outputs = predictor(im)
	end = time.time()
	print('duration:', end-start)

	# We can use `Visualizer` to draw the predictions on the image.
	v = Visualizer(im[:, :, ::-1], MetadataCatalog.get(cfg.DATASETS.TRAIN[0]), scale=1.2)
	v = v.draw_instance_predictions(outputs["instances"].to("cpu"))
	final_img = v.get_image()[:, :, ::-1]

	final_img = cv2.resize(final_img, (1333, 800))
	# cv2.imwrite('/home/work/output/detectron2/' + img_base_name, final_img)
	print('---'*40)