from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.datasets import make_blobs
from sklearn.preprocessing import StandardScaler
import sklearn
import numpy as np
from sklearn.cluster import DBSCAN
import numpy as np


def pad_traj(traj, length):
    # padd traj with zeros
    if len(traj)>= length:
        return traj
    traj = traj + [[0,0] for i in range(length-len(traj))]
    return traj


def flat_trajes(xx):
    def flatten(i):
        ll = []
        [[ll.append(d) for d in t] for t in i]
        return ll

    return np.array([flatten(i) for i in xx])


def preprocess_trajs(trajes):
    '''
    build input for dbscan from row data (list of unequal size trajes).
    :param trajes: is a list of trajes
    :return: input to DBScan.
    '''
    max_value = max(map(lambda x: len(x), trajes))
    pad_trajes = [pad_traj(trj, max_value) for trj in trajes]
    input_dbscan = flat_trajes(pad_trajes)
    return input_dbscan

def unflatten(yy):
    ll = []
    for i in range(0, len(yy), 2):
        ll.append([yy[i], yy[i+1]])
    return ll

def opp_transform(xx):
    '''
    opposite input_db_scan tranjes to raw tranjes.
    :param xx:
    :return:
    '''
    return np.array([unflatten(i) for i in xx])


def diff_metric(a, b):
    '''
    differences Metric of two (equal/unequal size) tranjes.
    :param a: first tranjes.
    :param b: second tranjes.
    :return: distance value.
    '''
    a = unflatten(a)
    b = unflatten(b)
    d = 0
    for coords1, coords2 in zip(a, b):
        d += abs(coords1[0] - coords2[0]) + abs(coords1[1] - coords2[1])
    print('distance', d)
    return d


row_trajes = np.array([[[1,1], [2,2], [3,3]],
              [[1.2,1.2], [2.1,1.9], [3.3,3]],
              [[4,4], [5,5], [6,6]],
              [[9,9], [8,8], [7,7]]
                ])
input_dbscan = preprocess_trajs(row_trajes)
zz = opp_transform(input_dbscan)
labels_true = np.array([0, 1, 1,1])
# diff_metric(uu[0], uu[1])

db = DBSCAN(eps=2, metric=diff_metric, min_samples=2).fit(input_dbscan)


# print("Silhouette Coefficient: %0.3f"
#       % metrics.silhouette_score(X, labels))

# #############################################################################
# Plot result
def draw(row_trajes, db):
    # region draw clusters
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_

    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)

    print('Estimated number of clusters: %d' % n_clusters_)
    print('Estimated number of noise points: %d' % n_noise_)
    print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels_true, labels))
    print("Completeness: %0.3f" % metrics.completeness_score(labels_true, labels))
    print("V-measure: %0.3f" % metrics.v_measure_score(labels_true, labels))
    print("Adjusted Rand Index: %0.3f"
          % metrics.adjusted_rand_score(labels_true, labels))
    print("Adjusted Mutual Information: %0.3f"
          % metrics.adjusted_mutual_info_score(labels_true, labels))

    import matplotlib.pyplot as plt

    # Black removed and is used for noise instead.
    unique_labels = set(labels)
    colors = [plt.cm.Spectral(each)
              for each in np.linspace(0, 1, len(unique_labels))]
    colors = ['b','r','g']
    for k, col in zip(unique_labels, colors):
        if k == -1:
            # Black used for noise.
            col = [0, 0, 0, 1]

        class_member_mask = (labels == k)

        xy = row_trajes[class_member_mask & core_samples_mask]
        for traj in xy:
            num_of_pts = len(traj)
            x = [x[0] for x in traj]
            y = [x[1] for x in traj]

            plt.plot(x, y, 'g', markerfacecolor=tuple(col),
                     markeredgecolor='k', markersize=6)

        xy = row_trajes[class_member_mask & ~core_samples_mask]
        for traj in xy:
            num_of_pts = len(traj)
            x = [x[0] for x in traj]
            y = [x[1] for x in traj]

            plt.plot(x, y, 'b', markerfacecolor=tuple(col),
                     markeredgecolor='k', markersize=6)

    plt.title('Estimated number of clusters: %d' % n_clusters_)
    plt.savefig('/home/work/plot.jpg')
    plt.show()

# endregion draw clusters
