from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
from tensorflow.python.platform import gfile
from data_tools.PredictionsTools.pred_tools import PredTools
from PIL import Image, ImageDraw
import pandas as pd
import numpy as np
import os


class PredToolsTest(tf.test.TestCase):
    """ Unit test for PredTools class. """

    def setUp(self):
        """Will run before each test. """

        # Constants:
        self.num_batches = 5
        self.batch_size = 2
        self.labels = [0, 4, 3, 7, 2, 3, 3, 4, 0, 2]
        self.predictions = [1, 4, 3, 5, 2, 6, 8, 5, 0, 9]
        self.some_numbers = [1.4, 6.3, 5, 1.2, 3, 6, 1, 8.3, 2.1, 42]

        temp_dir = None  # Leave None to get an automatic temp dir
        self.facets_dir_path = '/home/razor16/Projects/data_tools/facets'
        self.open_notebook = False  # Whether to open jupyther notebook directly from python code

        tf.reset_default_graph()

        self.batch_dict = self.create_batch_dict(labels=self.labels,
                                                 predictions=self.predictions,
                                                 some_numbers=self.some_numbers,
                                                 images_batch=self.create_images_batch(self.batch_size),
                                                 batch_size=self.batch_size)
        self.temp_dir = self.get_temp_dir() if not temp_dir else temp_dir

    @staticmethod
    def create_images_batch(batch_size):
        img_shape = (200, 180, 3)
        images = np.zeros((batch_size, img_shape[0], img_shape[1], img_shape[2]), dtype=np.uint8)
        for i in range(batch_size):
            img = Image.new('RGB', (img_shape[1], img_shape[0]), color='white')
            d = ImageDraw.Draw(img)
            d.text((10, 10), "Test Me #%d" % i, fill='green')
            images[i, Ellipsis] = np.array(img, dtype=np.uint8)
        return images

    @staticmethod
    def create_batch_dict(labels, predictions, images_batch, some_numbers, batch_size):
        data = (np.asarray(labels), np.asarray(predictions), np.asarray(some_numbers))
        label_batch, predictions_batch, some_numbers_batch = \
            tf.data.Dataset.from_tensor_slices(data).batch(batch_size).make_one_shot_iterator().get_next()

        batch_dict = {
            'label': label_batch,
            'prediction': predictions_batch,
            'some_number': some_numbers_batch,
            'text': tf.constant('some string', dtype=tf.string, shape=(batch_size,)),
            'my_image': tf.constant(images_batch),
        }
        return batch_dict

    def test_create_csv(self):
        print('Inside test_create_csv(). '
              'Temp dir is: %s' % self.temp_dir)

        batch_dict = self.batch_dict
        batch_dict.pop('my_image')
        fields = list(batch_dict.keys())

        pt = PredTools(save_dir_path=self.temp_dir,
                       fields=fields,
                       create_images=False,
                       create_csv=True,
                       create_statistics_graphs=False,
                       gt_label_field_name='label',
                       prediction_field_name='prediction')

        pt.set_csv_params(csv_file_name='my_predictions.csv')
        aggr_op = pt.aggregate_predictions_op(batch_dict)

        with self.test_session() as sess:
            for _ in range(self.num_batches):
                _ = sess.run([aggr_op])

            pt.save_results()
            self.assertTrue(gfile.Exists(pt.csv_path))

            with open(pt.csv_path, mode='rb') as f:
                df_from_csv = pd.read_csv(f)
            print('CSV data:\n', df_from_csv)
            self.assertAllEqual(self.labels, df_from_csv['label'])
            self.assertAllEqual(self.predictions, df_from_csv['prediction'])
            self.assertAllEqual(self.some_numbers, df_from_csv['some_number'])

    def test_create_csv_fields_order(self):
        print('Inside test_create_csv_fields_order(). '
              'Temp dir is: %s' % self.temp_dir)

        batch_dict = self.batch_dict
        batch_dict.pop('my_image')
        fields = ['prediction', 'label', 'text', 'some_number']

        pt = PredTools(save_dir_path=self.temp_dir,
                       fields=fields,
                       create_images=False,
                       create_csv=True,
                       create_statistics_graphs=False,
                       gt_label_field_name='label',
                       prediction_field_name='prediction')

        pt.set_csv_params(csv_file_name='my_predictions2.csv')
        aggr_op = pt.aggregate_predictions_op(batch_dict)

        with self.test_session() as sess:
            for _ in range(self.num_batches):
                _ = sess.run([aggr_op])

            pt.save_results()
            self.assertTrue(gfile.Exists(pt.csv_path))

            with open(pt.csv_path, mode='rb') as f:
                df_from_csv = pd.read_csv(f)
            print('CSV data:\n', df_from_csv)
            self.assertAllEqual(self.labels, df_from_csv['label'])
            self.assertAllEqual(self.predictions, df_from_csv['prediction'])
            self.assertAllEqual(self.some_numbers, df_from_csv['some_number'])
            self.assertAllEqual(fields, df_from_csv.columns.tolist())

    def test_create_csv_batch_keys_mismatch(self):
        print('Inside test_create_csv_batch_keys_mismatch(). '
              'Temp dir is: %s' % self.temp_dir)

        batch_dict = self.batch_dict
        batch_dict.pop('my_image')
        fields = ['prediction', 'label', 'some_number']  # 'text' field is missing here.

        pt = PredTools(save_dir_path=self.temp_dir,
                       fields=fields,
                       create_images=False,
                       create_csv=True,
                       create_statistics_graphs=False,
                       gt_label_field_name='label',
                       prediction_field_name='prediction')

        pt.set_csv_params(csv_file_name='my_predictions2.csv')

        with self.assertRaises(ValueError) as context:
            _ = pt.aggregate_predictions_op(batch_dict)

        self.assertTrue('batch_dict keys do not match columns names defined for PredTools'
                        in str(context.exception))

    def test_create_statistics_graphs(self):
        print('Inside test_create_statistics_graphs(). '
              'Temp dir is: %s' % self.temp_dir)

        batch_dict = self.batch_dict
        batch_dict.pop('my_image')
        fields = list(batch_dict.keys())

        pt = PredTools(save_dir_path=self.temp_dir,
                       fields=fields,
                       create_images=False,
                       create_csv=False,
                       create_statistics_graphs=True,
                       gt_label_field_name='label',
                       prediction_field_name='prediction')

        pt.set_csv_params(csv_file_name='my_predictions3.csv',
                          open_notebook=self.open_notebook,
                          facets_dir_path=self.facets_dir_path)
        aggr_op = pt.aggregate_predictions_op(batch_dict)

        with self.test_session() as sess:
            for _ in range(self.num_batches):
                _ = sess.run([aggr_op])

            pt.save_results()
            self.assertTrue(gfile.Exists(pt.csv_path))

            with open(pt.csv_path, mode='rb') as f:
                df_from_csv = pd.read_csv(f)
            print('CSV data:\n', df_from_csv)
            self.assertAllEqual(self.labels, df_from_csv['label'])
            self.assertAllEqual(self.predictions, df_from_csv['prediction'])
            self.assertAllEqual(self.some_numbers, df_from_csv['some_number'])

    def test_create_images(self):
        print('Inside test_create_images(). '
              'Temp dir is: %s' % self.temp_dir)

        batch_dict = self.batch_dict
        fields = list(batch_dict.keys())

        pt = PredTools(save_dir_path=self.temp_dir,
                       fields=fields,
                       create_images=True,
                       create_csv=False,
                       create_statistics_graphs=False,
                       gt_label_field_name='label',
                       prediction_field_name='prediction')

        img_field_name = 'my_image'
        imgs_save_dir_name = 'predictions_images'
        imgs_dir_name_format = 'images_gt%d-pred%d'
        img_name_format = '%s_%.1f'
        img_name_format_keys = ['text', 'some_number']
        pt.set_images_params(img_field_name=img_field_name,
                             imgs_save_dir_name=imgs_save_dir_name,
                             imgs_dir_name_format=imgs_dir_name_format,
                             img_name_format=img_name_format,
                             img_name_format_keys=img_name_format_keys)
        aggr_op = pt.aggregate_predictions_op(batch_dict)

        with self.test_session() as sess:
            for _ in range(self.num_batches):
                _ = sess.run([aggr_op])

            pt.save_results()

            for i in range(self.batch_size * self.num_batches):
                current_img_name = 'im_%06d_' % i + img_name_format % ('some string', self.some_numbers[i]) + '.jpg'
                img_path = os.path.join(self.temp_dir,
                                        imgs_save_dir_name,
                                        imgs_dir_name_format % (self.labels[i], self.predictions[i]),
                                        current_img_name)
                self.assertTrue(gfile.Exists(img_path))

    def test_create_images_2(self):
        """Check running without errors. """
        print('Inside test_create_images_2(). '
              'Temp dir is: %s' % self.temp_dir)

        batch_dict = {
            'label': tf.constant([2, 4]),
            'prediction': tf.constant([1, 0]),
            'image': tf.ones((2, 100, 100, 3), dtype=np.uint8),
        }

        pt = PredTools(save_dir_path=self.temp_dir,
                       create_images=True,
                       create_csv=True,
                       create_statistics_graphs=False,
                       fields=list(batch_dict.keys()),
                       gt_label_field_name='label',
                       prediction_field_name='prediction')

        pt.set_images_params(img_name_format='gt%d_pred%d',
                             img_name_format_keys=['label', 'prediction'])
        pt.set_csv_params()
        aggr_op = pt.aggregate_predictions_op(batch_dict)
        with tf.Session() as sess:
            for _ in range(2):
                _ = sess.run(aggr_op)
        pt.save_results()

    def test_create_csv_and_resized_images(self):
        print('Inside test_create_csv_and_resized_images(). '
              'Temp dir is: %s' % self.temp_dir)

        batch_dict = self.batch_dict
        fields = list(batch_dict.keys())

        pt = PredTools(save_dir_path=self.temp_dir,
                       fields=fields,
                       create_images=True,
                       create_csv=True,
                       create_statistics_graphs=False,
                       gt_label_field_name='label',
                       prediction_field_name='prediction')

        img_field_name = 'my_image'
        imgs_save_dir_name = 'predictions_images'
        imgs_dir_name_format = 'images_gt%d-pred%d'
        img_name_format = '%s_%.1f'
        img_name_format_keys = ['text', 'some_number']
        pt.set_images_params(img_field_name=img_field_name,
                             imgs_save_dir_name=imgs_save_dir_name,
                             imgs_dir_name_format=imgs_dir_name_format,
                             img_name_format=img_name_format,
                             img_name_format_keys=img_name_format_keys,
                             img_resize_factor=0.25)
        pt.set_csv_params()

        aggr_op = pt.aggregate_predictions_op(batch_dict)

        with self.test_session() as sess:
            for _ in range(self.num_batches):
                _ = sess.run([aggr_op])

            pt.save_results()

            # Check CSV:
            self.assertTrue(gfile.Exists(pt.csv_path))
            with open(pt.csv_path, mode='rb') as f:
                df_from_csv = pd.read_csv(f)
            print('CSV data:\n', df_from_csv)
            self.assertAllEqual(self.labels, df_from_csv['label'])
            self.assertAllEqual(self.predictions, df_from_csv['prediction'])
            self.assertAllEqual(self.some_numbers, df_from_csv['some_number'])

            # Check created images:
            for i in range(self.batch_size * self.num_batches):
                current_img_name = 'im_%06d_' % i + img_name_format % ('some string', self.some_numbers[i]) + '.jpg'
                img_path = os.path.join(self.temp_dir,
                                        imgs_save_dir_name,
                                        imgs_dir_name_format % (self.labels[i], self.predictions[i]),
                                        current_img_name)
                self.assertTrue(gfile.Exists(img_path))
                img = Image.open(img_path)
                self.assertAllEqual(img.size, (45, 50))


if __name__ == '__main__':
    tf.test.main()
