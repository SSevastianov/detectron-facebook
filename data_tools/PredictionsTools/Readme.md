# Predictions Tools
This code contain tools for saving and analyzing prediction results.

The main use-case is adding this code to evaluation scripts (examples below), which run batches of data through a prediction network. 
Then, the batch data can be easily aggregated and saved as a csv file, analyzed with statistics graphs (using Google Facets), and images can be saved in a convenient and organized form for further inspection.

## Features
- Create a CSV file with predictions, ground truths and other custom fields directly from batch tensors.
- Save tensor images as files with custom names (including selected fields),
    splitting to sub-directories according to ground-truth and predictions. Useful for analyzing predictions.
- Create statistics graphs from non-visual data (e.g. examples meta-data) according to ground-truth and prediction combinations.

## Installation

**Requirements:**
```
tensorflow-gpu
numpy
pandas
jupyter
matplotlib
pillow
lxml
tornado==5.1.1
```
**Tested on:**
* Ubuntu 16.04
* python 2.7 and 3.5
* tensorflow-gpu version 1.12

Note: Opening statistics graphs notebook directly from code is currently supported only for python 3.

## Running
Before running your own code, make sure you can run the unit tests file (pred_tools_test.py) without errors.


**Example 1**:
```python
batch_dict = {
    'label': batch_labels_tensor,
    'prediction': batch_predictions_tensor,
    'image': batch_images_tensor,
}

pt = PredTools(save_dir_path='enter_your_dir_path',
               create_images=True,
               create_csv=True,
               create_statistics_graphs=False,
               fields=list(batch_dict.keys()),
               gt_label_field_name='label',
               prediction_field_name='prediction')

pt.set_images_params(img_name_format='%s',
                     img_name_format_keys=['label'])
pt.set_csv_params()
aggr_op = pt.aggregate_predictions_op(batch_dict)
with tf.Session() as sess:
    for _ in range(num_batches):
        _ = sess.run(aggr_op)
pt.save_results()  # Only here csv file is saved.
```

**Example 2** - using with tf.Slim (edit eval_image_classifier.py):
```python
...

predictions = tf.argmax(logits, 1)
labels = tf.squeeze(labels)

# Add the code below:
batch_dict = {
    'gt_label': labels,
    'prediction': predictions,
    'image': images,
}

pt = PredTools(save_dir_path='enter_your_dir_path',
               create_images=True,
               create_csv=True,
               create_statistics_graphs=False,
               fields=list(batch_dict.keys())) # use default field names
               
pt.set_images_params(img_name_format='gt%d_pred%d',
                     img_name_format_keys=['gt_label', 'prediction'])
pt.set_csv_params(facets_dir_path='path_to_your_google_facets_main_dir',
                  open_notebook=True)
aggr_op = pt.aggregate_predictions_op(batch_dict)

...

slim.evaluation.evaluate_once(
    master=FLAGS.master,
    checkpoint_path=checkpoint_path,
    logdir=FLAGS.eval_dir,
    num_evals=num_batches,
    eval_op=list(names_to_updates.values()) + [aggr_op],  # Add aggregation op.
    variables_to_restore=variables_to_restore)

# Add after running all batches:
pt.save_results()
```

## Troubleshooting
Be sure to define your images in the appropriate format. 
Specifically, images of type `tf.float` are only allowed to have one channel (as noted in the docstrings). 
This is due to limitations of PIL package.
If you want to use float RGB images, you might convert them to uint8, while applying the correct affine transormation.
For example, if the images are float in the range [0,1], the following code will work:
```python

images_for_prediction_tool = tf.cast(images * 255, dtype=tf.uint8)

batch_dict = {
    'gt_label': labels,
    'prediction': predictions,
    'image': images_for_prediction_tool,
}
```

## Demo results
Demonstration here was taken from the unit tests.

An example of a generated csv file with fields of different types:

![Predictions csv example](doc/predictions_csv_example.png "Predictions csv example")


An example of a generated statistics graph (other type of graphs are supported as well via Facets):

![Statistics graph example](doc/statistics_graph_example.png "Statistics graph example")

An example of generated images with ground truth and prediction combinations:

![Saved images example](doc/saved_images_example.png "Saved images example")
