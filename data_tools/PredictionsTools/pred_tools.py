from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import pandas as pd
import tensorflow as tf
import os
from PIL import Image
import subprocess
import sys


class PredTools(object):
    """A util class for analyzing and saving predictions using csv, images and statistics graphs. """

    def __init__(self,
                 save_dir_path,
                 create_images=True,
                 create_csv=True,
                 create_statistics_graphs=True,
                 fields=None,
                 gt_label_field_name='gt_label',
                 prediction_field_name='prediction',
                 name='PredTools'):
        """Create a PredTools instance.

        Features:
            - Create a CSV file with predictions, ground truths and other custom fields from tensors.
            - Save tensor images as files with custom names,
                splitting to sub-directories according to gt and predictions.
            - Create statistics graphs from non-visual data.

        Args:
            save_dir_path (str): dir path where results will be saved to.
            fields(list): a list of strings depicting names of columns to aggregate
                (the same that will be passed to aggregate_predictions_op()).
                Notice that order can be arbitrary (will define the columns order of the output csv).
            gt_label_field_name(string): the field name for holding Ground-Truth label tensor.
            prediction_field_name(string): the field name for holding predictions tensor.
            create_images: True/False - whether to save images as ground truth and prediction combinations.
            create_csv: True/False - whether to create a csv file.
            create_statistics_graphs: True/False - whether to create data for generating statistics graphs.
            name: a string which will be used as name scope for class's ops.
        """
        # Define uninitialized image params:
        # (must be configured by calling `self.set_images_params`).
        self._img_field_name = None
        self._imgs_save_dir_name = None
        self._imgs_dir_name_format = None
        self._img_name_format = None
        self._img_name_format_keys = None
        self._img_extension = None
        self._images_resize = None
        self._img_resize_method = None
        self._img_resize_factor = None
        self._imgs_save_dir_path = None

        # Define name for class:
        self._name = name

        # Define main dir path for saving results:
        self._save_dir_path = save_dir_path

        # Define images saving configuration:
        self._create_images = create_images
        self._image_counter = 0

        # Define csv and statistics configuration.
        # (None parameters must be configured by calling `self.set_csv_params`).
        self._create_csv = create_csv
        self._create_statistics_graphs = create_statistics_graphs
        self._csv_file_name = None
        self._facets_dir_path = None
        self._open_terminal_with_jupyther_notebook = None

        # Define fields and data:
        self._gt_label_field_name = gt_label_field_name
        self._prediction_field_name = prediction_field_name
        self._unsorted_fields = fields
        self._fields = sorted(fields)
        self._data_list = []
        self._data_frame = None

        # Create dir for results (if doesn't exist):
        tf.gfile.MakeDirs(self._save_dir_path)

    def set_images_params(self,
                          img_field_name='image',
                          imgs_save_dir_name='predictions_images',
                          imgs_dir_name_format='images_gt%d-pred%d',
                          img_name_format='',
                          img_name_format_keys=None,
                          img_extension='.jpg',
                          images_resize=False,
                          img_resize_method=Image.ANTIALIAS,
                          img_resize_factor=1.0,
                          ):
        """

        Args:
            img_field_name: the field name for holding images tensor.
            imgs_save_dir_name: a string depicting a subdir name for saving all the images dirs.
            imgs_dir_name_format: a string depicting the format for dirs that will be created inside imgs_save_dir_name.
            img_name_format: a string defining a format for names of images.
            img_name_format_keys: a list of column names corresponding img_name_format.
            img_extension: a string depicting the extension of image file (e.g. '.jpg').
            images_resize: True/False - whether to re-size image before saving.
            img_resize_method: PIL's image resize method for images. Could be change to NEAREST for faster
                and less accurate down-sampling (relative to Image.ANTIALIAS).
            img_resize_factor: a float, resize factor (usually <= 1.0) for both dimensions of images.

        Returns:

        """
        self._img_field_name = img_field_name
        self._img_name_format = img_name_format
        self._img_name_format_keys = img_name_format_keys
        self._img_extension = img_extension
        self._imgs_save_dir_name = imgs_save_dir_name
        self._imgs_dir_name_format = imgs_dir_name_format
        self._images_resize = images_resize
        self._img_resize_method = img_resize_method
        self._img_resize_factor = img_resize_factor

        if self._img_field_name not in self._fields:
            raise ValueError('img_field_name is not defined in fields.')
        self._remove_image_field(self._unsorted_fields)

        if self._create_images:
            self._imgs_save_dir_path = os.path.join(self._save_dir_path, self._imgs_save_dir_name)
            tf.gfile.MakeDirs(self._imgs_save_dir_path)

    def set_csv_params(self,
                       csv_file_name='predictions.csv',
                       facets_dir_path=None,
                       open_notebook=False,
                       ):
        """Set parameters related to CSV and statistics graphs.

        Args:
            csv_file_name(str): a string, file name for csv.
            facets_dir_path(str): dir path of where Google's `facets` main directory is located.
            open_notebook(bool): whether to open terminal and run jupyther notebook with statistics graphs.

        """
        self._csv_file_name = csv_file_name
        self._facets_dir_path = facets_dir_path
        self._open_terminal_with_jupyther_notebook = open_notebook

    def _remove_image_field(self, fields):
        """ Remove (in-place) image field. """
        if self._img_field_name is None:
            return
        if isinstance(fields, dict):
            fields.pop(self._img_field_name)
        elif isinstance(fields, list):
            fields.remove(self._img_field_name)

    def aggregate_predictions_op(self, batch_tensor_dict):
        """An op to aggregate prediction tensors to `_data_matrix` and save images (if enabled).

        **Note**: For `images` tensor, if dtype is `tf.float`, it should be of shape (batch_size, height, width).

        Args:
            batch_tensor_dict (dict): a dictionary, where keys are tensor names
                and values are tensor values to aggregate.
                Mandatory fields must exist in dictionary (except 'image' field), as defined in mandatory_cols_names.

        Returns:
            tensorflow.python.framework.ops.Operation: an op for aggregating predictions.

        Raises:
            ValueError: if set_images_params() and/or set_csv_params()
                were not called before using this function, or if batch_dict keys do not match init()'s fields.

        """

        # Check if parameters were configured properly:
        if self._create_images and (not self._img_field_name):
            raise ValueError('set_images_params() must be called before calling aggregate_predictions_op().')
        if (self._create_csv or self._create_statistics_graphs) and (not self._csv_file_name):
            raise ValueError('set_csv_params() must be called before calling aggregate_predictions_op().')

        # Check consistency for current batch's keys.
        if set(batch_tensor_dict) != set(self._fields):
            raise ValueError('batch_dict keys do not match columns names defined for PredTools '
                             '(%s != %s)' % (repr(set(batch_tensor_dict.keys())), repr(set(self._fields))))

        with tf.name_scope(self._name):
            result = []
            if self._create_csv or self._create_images or self._create_statistics_graphs:
                # Create a list to the deliver as input of py_func.
                # Sorting batch dict by keys is needed for order consistency.
                inp = [batch_tensor_dict[key] for key in sorted(batch_tensor_dict.keys())]
                result = tf.py_func(func=self._aggregate_predictions_py_func,
                                    inp=inp,
                                    Tout=tf.bool,
                                    stateful=True,
                                    name='aggregate_predictions_py_func')
            return tf.group(result, name='aggregate_predictions_op')

    @staticmethod
    def _fix_numpy_strings(np_array_list):
        """Convert ndarray of bytes type to string.
        Tensorflow returns ndarray bytes when running tf.string tensors. """
        return [arr.astype(str) if arr.dtype.kind in {'S', 'O'} else arr for arr in np_array_list]

    def _aggregate_predictions_py_func(self, *args):
        """Py_func for aggregate_predictions.

        Args:
            *args: tensors args sorted by their name.

        Returns:
            True if successful.

        """

        _args = self._fix_numpy_strings(args)
        batch_np_dict = dict(zip(self._fields, _args))

        if self._create_images:
            images = batch_np_dict[self._img_field_name]
            for i in range(images.shape[0]):
                # Define image name:
                img_name = 'im_%06d_' % self._image_counter
                if self._img_name_format_keys:
                    img_name += self._img_name_format % tuple(batch_np_dict[k][i] for k in self._img_name_format_keys)
                img_name += self._img_extension

                # Define (and create) image dir path:
                img_subdir_path = os.path.join(
                    self._imgs_save_dir_path,
                    self._imgs_dir_name_format % (batch_np_dict[self._gt_label_field_name][i],
                                                  batch_np_dict[self._prediction_field_name][i])
                )
                tf.gfile.MakeDirs(img_subdir_path)

                # Save image:
                img_save_path = os.path.join(img_subdir_path, img_name)
                try:
                    img = Image.fromarray(images[i, Ellipsis])
                    if self._img_resize_factor != 1.0:
                        img = img.resize(size=(int(img.size[0] * self._img_resize_factor),
                                               int(img.size[1] * self._img_resize_factor)),
                                         resample=self._img_resize_method)
                    img.save(fp=img_save_path)  # TODO(O): add the ability to pass quality and optimize params.
                except IOError as e:
                    print("Cannot create image for '%s'" % img_save_path)
                    raise e
                self._image_counter += 1

        if self._create_csv or self._create_statistics_graphs:
            self._remove_image_field(batch_np_dict)
            batch_df = pd.DataFrame(data=batch_np_dict)
            # Aggregate to list for efficiency (see pd.append() notes):
            self._data_list.append(batch_df)

        return True

    @staticmethod
    def _list_to_data_frame(data_list):
        # Create a pd.DataFrame from the list of batches:
        return pd.concat(data_list, ignore_index=True, sort=True)

    def save_results(self):
        """Save results:

        - create csv file and write to it (if enabled)
        - create files for statistics graphs (if enabled)

        Raises:
            ValueError: if aggregate_predictions_op() was not called before.
        """
        if self._create_csv or self._create_statistics_graphs:
            self._data_frame = self._list_to_data_frame(self._data_list)

            # Re-order columns according to the order defined in _fields:
            self._data_frame = self._data_frame[self._unsorted_fields]

            # Save data matrix as csv file:
            with open(self.csv_path, 'w') as f:
                self._data_frame.to_csv(f, header=True, index=False)

            if self._create_statistics_graphs:
                self.create_statistics_proto(self._facets_dir_path)

    def create_statistics_proto(self,
                                facets_dir_path,
                                facets_overview_dir_name='facets_overview',
                                save_stats_proto_path='statistics.proto',
                                predictions_tools_notebook_path='statistics_graphs_visualization.ipynb'):
        # Define python path and relevant imports:
        sys.path.append(os.path.join(facets_dir_path, facets_overview_dir_name, 'python'))
        from generic_feature_statistics_generator import GenericFeatureStatisticsGenerator
        import base64

        grouped = self._data_frame.groupby([self._gt_label_field_name, self._prediction_field_name])

        # Calculate the feature statistics proto from the datasets and stringify it for use in facets overview
        gfsg = GenericFeatureStatisticsGenerator()
        proto = gfsg.ProtoFromDataFrames(
            [{'name': 'GT=%s, Prediction=%s' % name, 'table': group} for name, group in grouped]
        )
        protostr = base64.b64encode(proto.SerializeToString()).decode("utf-8")
        with open(save_stats_proto_path, "w") as f:
            f.write(protostr)

        if self._open_terminal_with_jupyther_notebook:
            subprocess.Popen(
                "gnome-terminal -x sh -c 'jupyter notebook --NotebookApp.iopub_data_rate_limit=10000000 %s; exec bash'"
                % os.path.abspath(predictions_tools_notebook_path),
                stdout=subprocess.PIPE,
                stderr=None,
                shell=True,
            )

    @property
    def csv_path(self):
        """
        Returns:
            (str): generated csv file path.
        """
        return os.path.join(self._save_dir_path, self._csv_file_name)

    @property
    def current_data_frame(self):
        """
        Returns:
            (pd.DataFrame): Current DataFrame with all batches aggregated so far.
        """
        return self._list_to_data_frame(self._data_list)
