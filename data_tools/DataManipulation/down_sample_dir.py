import os
from shutil import copyfile
from os.path import join as pjoin
import data_tools.DataManipulation.common_funcs as cf
import re


# ratio is the number of pics to skip when down sampling
def down_sample_dir(in_dir_path, out_dir_path, ratio=12,log=False, log_file_name='removed_files.txt'):
    img_files_list = [fname for fname in os.listdir(in_dir_path) if cf.is_pic(pjoin(in_dir_path, fname))]
    if not img_files_list:
        # empty list
        return

    # for sorting by image number
    def get_num(pic_name):
        num_list = re.findall(r'\d+', pic_name)
        return '%09d' % (int(num_list[-1]))

    img_files_list = sorted(img_files_list, key=get_num)

    if out_dir_path == in_dir_path:
        if log:
            log_file = open(pjoin(out_dir_path, log_file_name), 'w')
        num_removed = 0
        # copy inplace
        for ind, fname in enumerate(img_files_list):
            if ind % ratio != 0:
                os.remove(os.path.join(in_dir_path, fname))
                num_removed += 1
                if log:
                    # write the removed file name in the log file
                    log_file.write(fname + '\n')
        if log:
            log_file.write('total number of removed files: %d' % num_removed)
            log_file.close()

    else:
        # copy to another place
        for ind, fname in enumerate(img_files_list):
            if ind % ratio == 0:
                copyfile(pjoin(in_dir_path, fname), pjoin(out_dir_path, fname))


if __name__ == '__main__':
    data_path = '/hdd/istar_data/horizontal_payload_data/ds1.5/1k/Negev-Monument/noon/3000'
    out_path = '/hdd/qa_rita/data/a/carmel'
    log_file_name = 'removed_files.txt'
    # down_sample_dir(data_path, data_path, ratio=12)
    # cf.manipulate_files(data_path, out_path, manipulate_dir_funcs_list=[down_sample_dir])
    cf.manipulate_files(data_path, data_path, manipulate_dir_funcs_list=[down_sample_dir])
