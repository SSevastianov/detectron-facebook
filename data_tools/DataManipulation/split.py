''' Spliting video into images
'''
# TODO: add documentation

import os
import traceback
import data_tools.DataManipulation.common_funcs as cf
import imageio
import shutil
import data_tools.utils.img_util as iu

def create_frames(src_dir, dst_dir, fps, dst_width=None, dst_height=None):
    # every second
    for root, dirs, files in os.walk(src_dir):
        for file in files:
            if cf.is_video(os.path.join(root, file)):
                try:
                    vid_name = os.path.basename(file)
                    print("%s start" % vid_name)
                    file_path = os.path.join(root, file)
                    rel_path = os.path.relpath(file_path, src_dir)
                    dst_dir_path = os.path.join(dst_dir,
                                                os.path.splitext(rel_path)[0])  # TODO: change this to be the diff
                    create_frames_for_vid(file_path, dst_dir_path, dst_width, dst_height, fps)
                    print("%s end" % vid_name)
                except:
                    print(traceback.format_exc())


def create_frames_for_vid(src_vid, dst_dir_path, dst_width=None, dst_height=None, fps=1):
    # in case you want all the frames from the video, fps should be none
    vid_reader = imageio.get_reader(src_vid, 'ffmpeg')
    # read video length and fps which allow the computation of the overall number of frames
    if fps:
        org_fps = vid_reader.get_meta_data()[
            'fps']
        if org_fps < fps:
            print("Warning: fps param is higher than the original video fps! Extracting all the video frames.")
            fps = org_fps
        vid_fps = int(org_fps * 1. / fps)
    else:
        vid_fps = 1
    vid_name = os.path.basename(src_vid)
    # in the past i named the frames not by the frame id but by the frame saved counter
    # vid_frame_counter = 0
    cf.create_folder(dst_dir_path)
    for frame_id, im_frame in enumerate(vid_reader):
        if frame_id % vid_fps == 0:
            im_write(im_frame, dst_dir_path, vid_name, frame_id, dst_width, dst_height)


def im_write(im_frame, dst_dir, vid_name, frame_id, width, height, num_digits_in_filename=10):
    if width is None or height is None:
        if num_digits_in_filename > 0:
            base_name = '%s_%s' % (vid_name, str(frame_id).zfill(num_digits_in_filename))
        else:
            base_name = '%s_%s' % (vid_name, str(frame_id))

        base_name = base_name.replace('.', '_')
        img_path = os.path.join(dst_dir, '%s.jpg' % base_name)
        imageio.imwrite(img_path, im_frame)
    else:
        patch_id = 0
        for i in range(0, im_frame.shape[0], width):
            for j in range(0, im_frame.shape[1], height):
                base_name = '%s_%s_%s' % (vid_name, str(frame_id).zfill(num_digits_in_filename), str(patch_id).zfill(2))
                base_name = base_name.replace('.', '_')
                img_path = os.path.join(dst_dir, '%s.jpg' % base_name)
                patch_id += 1
                imageio.imwrite(img_path, im_frame[i:i + width, j:j + height])

def separate_gray_and_rgb(src_dir, dst_dir, rgb_postfix='vis', gray_postfix='ir', delete_src=False):
    """
    for each pic folder in src_dir we create 2 folders in dst_dir where 1 contains only rgb pics and the other only gray scale pics.
    this is good for separate channels (vis,ir) automatically
    :param src_dir: parent folder that contains sub folders with pics
    :param dst_dir: the output parent folder that will have 2 folders for each folder in the src_path
    :param rgb_postfix: the postfix of rgb sub folders under dst_dir
    :param gray_postfix: the postfix of gray sub folders under dst_dir
    :param delete_src: do you want to move the file from src to dst or just copy it? true = move, false = copy
    :return: None
    """
    pic_transfer_func = shutil.move if delete_src else shutil.copyfile

    for root, dirs, files in os.walk(src_dir):
        pics = [os.path.join(root, pic) for pic in files if cf.is_pic(os.path.join(root, pic))]
        if len(pics) == 0:
            continue

        # create the rgb and gray sub folders
        dst_path = root.replace(src_dir, dst_dir)
        rgb_folder = dst_path + '_' + rgb_postfix
        gray_folder = dst_path + '_' + gray_postfix
        cf.create_folder(rgb_folder)
        cf.create_folder(gray_folder)

        for pic_src_path in pics:
            if iu.is_grey_scale(pic_src_path):
                # gray scale pic
                pic_dst_path = pic_src_path.replace(root, gray_folder)
            else:
                # rgb pic
                pic_dst_path = pic_src_path.replace(root, rgb_folder)

            # transfer the pic
            pic_transfer_func(pic_src_path, pic_dst_path)
        if len(os.listdir(rgb_folder)) == 0:
            # delete rgb empty folder
            os.rmdir(rgb_folder)
        if len(os.listdir(gray_folder)) == 0:
            # delete gray empty folder
            os.rmdir(gray_folder)
        print('done handling folder: %s' % root)


if __name__ == '__main__':
    create_frames('/hdd/tmp/a',
                  '/hdd/tmp/a_frames',
                  None)

    # separate_gray_and_rgb('/hdd/istar_data/spectro/videos_4_10_frames',
    #                       '/hdd/istar_data/spectro/videos_4_10_frames_split')
