import subprocess
import os

# from imagenet average colors
pad_color_value = [103.939, 116.779, 123.68]


def pad_vid_folder(src_folder, dst_folder, dst_w, dst_h, verbose):
    stderr_val = None
    if not verbose:
        # put all the errors in devnull
        stderr_val = open(os.devnull, 'w')

    assert src_folder != dst_folder

    if not os.path.exists(dst_folder):
        os.makedirs(dst_folder)

    p_list = []

    for file in os.listdir(src_folder):
        src_path = os.path.join(src_folder, file)
        dst_path = os.path.join(dst_folder, file)

        bashCommand = "ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of default=nw=1:nk=1 " + src_path
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()
        if not output:
            continue
        output = output.split('\n')[:2]

        # get the width and height of the input video from video metadata
        src_w, src_h = int(output[0]), int(output[1])
        color = 'black'

        bashCommand = 'ffmpeg -i ' + src_path + ' -vf pad=width=' + str(dst_w) + ':height=' + str(dst_h) + ':x=' + str(
            (dst_w - src_w) / 2) + ':y=' + str((dst_h - src_h) / 2) + ':color=' + color + ' ' + dst_path
        process = subprocess.Popen(bashCommand.split(), stdout=None, stderr=stderr_val)
        p_list.append(process)

    exit_codes = [p.wait() for p in p_list]
    # close file if necessary
    if stderr_val is not None:
        stderr_val.close()
    # the subprocesses exit codes
    print(exit_codes)


if __name__ == '__main__':
    src_folder = '/hdd/istar_data/wise_eyes_data_shahar/videos/for_pad'
    dst_folder = '/hdd/istar_data/wise_eyes_data_shahar/videos/for_pad_out'
    dst_w, dst_h = 5472, 3648
    verbose = True
    pad_vid_folder(src_folder, dst_folder, dst_w, dst_h, verbose)

    # in_path = '/hdd/istar_data/horizontal_payload_data/Carlton_120718/check/000024.jpg'
    # out_path = '/hdd/istar_data/horizontal_payload_data/Carlton_120718/check_out/000024.jpg'
    # dst_w, dst_h = 2000, 4100
    # cf.pad_img(in_path, out_path, dst_h, dst_w)
