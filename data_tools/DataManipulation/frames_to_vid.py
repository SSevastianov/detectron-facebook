import cv2
import os
import data_tools.utils.img_util as iu
import numpy as np
from collections import Counter
from tqdm import tqdm


def create_vid_from_frames_dir(frames_dir, out_vid_path, start=None, end=None, fps=25):
    """
    Create a video from image folder. Image file names must have id number
    :param frames_dir: The image folder
    :param out_vid_path: The video full path
    :param start: Start frame number
    :param end: End frame number
    :param fps: The video fps
    :return: none
    """
    height, width = sample_img_size_from_dir(frames_dir, num_samples=10)
    # Video Writer
    fourcc = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')
    writer = cv2.VideoWriter(out_vid_path, fourcc, fps, (width, height))
    img_paths = [os.path.join(frames_dir, f) for f in os.listdir(frames_dir)]
    img_nums = iu.get_frame_numbers_from_names_list(img_paths)
    # sort
    combined_sorted = [(num, path) for num, path in sorted(zip(img_nums, img_paths), key=lambda pair: pair[0])]
    img_nums, img_paths = zip(*combined_sorted)
    img_nums = np.array(img_nums)
    img_paths = np.array(img_paths)

    if start is None:
        start = 0
    if end is None:
        end = max(img_nums)

    relevant_indexes = np.where((img_nums >= start) & (img_nums <= end))[0]
    for ind in tqdm(relevant_indexes, desc='creating video %s' % os.path.basename(out_vid_path)):
        # insert in the video
        frame = cv2.imread(img_paths[ind])
        # Write frame to out video
        writer.write(frame)

    # close video writer
    writer.release()


def sample_img_size_from_dir(frames_dir, num_samples=10):
    """
    Return predicted image height and width from image folder
    :param frames_dir: Image folder
    :param num_samples: Number of samples from folder to decide the image size from
    :return: Image height and width
    """
    img_list = [os.path.join(frames_dir, f) for f in os.listdir(frames_dir)]
    img_indexes = np.random.choice(len(img_list), num_samples)
    hs = Counter()
    ws = Counter()
    for ind in img_indexes:
        img_path = img_list[ind]
        h, w = iu.get_img_size_and_depth(img_path)[:2]
        hs[h] += 1
        ws[w] += 1
    h = hs.most_common()[0][0]
    w = ws.most_common()[0][0]
    return h, w


if __name__ == '__main__':
    frames_dir = '/media/rocket/Transcend/vert2/32'
    out_vid_path = '/media/rocket/Transcend/vert2/32_2.avi'
    create_vid_from_frames_dir(frames_dir, out_vid_path, start=375, end=460, fps=1)
