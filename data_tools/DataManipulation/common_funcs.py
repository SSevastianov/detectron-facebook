import os
import shutil
from PIL import Image
from collections import defaultdict
from tqdm import tqdm
from multiprocessing import Pool

XML_TYPES = ['xml']

def get_fname_ext(fname):
    """
    Return a file extension in lower case and without the '.'
    :param fname: File name / full path
    :return: File extension
    """
    ext = os.path.splitext(fname)[1].lower()
    if not ext:
        return ext
    return ext[1:] if ext[0] == '.' else ext


def is_pic(path, should_exist=True):
    """
    Check if file is an image according to file extension and if file exists
    :param path: File full path
    :param should_exist: should the file exist or not in the local files
    :return: True if file is an image, False otherwise
    """
    from data_tools.utils.img_util import IMAGE_TYPES
    is_pic_ext = get_fname_ext(path).lower() in IMAGE_TYPES
    is_pic_exist = os.path.isfile(path)
    if not should_exist:
        return is_pic_ext
    return is_pic_exist and is_pic_ext


def is_video(path, should_exist=True):
    """
    Check if file is a video according to file extension
    :param path: File full path
    :return: True if a video, False otherwise
    """
    from data_tools.utils.vid_util import VIDEO_TYPES
    is_vid_ext = get_fname_ext(path).lower() in VIDEO_TYPES
    is_vid_exist = os.path.isfile(path)
    if not should_exist:
        return is_vid_ext
    return is_vid_exist and is_vid_ext


def is_xml(path, should_exist=True):
    """
    Check if file is a xml file according ot file extension
    :param path: File full path
    :return: True if a xml file, False otherwise
    """
    is_xml_ext = get_fname_ext(path).lower() in XML_TYPES
    is_xml_exist = os.path.isfile(path)
    if not should_exist:
        return is_xml_ext
    return is_xml_exist and is_xml_ext

def create_folder(f_path):
    """
    Create new folder if folder doesn't exist
    :param f_path: Folder full path
    :return: none
    """
    if not os.path.exists(f_path):
        os.makedirs(f_path)


def delete_folder(f_path):
    """
    Delete a folder if exists
    :param f_path: Folder full path
    :return: none
    """
    if not os.path.isdir(f_path):
        print('failed to delete folder: %s' % f_path)
        return
    shutil.rmtree(f_path)

def get_data_stats(path, fast=True):
    """
    Get data statistics. collect the data on the image, xml and videos files
    :param path: The root path of the folder tree where all the data is
    :param fast: Flag for fast and less detailed execution of the function
    :return: Stats dictionary
    """
    from data_tools.utils.vid_util import get_vid_stats
    stats = {}
    # all the stats fields
    stats['pic_w_sizes'] = defaultdict(int)
    stats['pic_h_sizes'] = defaultdict(int)
    stats['vid_durations_in_sec'] = defaultdict(int)
    stats['vid_fps'] = defaultdict(int)
    stats['num_pics'] = 0
    stats['num_folders'] = 0
    stats['num_xmls'] = 0
    stats['num_vids'] = 0
    if not fast:
        stats['detailed'] = {}

    for root, dirs, files in tqdm(list(os.walk(path, topdown=True))):
        if not fast:
            stats['detailed'][root] = {'files': {}, 'num_pics': 0, 'num_xmls': 0, 'num_vids': 0}
        for file in files:
            f_path = os.path.join(root, file)
            if is_pic(f_path):
                im = Image.open(f_path)
                w, h = im.size
                stats['pic_w_sizes'][w] += 1
                stats['pic_h_sizes'][h] += 1
                stats['num_pics'] += 1
                if not fast:
                    stats['detailed'][root]['num_pics'] += 1
                    stats['detailed'][root]['files'][file] = {'type': 'pic', 'width': w, 'height': h}
            elif is_video(f_path):
                vid_stats = get_vid_stats(f_path)
                stats['vid_durations_in_sec'][vid_stats['duration']] += 1
                stats['vid_fps'][vid_stats['fps']] += 1
                stats['num_vids'] += 1
                if not fast:
                    stats['detailed'][root]['num_vids'] += 1
                    stats['detailed'][root]['files'][file] = {'type': 'video', 'duration': vid_stats['duration'],
                                                              'fps': vid_stats['fps']}
            elif is_xml(f_path):
                stats['num_xmls'] += 1
                if not fast:
                    stats['detailed'][root]['num_xmls'] += 1
        # update the number of directories
        stats['num_folders'] += len(dirs)
    return stats


def manipulate_files(data_path, out_path, manipulate_file_funcs_list=None, manipulate_dir_funcs_list=None):
    """
    Manipulate files and folders using list of functions. First, will be used the folder functions and then the
    file functions. Each function should get input file/folder and output file/folder all other function's arguments
    should be defaults
    The out_path will keep the same folders tree as the data_path.

    :param data_path: The source directory
    :param out_path: The output directory (can be the same as data_path for inplace operations)
    :param manipulate_file_funcs_list: List of functions which required passed arguments are
    input file path and output file path
    :param manipulate_dir_funcs_list: List of functions which required passed arguments are input folder and
    output folder
    :return: none
    """
    if manipulate_file_funcs_list != None and type(manipulate_file_funcs_list) != list:
        manipulate_file_funcs_list = [manipulate_file_funcs_list]
    if manipulate_dir_funcs_list != None and type(manipulate_dir_funcs_list) != list:
        manipulate_dir_funcs_list = [manipulate_dir_funcs_list]
    inplace = False
    if data_path == out_path:
        inplace = True
    # create the output folder
    create_folder(out_path)
    # check recursive all folders and sub folders
    for root, dirs, files in os.walk(data_path, topdown=True):
        # create all the dirs
        if not inplace:
            for dir in dirs:
                in_dir = os.path.join(root, dir)
                out_dir = in_dir.replace(data_path, out_path)
                create_folder(out_dir)
        if manipulate_dir_funcs_list != None:
            for dir in dirs:
                in_dir = os.path.join(root, dir)
                out_dir = in_dir.replace(data_path, out_path)
                # manipulate the dir
                for func in manipulate_dir_funcs_list:
                    func(in_dir, out_dir)
        if manipulate_file_funcs_list != None:
            for file in files:
                in_file_path = os.path.join(root, file)
                out_file_path = in_file_path.replace(data_path, out_path)
                # manipulate the file
                is_first = True
                for func in manipulate_file_funcs_list:
                    if is_first:
                        is_first = False
                    else:
                        in_file_path = out_file_path
                    func(in_file_path, out_file_path)


def manipulate_files_process_func(data):
    # process function to be used for manipulate_files_parallel
    data_path, out_path, manipulate_file_funcs_list, manipulate_dir_funcs_list, root, dirs, files = data
    print('-------Strating to work on %s-------' % root)
    if manipulate_dir_funcs_list != None:
        for dir in dirs:
            in_dir = os.path.join(root, dir)
            out_dir = in_dir.replace(data_path, out_path)
            # manipulate the dir
            for func in manipulate_dir_funcs_list:
                func(in_dir, out_dir)
    if manipulate_file_funcs_list != None:
        for file in files:
            in_file_path = os.path.join(root, file)
            out_file_path = in_file_path.replace(data_path, out_path)
            # manipulate the file
            is_first = True
            for func in manipulate_file_funcs_list:
                if is_first:
                    is_first = False
                else:
                    in_file_path = out_file_path
                func(in_file_path, out_file_path)
    print('-------Finished to work on %s-------' % root)


def manipulate_files_parallel(data_path, out_path, manipulate_file_funcs_list=None, manipulate_dir_funcs_list=None,
                              processes=5):
    """
    Manipulate files and folders like the 'manipulate_files' function but in parallel.
   :param data_path: The source directory
    :param out_path: The output directory (can be the same as data_path for inplace operations)
    :param manipulate_file_funcs_list: List of functions which required passed arguments are
    input file path and output file path
    :param manipulate_dir_funcs_list: List of functions which required passed arguments are input folder and
    output folder
    :param processes: Number of processes to used
    :return: none
    """
    pool = Pool(processes=processes)
    if manipulate_file_funcs_list != None and type(manipulate_file_funcs_list) != list:
        manipulate_file_funcs_list = [manipulate_file_funcs_list]
    if manipulate_dir_funcs_list != None and type(manipulate_dir_funcs_list) != list:
        manipulate_dir_funcs_list = [manipulate_dir_funcs_list]
    inplace = False
    if data_path == out_path:
        inplace = True
    # create the output folder
    create_folder(out_path)
    # input list for map
    input_list = []
    # check recursive all folders and sub folders
    for root, dirs, files in os.walk(data_path, topdown=True):
        # create all the dirs
        if not inplace:
            for dir in dirs:
                in_dir = os.path.join(root, dir)
                out_dir = in_dir.replace(data_path, out_path)
                create_folder(out_dir)
        input_list.append(
            (data_path, out_path, manipulate_file_funcs_list, manipulate_dir_funcs_list, root, dirs, files))
    pool.map(manipulate_files_process_func, input_list)
    pool.close()
    pool.join()
    print('Finished')
