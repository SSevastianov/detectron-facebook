"""
GSD - cm/pixel.
converting images/videos with known gsd to another gsd
"""
import subprocess
import os
import cv2
import data_tools.DataManipulation.common_funcs as cf

interpolation_method = {'enlarge': cv2.INTER_CUBIC, 'shrink': cv2.INTER_AREA}


def gsd_converter(src_path, inp_dict, out_folder, dst_gsd=10.):
    """
    Convert image or video to the dst_gsd and save it in the out_folder. The function used processes
    for parallel execution.
    :param src_path: The root folder where all the convertible images and video are inside it
    :param inp_dict: Dictionary of path: current gsd map
    :param out_folder: Output root dir
    :param dst_gsd: Output gsd
    :return: List of process exit codes
    """
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)
    pro_list = []
    for root, dirs, files in os.walk(src_path):
        for file in files:
            full_path = os.path.join(root, file)
            for inp_key in inp_dict.keys():
                if full_path == inp_key or full_path.startswith(inp_key):
                    src_gsd = inp_dict[inp_key]
                    dst_folder = root.replace(src_path, out_folder)
                    if cf.is_pic(full_path):
                        handle_pic(full_path, dst_folder, src_gsd, dst_gsd)
                    elif cf.is_video(full_path):
                        p = handle_vid(full_path, dst_folder, src_gsd, dst_gsd)
                        pro_list.append(p)
                    break
    # wait for all the subprocesses of the video converting
    exit_codes = [p.wait() for p in pro_list]
    return exit_codes


def handle_vid(src_path, dst_folder, src_gsd, dst_gsd):
    if not os.path.exists(dst_folder):
        os.makedirs(dst_folder)
    bashCommand = "ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of default=nw=1:nk=1 " + src_path
    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    output = output.split('\n')[:2]

    # get the width and height of the input video from video metadata
    src_w, src_h = int(output[0]), int(output[1])

    gsd_factor = src_gsd / float(dst_gsd)
    new_w = int(src_w * gsd_factor)
    new_h = int(src_h * gsd_factor)
    if new_h % 2 != 0: new_h += 1
    new_full_path = os.path.join(dst_folder, os.path.basename(src_path))
    bashCommand = "ffmpeg -i " + src_path + " -vf scale=" + str(new_w) + ":" + str(new_h) + " " + new_full_path
    process = subprocess.Popen(bashCommand.split(), stdout=None)
    return process


def handle_pic(src_path, dst_folder, src_gsd, dst_gsd):
    if not os.path.exists(dst_folder):
        os.makedirs(dst_folder)
    img_org = cv2.imread(src_path)
    # calculate the gsd factor
    gsd_factor = src_gsd / float(dst_gsd)
    gsd_factor_type = 'enlarge' if gsd_factor >= 1 else 'shrink'
    # resizing
    img_resized = cv2.resize(img_org, (0, 0), fx=gsd_factor, fy=gsd_factor,
                             interpolation=interpolation_method.get(gsd_factor_type))
    # save the new image
    file_name = os.path.basename(src_path)
    dst_file = os.path.join(dst_folder, file_name)
    cv2.imwrite(dst_file, img_resized)


if __name__ == '__main__':
    src_path = '/hdd/istar_data/wise_eyes_data/videos/cloud'
    out_folder = '/hdd/istar_data/wise_eyes_data/videos/cloud/gsd_10'
    inp_dict = {src_path + '/DJI_0051.MP4': 5.}
    gsd_converter(src_path, inp_dict, out_folder, dst_gsd=10.)
