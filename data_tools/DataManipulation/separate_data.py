import numpy as np
import pandas as pd
import itertools
import  math

def get_data_from_csv(csv_path):
    # must be a csv file and not in another format
    my_data = pd.read_csv(csv_path)
    # convert to numpy array
    my_data = my_data.values.astype(np.int32)
    return my_data


def cal_error(data, sums, pre_ideal=0.1):
    # L1 loss, comparing the split percent to the ideal percent
    local_sum = np.sum(data, axis=0)
    precent = local_sum / (sums).astype(np.float32)
    err = np.sum(np.abs(precent - pre_ideal))
    return err


def top_split_suggestions(data, num_suggestions, max_combo_size=3, perc_ideal=0.1):
    """
    Suggest eval set from the whole data according to tags for each class in each data folder.
    :param data: Matrix (numpy array) that each row is a data folder and each column is a class. The matrix values are
    the amount of class tags in each data folder
    :param num_suggestions: Number of suggestion to the split
    :param max_combo_size: The maximum number of different folders for the eval set
    :param perc_ideal: Ideal eval set percent from the whole data
    :return: List of num_suggestions eval sets and their corresponding errors
    """
    r, c = data.shape
    sums = np.sum(data, axis=0)
    min_err = np.inf
    top_combos = [[None]]
    top_errors = [min_err]
    indxs = list(range(r))

    for k in range(1, max_combo_size + 1):
        l = [indxs for i in range(k)]
        combos = itertools.product(*l)
        for com in combos:
            curr_set = set(com)
            if len(curr_set) < k:
                continue
            # build the subset of the data for this combo
            d_temp = np.array([data[i] for i in list(com)])
            curr_err = cal_error(d_temp, sums, perc_ideal)
            if curr_err < min_err or len(top_errors) <= num_suggestions:
                list_size = len(top_errors)
                # add to the correct position in top list
                for ind in range(list_size):
                    err = top_errors[ind]
                    # check if we already have this combo in the top combos list
                    elm_set = set(top_combos[ind - 1])
                    if curr_set == elm_set:
                        # already have this combo
                        break
                    if curr_err < err:
                        # found the position
                        top_errors.insert(ind, curr_err)
                        min_err = top_errors[-1]
                        top_combos.insert(ind, com)
                        if len(top_errors) > num_suggestions:
                            top_errors.pop()
                            top_combos.pop()
                        break
    return top_combos, top_errors


if __name__ == "__main__":
    # due to brute force search, this option only works for small matrices
    csv_path = '/home/rocket/Downloads/we.csv'
    num_suggestions = 10
    max_combo = 3
    per_ideal = 0.1
    data = get_data_from_csv(csv_path)
    top_combos, top_errors = top_split_suggestions(data, num_suggestions, max_combo, per_ideal)
    # format floating point for print purpose
    top_errors = [float("{0:.5f}".format(er)) for er in top_errors]
    print('Top folders combos: %s' % top_combos)
    print('Top Errors: %s' % top_errors)
