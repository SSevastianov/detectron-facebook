# Functions that I used to handle elbit data

import csv
import data_tools.DataManipulation.common_funcs as cf
import os
import shutil
from collections import Counter, defaultdict
from data_tools.utils import img_util as iu
import pandas as pd
from PIL import Image
import numpy as np
from os.path import join as pjoin


def flat_img_tree(src_path, dst_path, with_folder=True):
    """
    This function has 2 modes:
    1. with_folder=True - Create images/videos folders in dst_path with name composed from the folders' relative path from src_path. e.g:
    'src_path/a/b/c/d' will become 'dst_path/a_b_c_d' and then copy the image/video to this folder, e.g dst_path/a_b_c_d/img1.jpg
    2. with_folder=False - copy images/videos to the dst_path after composed their name from the relative path. e.g: 'src_path/a/b/c/img2.jpg'
    will be copied to 'dst_path/a_b_c_img2.jpg'
    :param src_path: source path that contains image/video folders
    :param dst_path: target path
    :param with_folder: do we need to put the images/videos in folders or all mixed in the dst_path
    :return:
    """
    cf.create_folder(dst_path)
    for root, dirs, files in os.walk(src_path, topdown=True):
        for file in files:
            file_path = os.path.join(root, file)
            first = True
            if cf.is_pic(file_path) or cf.is_video(file_path):
                if with_folder:
                    dst_dir = root.replace(src_path, '')[1:].replace('/', '_').replace(' ', '_')
                    dst_dir = os.path.join(dst_path, dst_dir)
                    if first:
                        # create the dst_dir
                        cf.create_folder(dst_dir)
                        first = False
                    out_file_path = pjoin(dst_dir,file)
                else:
                    out_file_path = pjoin(dst_path,file_path.replace(src_path, '')[1:].replace('/','_').replace(' ','_'))

                # now copy image or video
                shutil.copyfile(file_path, out_file_path)

# Create images folders in dst_path with name composed from the folders relative path from src_path. e.g:
# 'src_path/a/b/c/d' will become 'dst_path/a_b_c_d'.
# Rename images name to sequential numbers
def flat_img_tree_with_rename(src_path, dst_path, black_list=[]):
    num_format = '%09d'
    for root, dirs, files in os.walk(src_path, topdown=True):
        rename = True
        if files:
            for b in black_list:
                if root.startswith(b):
                    rename = False
            ind = 0
            parent_dir = os.path.relpath(root, src_path).split('/')[0]
            dst_dir = os.path.join(dst_path, parent_dir)
            cf.create_folder(dst_dir)

            for file in sorted(files):
                file_path = os.path.join(root, file)
                if cf.is_pic(file_path):
                    ind += 1
                    if not rename:
                        ind = iu.get_frame_numbers_from_names_list([file])[0]

                    name_prefix = root.replace(src_path, '').replace('/', '_').replace(' ', '_')

                    new_img_path = os.path.join(dst_dir, name_prefix + '_' + (num_format % ind) + iu.DEFAULT_IMG_EXT)
                    # now copy image
                    shutil.copyfile(file_path, new_img_path)


# Assume you got elbit's images with tags data in a folder tree, this function will manipulate the folder tree so
# you will have minimum number of folders and each img folder will have xml folder inside it with corresponding xmls
def fix_folder_format(path):
    for i in range(2):
        for root, dirs, files in os.walk(path, topdown=True):
            # empty folder
            if len(dirs) == 0:
                if len(files) == 0:
                    shutil.rmtree(root)
                    print('deleted %s' % root)
                    continue
            # folder with only files and none of them are pic or xml
            for file in files:
                file_path = os.path.join(root, file)
                if not cf.is_pic(file_path) and not cf.is_xml(file_path):
                    # delete file
                    os.remove(file_path)
                    print('deleted %s' % file_path)

            if len(os.listdir(root)) == 0:
                shutil.rmtree(root)
                print('deleted %s' % root)

            # move annotation to pic dir
            for dir in dirs:
                files_in_dir = os.listdir(os.path.join(root, dir))
                flag = False
                for f in files_in_dir:
                    if cf.is_pic(os.path.join(root, dir, f)):
                        flag = True
                        break
                if flag and 'Annotations' in dirs:
                    xml_old_path = os.path.join(root, 'Annotations')
                    shutil.copytree(xml_old_path, os.path.join(root, dir, 'xml'))
                    shutil.rmtree(xml_old_path)
                    print('deleted %s' % xml_old_path)
                    break

    for root, dirs, files in os.walk(path, topdown=False):
        if len(dirs) == 1 and len(files) == 0:
            # only 1 dir in root
            p = os.path.join(root, dirs[0])
            for f_d in os.listdir(p):
                p1 = os.path.join(p, f_d)
                p2 = os.path.join(root, f_d)
                if os.path.isfile(p1):
                    shutil.copyfile(p1, p2)
                    os.remove(p1)
                else:
                    shutil.copytree(p1, p2)
                    shutil.rmtree(p1)
            # delete folder
            shutil.rmtree(p)
            print('deleted %s' % p)


def remove_empty_dirs(path):
    while True:
        dir_del_counter = 0
        for root, dirs, files in os.walk(path, topdown=False):
            if not dirs and not files:
                os.rmdir(root)
                dir_del_counter += 1
        print('removed %d dirs' % (dir_del_counter))
        if dir_del_counter == 0:
            break


def fast_stats(path):
    res = cf.get_data_stats(path)
    print('stats for path: %s' % path)
    for key, val in res.items():
        print('%s : %s' % (key, val))


def get_labels_count_from_tfr_csvs(csv_dir):
    files = os.listdir(csv_dir)
    modes = ['train', 'val']
    channels = ['vis', 'mwir', 'swir', 'ir', 'n_a']
    labels_counter_per_channel = {}
    # init the counter dict
    for m in modes:
        labels_counter_per_channel[m] = {}
        for c in channels:
            labels_counter_per_channel[m][c] = Counter()
    none_relevant_headers = ['rel path', 'processed images count']
    for f in files:
        if f.endswith('_content_summary.csv'):
            mode = None
            for m in modes:
                if f.find(m) != -1:
                    mode = m
                    break
            if not mode:
                raise ValueError('problem in extracting the mode from csv name')
            channel = None
            for c in channels:
                if f.find(c) != -1:
                    channel = c
                    break
            if not channel:
                channel = 'n_a'
                # raise ValueError('problem in extracting the channel from the csv name')
            df = pd.read_csv(os.path.join(csv_dir, f))
            headers = list(df.keys())
            for n_r in none_relevant_headers:
                headers.remove(n_r)
            for h in headers:
                # count the labels for this csv
                labels_counter_per_channel[mode][channel][h] += sum(df[h])
    return labels_counter_per_channel


def change_file_names(root_dir, prefix_from, prefix_to):
    out_file = os.path.join(root_dir, 'file_names_conversion.csv')
    names_conversion_tuples = []
    dir_count = 1
    for root, dirs, files in os.walk(root_dir, topdown=True):
        # in case of dir
        if os.path.basename(root).startswith(prefix_from):
            new_name_prefix = '%s_%d' % (prefix_to, dir_count)
            new_root = os.path.join(os.path.dirname(root), new_name_prefix)
            # change dir name
            os.rename(root, new_root)
            names_conversion_tuples.append((root, new_root))
            file_count = 1
            # change file names
            for file in files:
                f_path = os.path.join(new_root, file)
                f_ext = os.path.splitext(file)[1]
                new_file_name = '%s_%d%s' % (new_name_prefix, file_count, f_ext)
                new_f_path = os.path.join(new_root, new_file_name)
                os.rename(f_path, new_f_path)
                names_conversion_tuples.append((f_path, new_f_path))
                file_count += 1
            dir_count += 1

        # in case of file
        else:
            new_name_prefix = ('%s_%s' % (os.path.basename(root), prefix_to)).replace(' ', '_')
            file_count = 1
            for file in files:
                if file.startswith(prefix_from):
                    f_path = os.path.join(root, file)
                    f_ext = os.path.splitext(file)[1]
                    new_file_name = '%s_%d%s' % (new_name_prefix, file_count, f_ext)
                    new_f_path = os.path.join(root, new_file_name)
                    os.rename(f_path, new_f_path)
                    names_conversion_tuples.append((f_path, new_f_path))
                    file_count += 1

    with open(out_file, 'w') as csv_file:
        writer = csv.writer(csv_file)
        # write header
        writer.writerow(['original_relative_path', 'new_relative_path'])
        for t in names_conversion_tuples:
            org_path = os.path.relpath(t[0], root_dir)
            new_path = os.path.relpath(t[1], root_dir)
            writer.writerow([org_path, new_path])

# Build dictionary of file types by their extension
def get_paths_by_type(root_dir):
    types_dict = defaultdict(list)
    for root, dirs, files in os.walk(root_dir, topdown=True):
        for file in files:
            f_name, ext = os.path.splitext(file)
            types_dict[ext].append(os.path.join(root, file))
    return types_dict

# Apply black regions to the images in the src_path according to the example image in example_pic_path
def black_region_by_dir(src_path, dst_path, example_pic_path):
    if not example_pic_path:
        raise ValueError('no example img for folder %s' % src_path)
    img_files_list = [fname for fname in os.listdir(src_path) if cf.is_pic(pjoin(src_path, fname))]
    if not img_files_list:
        # empty list
        return
    # make dst folder
    cf.create_folder(dst_path)
    # extract regions
    img = np.array(Image.open(example_pic_path))
    # find indices of black pixels
    indices = np.where(np.all(img == (0, 0, 0), axis=2))
    for img_name in img_files_list:
        img_path = pjoin(src_path, img_name)
        img = np.array(Image.open(img_path))
        # put black regions on img
        img[indices[0], indices[1]] = (0, 0, 0)
        new_img = Image.fromarray(img)
        # save img
        new_img.save(pjoin(dst_path, img_name))


def divide_big_folder_into_subfolders(src_path, dst_path, max_items=7000):
    inplace = False
    if src_path == dst_path:
        inplace = True
    # TODO: this might be dangerous because of sorting problems
    img_files_list = sorted([fname for fname in os.listdir(src_path) if cf.is_pic(pjoin(src_path, fname))])
    counter = 0
    for file_name in img_files_list:
        file_path = pjoin(src_path,file_name)
        folder_ind = int(counter/max_items)
        dst_dir = dst_path+'_'+str(folder_ind)
        dst_file_path = pjoin(dst_dir,file_name)
        if counter%max_items==0:
            cf.create_folder(dst_dir)
        if inplace:
            os.rename(file_path,dst_file_path)
        else:
            shutil.copyfile(file_path,dst_file_path)
        counter+=1



if __name__ == '__main__':
    # changing all the time because i use different funcs
    path = '/hdd/istar_data/horizontal_payload_data/coaps/coaps_elad_1_9_19'
    dst_path = '/hdd/istar_data/ift/filtered_flat'

    # example_pic_path = '/home/rocket/Downloads/000000.jpg'
    # fix_folder_format(path)
    # flat_img_tree(path,dst_path,False)
    fast_stats(path)
    # print('-------')
    # fast_stats(dst_path)
