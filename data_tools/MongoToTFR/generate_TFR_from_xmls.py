import os
import sys
from os.path import join as pjoin
import tensorflow as tf
import hashlib
import io
import yaml
from tqdm import tqdm
from data_tools.MongoToTFR import dataset_util
from collections import defaultdict, Counter
import intervals as I
from PIL import Image
import data_tools.MongoToTFR.general_utils as ut
import data_tools.MongoToTFR.draw_util as du
from data_tools.MongoToTFR.subimages_util import image_tag_dict_to_subimgs_examples
import data_tools.DataManipulation.common_funcs as cf
import data_tools.utils.img_util as iu
from data_tools.MongoToTFR.reporter import Reporter
import webcolors
from data_tools.utils.xml_util import XmlParser, find_xml_dir, get_xml_fname
import traceback

default_color_val = (255, 255, 255)  # white
default_bbox_size_filter_interval_key = 'default'
default_bbox_size_filter_interval_val = I.closedopen(10, I.inf).to_atomic()


class TFRGenerator:

    def __init__(self, params_dict):
        # params_dict is the dictionary from the yaml file

        # must fields
        self.data_dir = os.path.abspath(params_dict['DATA_DIR'])
        self.output_dir = os.path.abspath(params_dict['OUTPUT_DIR'])
        self.correct_labels = params_dict['CORRECT_LABELS']
        self.dir_mode = params_dict['DIR_MODE']
        # change to dict if necessary
        if type(self.dir_mode) == str:
            self.dir_mode = {'train': self.dir_mode, 'val': self.dir_mode}

        # optional fields
        self.modes = params_dict.get('MODES', ['train', 'val'])
        val_dir_list = params_dict.get('VAL_DIR_LIST', [])
        blacklist = params_dict.get('BLACKLIST', [])
        self.channel = params_dict.get('CHANNEL', '')
        self.save_marked = params_dict.get('SAVE_MARKED', True)
        self.line_thickness = params_dict.get('LINE_THICKNESS', 1)
        self.file_desc = params_dict.get('FILE_DESC', '')
        bbox_filtering_interval = params_dict.get('FILTERING_INTERVAL', None)
        label_to_color = params_dict.get('LABEL_TO_COLOR', [])
        self.exchange_labels = params_dict.get('EXCHANGE_LABELS', {})
        self.xml_struct = params_dict.get('XML_STRUCT', {XmlParser.DETECTION_KEY: 'object',
                                                         XmlParser.ID_KEY: 'id',
                                                         XmlParser.YMIN_KEY: 'bndbox/ymin',
                                                         XmlParser.XMIN_KEY: 'bndbox/xmin',
                                                         XmlParser.XMAX_KEY: 'bndbox/xmax',
                                                         XmlParser.YMAX_KEY: 'bndbox/ymax',
                                                         XmlParser.ANGLE_KEY: 'bndbox/angle',
                                                         XmlParser.LABEL_KEY: ['class', 'type', 'name'],
                                                         XmlParser.OCCLUSION_LEVEL_KEY: 'occluded',
                                                         XmlParser.APPEARANCE_LEVEL_KEY: 'difficult',
                                                         XmlParser.COLOR_KEY: 'color'})

        # nms params
        use_nms = params_dict.get('USE_NMS', False)
        nms_iou_threshold = params_dict.get('NMS_IOU_THRESHOLD', 1.0)
        nms_priority_field = params_dict.get('NMS_PRIORITY_FIELD', 'occlusion')
        nms_priority_higher_better = params_dict.get('NMS_PRIORITY_HIGHER_BETTER', False)
        self.nms_config = {'use_flag': use_nms, 'iou_th': nms_iou_threshold, 'priority_field': nms_priority_field,
                           'is_priority_higher_better': nms_priority_higher_better}

        # none_relevant params
        iou_th_none_relevant_labels = params_dict.get('IOU_THRESHOLD_NONE_RELEVANT_LABELS', 0.0)
        none_relevant_labels = params_dict.get('NONE_RELEVANT_LABELS', [])
        self.none_relevant_config = {'iou_th': iou_th_none_relevant_labels, 'labels': none_relevant_labels}

        # subimages params
        subimages = params_dict.get('SUBIMAGES', False)
        sub_image_size = params_dict.get('SUB_IMAGE_SIZE', None)
        sub_images_overlap = params_dict.get('SUB_IMAGES_OVERLAP', None)
        overlap_threshold = params_dict.get('SUB_OVERLAP_THRESHOLD', None)
        pad_val = params_dict.get('SUB_PAD_VAL', None)

        self.sub_images_config = {'use_flag': subimages, 'size': sub_image_size, 'overlap': sub_images_overlap,
                                  'overlap_th': overlap_threshold, 'pad': pad_val}

        self.bbox_filtering_interval = self.get_bbox_size_filter_interval_per_class(bbox_filtering_interval)
        print('filtering interval will be: %s' % self.bbox_filtering_interval)

        self.label_id_map = {}
        for ind, label in enumerate(self.correct_labels):
            self.label_id_map[label] = ind + 1
            self.label_id_map[ind + 1] = label

        for l in self.correct_labels:
            self.exchange_labels[l] = l

        # change color names to BGR tuple and labels to enum
        self.enum_to_color = {}
        for label, color_name in label_to_color.items():
            rgb_tuple = webcolors.name_to_rgb(color_name)
            bgr_tuple = rgb_tuple[::-1]
            exc_label = self.exchange_labels[label]
            enum = self.label_id_map[exc_label]
            self.enum_to_color[enum] = bgr_tuple

        self.log_dir = pjoin(self.output_dir, 'logs')

        # create the output folder
        cf.create_folder(self.output_dir)

        # xml parser
        self.xml_parser = XmlParser(self.log_dir, self.xml_struct)
        self.modes_params_dict = {'train': [val_dir_list + blacklist, None], 'val': [blacklist, val_dir_list]}

        # csv logger
        self.csv_reporter = Reporter(self.log_dir)

    def get_tfr_name(self, mode):
        tfr_name = self.file_desc + '_' + mode + '_' + self.channel
        if tfr_name.startswith('_'):
            tfr_name = tfr_name[1:]
        if tfr_name.endswith('_'):
            tfr_name = tfr_name[:-1]
        return tfr_name

    def generate_tfr(self):
        for mode in self.modes:
            self.blacklist, self.whitelist = self.modes_params_dict[mode]
            marked_output_dir = pjoin(self.log_dir, mode)
            cf.create_folder(marked_output_dir)

            self.curr_tfr_name = self.get_tfr_name(mode)

            # Generating csv files headers
            csv_names_and_headers_dict = {
                self.curr_tfr_name + '_data_content': ['image_relpath', 'nun_objects'] + self.correct_labels,
                self.curr_tfr_name + '_data_content_summary': ['rel path',
                                                               'processed images count'] + self.correct_labels,
                self.curr_tfr_name + '_filter_by_nms': ['rel path', 'tag_id', 'label'],
                self.curr_tfr_name + '_filter_by_none_relevant': ['rel path', 'tag_id', 'label'],
                self.curr_tfr_name + '_filter_by_bbox_size': ['rel path', 'tag_id', 'label'],
                self.curr_tfr_name + '_unrecognized_labels': ['rel path', 'tag_id', 'label'],
                self.curr_tfr_name + '_no_tags_left_imgs': ['image_path']
            }

            self.csv_reporter.create_csvs(csv_names_and_headers_dict)

            # log_fname = tfr_name + "_log.txt"

            annotation_dict = self.generate_annotation_dict_from_tree(marked_output_dir)
            if not annotation_dict:
                # annotation dict is empty!
                # delete the csvs
                self.csv_reporter.delete_csvs(csv_names_and_headers_dict.keys())
                # delete the marked folder
                # cf.delete_folder(marked_output_dir)
                continue
            if self.dir_mode[mode] == 'directory_wise':
                self.generate_tfr_form_annotation_dir_wise(annotation_dict)
            elif self.dir_mode[mode] == 'single':
                self.generate_tfr_form_annotation_single(annotation_dict)
            else:
                raise ("Error, invalid 'DIR_MODE' parameter.")

    def write_to_record(self, image_annotaion_dict, image_full_path, writer):
        if not image_annotaion_dict:
            # image with no tags
            message = "{} has no tags left"
            message_vars = [image_full_path]
            self.csv_reporter.logger(message, message_vars, self.curr_tfr_name + '_no_tags_left_imgs')
            return
        try:
            tf_example = self.annotation_to_tf_example(image_full_path, image_annotaion_dict)
            writer.write(tf_example.SerializeToString())
        except:
            print(traceback.format_exc())

    def labels_count(self, img_tags):
        labels_enum_list = [tag[XmlParser.LABEL_KEY] for tag in img_tags.values()]
        labels_counter = Counter(
            {label_enum: labels_enum_list.count(label_enum) for label_enum in self.label_id_map.keys() if
             type(label_enum) == int})
        return labels_counter

    def annotation_to_tf_example(self, image_full_path, img_tags):
        img_rel_path = os.path.relpath(image_full_path, self.data_dir)
        image_name, image_ext = os.path.splitext(img_rel_path)
        with tf.gfile.GFile(image_full_path, 'rb') as fid:
            encoded_jpg = fid.read()
        encoded_jpg_io = io.BytesIO(encoded_jpg)
        image = Image.open(encoded_jpg_io)
        width, height = image.size
        key = hashlib.sha256(encoded_jpg).hexdigest()
        xmins = []
        xmaxs = []
        ymins = []
        ymaxs = []
        classes_text = []
        classes = []
        for tag_id, tag in img_tags.items():
            xmins.append(float(tag[XmlParser.XMIN_KEY]) / width)
            xmaxs.append(float(tag[XmlParser.XMAX_KEY]) / width)
            ymins.append(float(tag[XmlParser.YMIN_KEY]) / height)
            ymaxs.append(float(tag[XmlParser.YMAX_KEY]) / height)
            label_enum = tag[XmlParser.LABEL_KEY]
            label = self.label_id_map[label_enum]
            classes_text.append(label.encode('utf-8'))
            classes.append(label_enum)

        example = tf.train.Example(features=tf.train.Features(feature={
            'image/height': dataset_util.int64_feature(height),
            'image/width': dataset_util.int64_feature(width),
            'image/filename': dataset_util.bytes_feature(
                image_name.encode('utf8')),
            'image/source_id': dataset_util.bytes_feature(
                image_name.encode('utf8')),
            'image/key/sha256': dataset_util.bytes_feature(key.encode('utf8')),
            'image/encoded': dataset_util.bytes_feature(encoded_jpg),
            'image/format': dataset_util.bytes_feature(image_ext.encode('utf8')),
            'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
            'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
            'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
            'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
            'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
            'image/object/class/label': dataset_util.int64_list_feature(classes),
        }))
        return example

    def generate_tfr_form_annotation_dir_wise(self, annotation_dict):
        sub_images_flag = self.sub_images_config['use_flag']
        if not sub_images_flag:
            # TODO: try to merege the two cases
            # Generating records from the full frame
            for directory, directory_dict in annotation_dict.items():
                dir_labels_counter = Counter()
                dir_proc_imgs_count = 0
                TFR_name = (self.curr_tfr_name + '_' + directory + '.record').replace('/', '_')
                TFR_fullpath = pjoin(self.output_dir, TFR_name)
                with tf.python_io.TFRecordWriter(TFR_fullpath) as writer:
                    for image_fname, image_annotaion_dict in directory_dict.items():
                        # Writing to TFRecord
                        image_path = pjoin(self.data_dir, directory, image_fname)
                        self.write_to_record(image_annotaion_dict, image_path, writer)
                        # Recording the image name, tags count and the tag count for each class
                        image_label_counter = self.labels_count(image_annotaion_dict)
                        dir_labels_counter.update(image_label_counter)
                        dir_proc_imgs_count += 1
                        # Publishing
                        image_labels_counter_list = [image_label_counter[label_enum] for label_enum in
                                                     sorted(image_label_counter.keys())]
                        labels_enum_list = [tag[XmlParser.LABEL_KEY] for tag in image_annotaion_dict.values()]
                        message_vars = [pjoin(directory, image_fname),
                                        len(labels_enum_list)] + image_labels_counter_list
                        self.csv_reporter.report_csv(message_vars, self.curr_tfr_name + '_data_content.csv')
                # Recording and publishing directory summary
                dir_labels_counter_list = [dir_labels_counter[label_enum] for label_enum in
                                           sorted(dir_labels_counter.keys())]
                message_vars = [directory, dir_proc_imgs_count] + dir_labels_counter_list
                self.csv_reporter.report_csv(message_vars, self.curr_tfr_name + '_data_content_summary.csv')
        else:
            # Generating records from the subimages frame
            sub_image_size = self.sub_images_config['size']
            sub_image_overlap = self.sub_images_config['overlap']
            overlap_th = self.sub_images_config['overlap_th']
            pad_val = self.sub_images_config['pad']
            # TODO(guy): refactored the sub image logic also. this code is untested and may have bugs!
            # filter the label_id_map to contain only enum keys. patch because i don't want to change the subimage logic for now.
            label_to_id = {}
            for key, val in self.label_id_map.items():
                if type(key) != int:
                    label_to_id[key] = val
            for directory, directory_dict in annotation_dict.items():
                dir_labels_counter = Counter()
                dir_proc_imgs_count = 0
                TFR_name = (self.curr_tfr_name + '_' + directory + '.record').replace('/', '_')
                TFR_fullpath = pjoin(self.output_dir, TFR_name)
                with tf.python_io.TFRecordWriter(TFR_fullpath) as writer:
                    for image_fname, image_annotaion_dict in directory_dict.items():
                        # Writing to TFRecord
                        image_data = self.convert_image_annotation_format(image_annotaion_dict, directory, image_fname)
                        tf_examples, img_labels_count_dict = image_tag_dict_to_subimgs_examples(image_data,
                                                                                                label_to_id,
                                                                                                sub_image_size,
                                                                                                sub_image_overlap,
                                                                                                overlap_th,
                                                                                                self.data_dir,
                                                                                                pad_val,
                                                                                                self.log_dir,
                                                                                                testing_flag=True,
                                                                                                filter_untaged_subimgs_flag=True)
                        for tf_example in tf_examples:
                            writer.write(tf_example.SerializeToString())
                        # TODO: add summary data

    def generate_tfr_form_annotation_single(self, annotation_dict):
        sub_images_flag = self.sub_images_config['use_flag']
        TFR_fullname = self.curr_tfr_name + '.record'
        TFR_fullpath = pjoin(self.output_dir, TFR_fullname)
        if not sub_images_flag:
            # TODO: try to mrege the two cases
            # Generating records from the full frame
            with tf.python_io.TFRecordWriter(TFR_fullpath) as writer:
                for directory, directory_dict in annotation_dict.items():
                    dir_labels_counter = Counter()
                    dir_proc_imgs_count = 0
                    for image_fname, image_annotaion_dict in directory_dict.items():
                        # Writing to TFRecord
                        image_path = pjoin(self.data_dir, directory, image_fname)
                        self.write_to_record(image_annotaion_dict, image_path, writer)
                        # Recording the image name, tags count and the tag count for each class
                        image_label_counter = self.labels_count(image_annotaion_dict)
                        dir_labels_counter.update(image_label_counter)
                        dir_proc_imgs_count += 1
                        # Publishing
                        image_labels_counter_list = [image_label_counter[label_enum] for label_enum in
                                                     sorted(image_label_counter.keys())]
                        labels_enum_list = [tag[XmlParser.LABEL_KEY] for tag in image_annotaion_dict.values()]
                        message_vars = [pjoin(directory, image_fname),
                                        len(labels_enum_list)] + image_labels_counter_list
                        self.csv_reporter.report_csv(message_vars, self.curr_tfr_name + '_data_content.csv')
                    # Recording and publishing directory summary
                    dir_labels_counter_list = [dir_labels_counter[label_enum] for label_enum in
                                               sorted(dir_labels_counter.keys())]
                    message_vars = [directory, dir_proc_imgs_count] + dir_labels_counter_list
                    self.csv_reporter.report_csv(message_vars, self.curr_tfr_name + '_data_content_summary.csv')
        else:
            # Generating records from the subimages frame
            sub_image_size = self.sub_images_config['size']
            sub_image_overlap = self.sub_images_config['overlap']
            overlap_th = self.sub_images_config['overlap_th']
            pad_val = self.sub_images_config['pad']
            # TODO(guy): refactored the sub image logic also. this code is untested and may have bugs!
            # filter the label_id_map to contain only enum keys. patch because i don't want to change the subimage logic for now.
            label_to_id = {}
            for key, val in self.label_id_map.items():
                if type(key) != int:
                    label_to_id[key] = val
            with tf.python_io.TFRecordWriter(TFR_fullpath) as writer:
                for directory, directory_dict in annotation_dict.items():
                    dir_labels_counter = Counter()
                    dir_proc_imgs_count = 0
                    for image_fname, image_annotaion_dict in directory_dict.items():
                        # Writing to TFRecord
                        image_data = self.convert_image_annotation_format(image_annotaion_dict, directory, image_fname)
                        tf_examples, img_labels_count_dict = image_tag_dict_to_subimgs_examples(image_data,
                                                                                                label_to_id,
                                                                                                sub_image_size,
                                                                                                sub_image_overlap,
                                                                                                overlap_th,
                                                                                                self.data_dir,
                                                                                                pad_val,
                                                                                                self.log_dir,
                                                                                                testing_flag=True,
                                                                                                filter_untaged_subimgs_flag=True)
                        for tf_example in tf_examples:
                            writer.write(tf_example.SerializeToString())
                        # TODO: add summary data

    def generate_annotation_dict_from_tree(self, marked_output_dir):
        tree = lambda: defaultdict(tree)
        annotation_dict_filtered = tree()  # tree like default dictionary
        for root, dirs, files in os.walk(self.data_dir):

            # black list filtering
            ut.blacklist_filtering(self.blacklist, root, dirs)

            # check for image files
            imgs_flist = [fname for fname in files if cf.is_pic(pjoin(root, fname))]
            if not imgs_flist:
                continue
            imgs_flist.sort()

            # Whitelist filtering
            if not ut.whitelist_filtering(self.whitelist, root):
                continue

            tags_dir = find_xml_dir(root)
            if tags_dir is None:
                continue
            # Extracting annotations from XML
            rel_path = os.path.relpath(root, self.data_dir)
            tqdm_text = rel_path
            # create a sub output dir for this images folder
            if self.save_marked:
                curr_marked_output_dir = pjoin(marked_output_dir, os.path.basename(root))
                cf.create_folder(curr_marked_output_dir)
            for idx in tqdm(range(len(imgs_flist)), ascii=True, desc=tqdm_text):
                img_fname = imgs_flist[idx]
                # Get matching xml file name
                xml_fname = get_xml_fname(img_fname)
                xml_path = pjoin(tags_dir[0], xml_fname)
                img_path = pjoin(root, img_fname)
                img_rel_path = os.path.relpath(img_path, self.data_dir)
                # Get image spatial shape
                # img_size = iu.get_img_size_and_depth(img_path)
                # Parse the XML and save it as dictionary
                tags = self.xml_parser.parse_xml_tags(xml_path)
                if not tags:
                    continue
                drop_tags = {}
                # filter by nms
                if self.nms_config['use_flag']:
                    tags, drop_tags = self.filter_by_nms(img_rel_path, tags)
                # filter none relevant tag
                tags, tmp_drop_tags = self.filter_by_none_relevant(img_rel_path, tags)
                # add the new drop tags to the general drop tags dictionary
                drop_tags.update(tmp_drop_tags)
                # filter by bbox size
                tags, tmp_drop_tags = self.filter_by_bbox_size(img_rel_path, tags)
                # add the new drop tags to the general drop tags dictionary
                drop_tags.update(tmp_drop_tags)
                # filter by exchange labels dict
                tags, tmp_drop_tags = self.filter_by_exchange_labels(img_rel_path, tags)
                # add the new drop tags to the general drop tags dictionary
                drop_tags.update(tmp_drop_tags)
                # change the labels from string to int
                self.convert_labels_to_enum(tags)
                # add to dict tree
                annotation_dict_filtered[rel_path][img_fname] = tags
                if self.save_marked:
                    du.plot_bboxes_and_save(img_path, tags, drop_tags, self.enum_to_color, curr_marked_output_dir,
                                            default_color_val=default_color_val, thickness=self.line_thickness)

        return annotation_dict_filtered

    def convert_labels_to_enum(self, tags):
        # change the tags inplace
        for tag_id in tags.keys():
            label = tags[tag_id][XmlParser.LABEL_KEY].lower()
            if self.exchange_labels:
                label = self.exchange_labels.get(label, None)
            assert label != None  # by now label should be only from correct labels list
            tags[tag_id][XmlParser.LABEL_KEY] = self.label_id_map[label]

    def convert_image_annotation_format(self, annotation_dict, relative_path, img_fname):
        # guy addition
        '''
        bbox coordinates are given in relative form and are numbers between 0 and 100
        '''
        img_path = pjoin(self.data_dir, relative_path, img_fname)
        # TODO(guy): try to calculate and save this before this func
        height, width, depth = iu.get_img_size_and_depth(img_path)
        tags = [img_path, []]
        for tag in annotation_dict.values():
            detection = {}
            detection['xmin'] = tag[XmlParser.XMIN_KEY] / width * 100
            detection['ymin'] = tag[XmlParser.YMIN_KEY] / height * 100
            detection['xmax'] = tag[XmlParser.XMAX_KEY] / width * 100
            detection['ymax'] = tag[XmlParser.YMAX_KEY] / height * 100
            detection['filename'] = img_fname
            detection['foldername'] = relative_path
            detection['label'] = self.label_id_map[tag[XmlParser.LABEL_KEY]]  # convert from label_enum to label_txt
            tags[1].append(detection)
        return tags

    # TODO(guy): all the filter funcs are in the same format so we can generalize it to 1 func for code reuse
    def filter_by_nms(self, img_rel_path, tags):
        iou_th = self.nms_config['iou_th']
        priority_field = self.nms_config['priority_field']
        priority_higher_better = self.nms_config['is_priority_higher_better']
        keep_tag_ids = ut.filter_tags_by_nms(tags, iou_th, priority_field, priority_higher_better)
        keep_tags, drop_tags = ut.split_tags(tags, keep_tag_ids)
        # log the drop tags
        for tag_id, tag in drop_tags.items():
            message_vars = [img_rel_path, tag_id, tag[XmlParser.LABEL_KEY]]
            self.csv_reporter.report_csv(message_vars, self.curr_tfr_name + '_filter_by_nms')
        return keep_tags, drop_tags

    def filter_by_none_relevant(self, img_rel_path, tags):
        iou_th = self.none_relevant_config['iou_th']
        none_relevant_labels = self.none_relevant_config['labels']
        keep_tag_ids = ut.filter_tags_by_none_relevant(tags, none_relevant_labels, iou_th)
        keep_tags, drop_tags = ut.split_tags(tags, keep_tag_ids)
        # log the drop tags
        for tag_id, tag in drop_tags.items():
            message_vars = [img_rel_path, tag_id, tag[XmlParser.LABEL_KEY]]
            self.csv_reporter.report_csv(message_vars, self.curr_tfr_name + '_filter_by_none_relevant')
        return keep_tags, drop_tags

    def filter_by_bbox_size(self, img_rel_path, tags):
        keep_tag_ids = ut.filter_tags_by_bbox_side(tags, self.bbox_filtering_interval,
                                                   default_bbox_size_filter_interval_key)
        keep_tags, drop_tags = ut.split_tags(tags, keep_tag_ids)
        # log the drop tags
        for tag_id, tag in drop_tags.items():
            message_vars = [img_rel_path, tag_id, tag[XmlParser.LABEL_KEY]]
            self.csv_reporter.report_csv(message_vars, self.curr_tfr_name + '_filter_by_bbox_size')
        return keep_tags, drop_tags

    def filter_by_exchange_labels(self, img_rel_path, tags):
        keep_tag_ids = ut.filter_tags_by_exchange_labels(tags, self.exchange_labels)
        keep_tags, drop_tags = ut.split_tags(tags, keep_tag_ids)
        # log the drop tags
        for tag_id, tag in drop_tags.items():
            message_vars = [img_rel_path, tag_id, tag[XmlParser.LABEL_KEY]]
            self.csv_reporter.report_csv(message_vars, self.curr_tfr_name + '_unrecognized_labels')
        return keep_tags, drop_tags

    def get_bbox_size_filter_interval_per_class(self, intervals):
        filter_interval_per_class = {default_bbox_size_filter_interval_key: default_bbox_size_filter_interval_val}
        if not intervals:
            # return the default interval
            print('Using the default bbox size filtering interval')
            return filter_interval_per_class
        if type(intervals) == list:
            # only default value for all classes
            filter_interval_per_class[default_bbox_size_filter_interval_key] = self.convert_interval_str_to_interval(
                intervals)
        elif type(intervals) == dict:
            for cls, interval_str in intervals.items():
                filter_interval_per_class[cls] = self.convert_interval_str_to_interval(interval_str)
        else:
            raise ValueError('Filtering interval should be list or dictionary')
        return filter_interval_per_class

    def convert_interval_str_to_interval(self, interval_list):
        if len(interval_list) != 2:
            raise ValueError("filtering interval list doesn't have 2 elements")
        start, end = interval_list
        if end == 'inf':
            return I.closedopen(start, I.inf).to_atomic()
        else:
            return I.closedopen(start, end).to_atomic()


def main(yaml_path):
    with open(yaml_path, 'r') as yaml_file:
        params_dict = yaml.load(yaml_file, Loader=yaml.SafeLoader)
    tfr_generator = TFRGenerator(params_dict)
    tfr_generator.generate_tfr()


if __name__ == '__main__':
    # read the program arguments - put the yaml full path as a program parameter
    assert len(sys.argv) == 2
    yaml_path = sys.argv[1]
    main(yaml_path)
