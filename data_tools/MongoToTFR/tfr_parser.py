import argparse

import tensorflow as tf
import numpy as np
from PIL import Image
import io
from collections import Counter
import os
import cv2
import data_tools.DataManipulation.common_funcs as cf
from tqdm import tqdm
from termcolor import colored
from data_tools.MongoToTFR import dataset_util
import math
# tf.train.example value types keywords
BYTES_TYPE_KEY = 'bytes'
INT64_TYPE_KEY = 'int64'
FLOAT_TYPE_KEY = 'float'

encode_func_map = {BYTES_TYPE_KEY: dataset_util.bytes_list_feature,
                   INT64_TYPE_KEY: dataset_util.int64_list_feature,
                   FLOAT_TYPE_KEY: dataset_util.float_list_feature}


def get_record_value_type(example, key):
    """
    Return the value type of a specific tf.train.example key
    :param example: A tf.train.example instance
    :param key: The relevant key to extract its value type
    :return: Value type keyword
    """

    ex_val = example.features.feature[key]
    if len(ex_val.bytes_list.value) > 0:
        return BYTES_TYPE_KEY
    elif len(ex_val.int64_list.value) > 0:
        return INT64_TYPE_KEY
    elif len(ex_val.float_list.value) > 0:
        return FLOAT_TYPE_KEY
    else:
        raise ValueError("can't determine value type for key: %s" % key)


def get_record_keys(example):
    """
    Return a tf.train.example key values (decoded)
    :param example: A tf.train.example instance
    :return: List of keys
    """
    return list(dict(example.features.feature).keys())


def filter_is_crowd(record_dict):
    """
    Used for coco records
    Filter out all the crowded tags from the record dictionary
    :param record_dict: Dictionary contains byte list/int list or float list values
    :return: record_dict after the filter
    """
    is_crowd_np = np.array(record_dict['image/object/is_crowd'])
    is_crowd_indices = np.where(is_crowd_np != 0)[0]
    if len(is_crowd_indices) == 0:
        return record_dict
    filter_fields_list = ['image/object/class/label', 'image/object/bbox/ymin', 'image/object/bbox/ymax',
                          'image/object/bbox/xmin', 'image/object/bbox/xmax', 'image/object/area',
                          'image/object/is_crowd']
    for field in filter_fields_list:
        field_val = record_dict[field]
        # filter out the relevant elements
        field_val = np.delete(field_val, is_crowd_indices)
        record_dict[field] = field_val.tolist()
    return record_dict


def manipulate_example(manipulate_func, example):
    """
    Manipulate a tf.train.example with the manipulate_func
    :param manipulate_func: Function to manipulate an example dictionary with values which are int/bytes/float lists
    :param example: tf.train.example
    :return: A new tf.train.example after the manipulation
    """
    example_dict = {}
    manipulate_ex = {}
    # convert example to python dict
    for k in get_record_keys(example):
        v_feature = example.features.feature[k]
        v_type = get_record_value_type(example, k)
        v = None
        if v_type == BYTES_TYPE_KEY:
            v = v_feature.bytes_list.value
        elif v_type == INT64_TYPE_KEY:
            v = v_feature.int64_list.value
        elif v_type == FLOAT_TYPE_KEY:
            v = v_feature.float_list.value
        example_dict[k] = v
    # apply filter func
    example_dict = manipulate_func(example_dict)
    # convert example dict to ready tf.train.Example
    for k, v in example_dict.items():
        v_element = v[0]
        if type(v_element) == bytes:
            manipulate_ex[k] = encode_func_map[BYTES_TYPE_KEY](v)
        elif type(v_element) == int:
            manipulate_ex[k] = encode_func_map[INT64_TYPE_KEY](v)
        elif type(v_element) == float:
            manipulate_ex[k] = encode_func_map[FLOAT_TYPE_KEY](v)

    manipulate_ex = tf.train.Example(features=tf.train.Features(feature=manipulate_ex))
    return manipulate_ex


def manipulate_tfr(src_tfr_path, out_tfr_path, manipulate_func_list):
    record_iterator = tf.python_io.tf_record_iterator(path=src_tfr_path)
    abort_counter  = 0
    with tf.python_io.TFRecordWriter(out_tfr_path) as writer:
        for ind, string_record in enumerate(record_iterator):
            example = tf.train.Example()
            # Read example from record
            example.ParseFromString(string_record)
            # Manipulate the example
            for func in manipulate_func_list:
                try:
                    example = manipulate_example(func, example)
                except:
                    # Caused by unknown type of a record field (usually because it is an empty list).
                    # Aborting the manipulation and return the original example
                    abort_counter+=1
                    if abort_counter % 50 == 0:
                        print('Aborted manipulation of %d/%d records' % (abort_counter,ind+1))
            # Write new example to out record
            writer.write(example.SerializeToString())
    print('Aborted manipulation of %d/%d records' % (abort_counter, ind + 1))


# for object detection, but if you change the fields you can modify it to every tfrecord you want
def parse_tfr_generator(file_path):

    record_iterator = tf.python_io.tf_record_iterator(path=file_path)
    record_iterator_1 = tf.python_io.tf_record_iterator(path=file_path)

    count_images = 1
    for string_record in record_iterator_1:
        example1 = tf.train.Example()
        example1.ParseFromString(string_record)
        count_images += 1

    tf_examples = []
    i = 0
    for string_record in record_iterator:
        i += 1
        example_data = {}
        example = tf.train.Example()
        example.ParseFromString(string_record)
        if (i+9) % (10) == 0:
            # if i%(1) == 0:
            # start to parse all the record fields
            example_data['filename'] = example.features.feature['image/filename'].bytes_list.value[0].decode('utf8')
            example_data['height'] = int(example.features.feature['image/height'].int64_list.value[0])
            example_data['width'] = int(example.features.feature['image/width'].int64_list.value[0])
            example_data['source_id'] = example.features.feature['image/source_id'].bytes_list.value[0].decode('utf8')
            example_data['key'] = example.features.feature['image/key/sha256'].bytes_list.value[0].decode('utf8')
            image_string = example.features.feature['image/encoded'].bytes_list.value[0]
            image = Image.open(io.BytesIO(image_string))
            example_data['image'] = np.asarray(image)
            example_data['format'] = example.features.feature['image/format'].bytes_list.value[0].decode('utf8')
            example_data['bbox_xmin'] = [float(x) for x in
                                         example.features.feature['image/object/bbox/xmin'].float_list.value]
            example_data['bbox_xmax'] = [float(x) for x in
                                         example.features.feature['image/object/bbox/xmax'].float_list.value]
            example_data['bbox_ymin'] = [float(y) for y in
                                         example.features.feature['image/object/bbox/ymin'].float_list.value]
            example_data['bbox_ymax'] = [float(y) for y in
                                         example.features.feature['image/object/bbox/ymax'].float_list.value]
            example_data['class_text'] = [t.decode('utf8') for t in
                                          example.features.feature['image/object/class/text'].bytes_list.value]
            example_data['class_label'] = [int(l) for l in
                                           example.features.feature['image/object/class/label'].int64_list.value]
            difficult = [int(d) for d in example.features.feature['image/object/difficult'].int64_list.value]
            # check that there is value
            # example_data['difficult'] = difficult[0] if len(difficult)!=0 else None
            example_data['truncated'] = [int(t) for t in
                                         example.features.feature['image/object/truncated'].int64_list.value]
            # example_data['view'] = example.features.feature['image/object/view'].bytes_list.value[0]
            # finish parsing
            tf_examples.append(example)
            yield example_data

    with tf.python_io.TFRecordWriter(file_path.replace('/coco/', '/new_coco_TF_rec/')) as writer:
        for tf_example in tf_examples:
            writer.write(tf_example.SerializeToString())

def get_records_stats(tfr_dir_path):
    # assume the record ends with .record
    tfr_paths = [os.path.join(tfr_dir_path, f) for f in os.listdir(tfr_dir_path) if f.endswith('.record')]
    stats = {}
    for path in tqdm(tfr_paths, desc='tfr'):
        record_iterator = parse_tfr_generator(path)
        r_counter = Counter()
        for record in record_iterator:
            r_counter['num_imgs'] += 1
            for l in record['class_text']:
                r_counter[l] += 1
        stats[path] = r_counter
    return stats


def validate_bbox_params(bbox):
    if np.min(bbox) < 0 or np.max(bbox) > 1.1:
        return False
    return True


def validate_labels(labels, correct_labels):
    return np.isin(labels, correct_labels).all()


def check_tfr(tfr_dir_path, correct_labels=None, save_dir=None):
    def check_for_duplications(all, curr, record_key):
        duplicated_fn = [item for item, count in Counter(all).items() if count > 1 and item in curr]
        if len(duplicated_fn) > 0:
            print('%s has duplications in field %s: %s' % (path, record_key, duplicated_fn))
            return False
        return True

    tfr_paths = [os.path.join(tfr_dir_path, f) for f in os.listdir(tfr_dir_path) if f.endswith('.record')]
    check_labels = False
    # create save dir
    if save_dir:
        cf.create_folder(save_dir)
    if correct_labels is not None:
        check_labels = True
        # convert to numpy if should check labels
        correct_labels = np.array(correct_labels)
    all_filenames = []
    all_keys = []
    valid_records = []
    invalid_records = []
    for path in tfr_paths:
        pass_flag = True
        record_iterator = parse_tfr_generator(path)
        curr_filenames = []
        curr_keys = []
        for record in tqdm(record_iterator, desc=os.path.basename(path)):
            curr_filenames.append(record['filename'])
            curr_keys.append(record['key'])
            if save_dir:
                # save picture
                marked_img = plot_bboxes(record)
                out_path = os.path.join(save_dir, str(record['filename']))
                if not cf.is_pic(out_path, should_exist=False):
                    out_path += '.jpg'
                cf.create_folder(os.path.dirname(out_path))
                out_img = Image.fromarray(marked_img.astype(np.uint8))
                out_img.save(out_path)
            # check bbox params
            bbox = np.array([record['bbox_ymin'], record['bbox_xmin'], record['bbox_xmax'], record['bbox_ymax']])
            if not validate_bbox_params(bbox):
                print('%s has bbox: %s with values not between 0 to 1' % (path, bbox))
                pass_flag = False
            # check labels
            if check_labels:
                labels = np.array(record['class_label'])
                if not validate_labels(labels, correct_labels):
                    print('%s has label: %s which is not in the correct labels: %s' % (path, labels, correct_labels))
                    pass_flag = False
        # check for duplications in filenames
        all_filenames.extend(curr_filenames)
        if not check_for_duplications(all_filenames, curr_filenames, 'filename'):
            pass_flag = False

        # check for duplications in keys
        all_keys.extend(curr_keys)
        if not check_for_duplications(all_keys, curr_keys, 'key'):
            pass_flag = False
        if pass_flag:
            valid_records.append(path)
            print(colored('\nrecord: %s is valid' % path, 'green'))
        else:
            invalid_records.append(path)
            print(colored('\nrecord: %s is NOT valid!' % path, 'red'))
    if len(invalid_records) == 0:
        print(colored('Records are valid!', 'green'))
    else:
        print(colored('NOT all records are valid!!!', 'red'))
    print('valid records: %s' % valid_records)
    print('invalid records: %s' % invalid_records)


def plot_bboxes(record, color=(255, 0, 0), thickness=2):
    image = record['image'].copy()
    xmins = np.array(record['bbox_xmin']) * record['width']
    xmaxes = np.array(record['bbox_xmax']) * record['width']
    ymins = np.array(record['bbox_ymin']) * record['height']
    ymaxes = np.array(record['bbox_ymax']) * record['height']
    num_boxes = len(xmins)
    # plot tags
    for i in range(num_boxes):
        tag_top_left_pixel = (int(xmins[i]), int(ymins[i]))
        tag_bottom_right_pixel = (int(xmaxes[i]), int(ymaxes[i]))
        cv2.rectangle(image, tag_top_left_pixel, tag_bottom_right_pixel, color=color, thickness=thickness)
    return image


def parse_args():
    """
    parse command line arguments
    :return:
    """
    parser = argparse.ArgumentParser(description="tfr checker")
    parser.add_argument(
        "--src_dir", help="Path to the tfrs directory", required=True)
    parser.add_argument(
        "--out_dir", help="Path to the output directory", default=None, required=False)
    parser.add_argument("--num_classes", default=None, help="number of classes")
    parser.add_argument("--check_tfr", help="check tfr if valid", action='store_true')
    parser.add_argument("--stats_tfr", help="count tags and imgs in tfr", action='store_true')
    return parser.parse_args()


def run(args):
    is_check = args.check_tfr
    is_count = args.stats_tfr
    src_dir = args.src_dir

    if not (is_check or is_count):
        print(colored('You should provide at least one of the flags: --check_tfr,--stats_tfr', 'red'))

    if is_check:
        print(colored('Start checking tfr dir: %s' % src_dir, 'cyan'))
        out_dir = args.out_dir
        num_cls = args.num_classes
        if num_cls is not None:
            corr_labels = list(range(1, int(num_cls) + 1))
        else:
            corr_labels = None
        # check the tfr in src_dir
        check_tfr(src_dir, correct_labels=corr_labels, save_dir=out_dir)
        print(colored('Finished checking tfr dir: %s' % src_dir, 'cyan'))

    if is_count:
        print(colored('Start collecting stats on tfrs in dir: %s' % src_dir, 'cyan'))
        stats = get_records_stats(src_dir)
        total_counter = Counter()
        for key, stat in stats.items():
            total_counter += stat
        print(total_counter)
        print(colored('Finished collecting stats on tfr dir: %s' % src_dir, 'cyan'))


if __name__ == "__main__":
    args = parse_args()
    run(args)
