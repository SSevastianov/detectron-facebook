from data_tools.MongoToTFR import get_mongo_data2
import data_tools.DataManipulation.common_funcs as cf
import data_tools.utils.img_util as iu
import data_tools.utils.vid_util as vu
import data_tools.MongoToTFR.general_utils as ut
from data_tools.utils.box_util import get_rotated_rectangle_corners
import yaml
import sys
import os
from collections import defaultdict
import csv
import copy
from data_tools.EvalTools.eval_analysis import EVAL_SYSTEM_CSV_HEADERS
import data_tools.MongoToTFR.draw_util as du
import pandas as pd
from data_tools.utils.xml_util import XmlParser

# TODO: remove after only a patch
from data_tools.DataManipulation.temp import is_filter_tag

EVAL_SYSTEM_GT_CSV_HEADERS = copy.deepcopy(EVAL_SYSTEM_CSV_HEADERS)
EVAL_SYSTEM_GT_CSV_HEADERS.remove('score')


def plot_tags_from_csv(img_dir, csv_path, save_dir, frame_range=None):
    # frame_range is a list of 2 integers (start,end)
    frame_h, xmin_h, ymin_h, xmax_h, ymax_h, label_h, id_h = EVAL_SYSTEM_GT_CSV_HEADERS
    df = pd.read_csv(csv_path)
    # set the list of frames to draw
    if not frame_range:
        frame_range = set(df[frame_h])
    else:
        frame_range = range(*frame_range)

    for frame_num in frame_range:
        relevant_tag_rows = df.loc[df[frame_h] == frame_num]
        # build the tags dict
        tags = {}
        for ind in range(len(relevant_tag_rows)):
            tag = {}
            tag[XmlParser.ANGLE_KEY] = 0
            tag[XmlParser.XMIN_KEY] = relevant_tag_rows.iloc[ind][xmin_h]
            tag[XmlParser.XMAX_KEY] = relevant_tag_rows.iloc[ind][xmax_h]
            tag[XmlParser.YMIN_KEY] = relevant_tag_rows.iloc[ind][ymin_h]
            tag[XmlParser.YMAX_KEY] = relevant_tag_rows.iloc[ind][ymax_h]
            tags[ind] = tag

        img_path = os.path.join(img_dir, str(frame_num) + iu.DEFAULT_IMG_EXT)
        if not os.path.exists(img_path):
            print('%s does not exist. Abort save marked img' % img_path)
            return
        du.plot_bboxes_and_save(img_path, tags, drop_tags={}, label_to_color={}, saving_dir=save_dir, thickness=5)


def get_img_meta_dict(data_dir_path, pic_data):
    # first check for img file
    pic_file_name = pic_data[0]['filename']
    folder = pic_data[0]['foldername']
    img_path = os.path.join(data_dir_path, folder, pic_file_name)
    if os.path.exists(img_path):
        height, width, depth = iu.get_img_size_and_depth(img_path=img_path)
        meta_dict = {'filename': pic_file_name, 'height': height, 'width': width, 'depth': depth,
                     'from_vid': False, 'org_fps': -1}
        return meta_dict

    # check for video file
    vid_names, vid_ext = map(list, zip(*[os.path.splitext(v) for v in os.listdir(data_dir_path)]))
    if folder not in vid_names:
        # in case file is not local
        return None
    ind = vid_names.index(folder)
    vid_path = os.path.join(data_dir_path, folder + vid_ext[ind])
    vid_stats = vu.get_vid_stats(vid_path)
    meta_dict = {'filename': pic_file_name, 'height': vid_stats['height'], 'width': vid_stats['width'],
                 'depth': vid_stats['depth'],
                 'from_vid': True, 'org_fps': vid_stats['fps']}
    return meta_dict


if __name__ == '__main__':
    # read the program arguments - put the yaml full path as a program parameter
    assert len(sys.argv) == 2
    yaml_path = sys.argv[1]
    with open(yaml_path, 'r') as yaml_file:
        data = yaml.load(yaml_file, Loader=yaml.SafeLoader)

    # yaml args
    logging_path = data['logging_path']
    project_name = data['project_name']
    data_path = data['data_path']
    out_dir_path = data['out_dir_path']
    save_marked = data.get('save_marked_img', False)
    exchange_labels_dict = data.get('exchange_labels', {})
    colors = data.get('colors', None)
    remove_labels_hard = [l.lower() for l in data.get('remove_labels_hard', [])]
    remove_labels_soft = [l.lower() for l in data.get('remove_labels_soft', ['car'])]
    keep_labels_soft = [l.lower() for l in data.get('keep_labels_soft', ['none_relevant'])]
    filter_labels = [l.lower() for l in data.get('filter_labels', [])]

    track_label_prefix = data.get('track_label_prefix', None)
    allow_multi_label = data.get('multi_label', False)
    marked_img_dir = os.path.join(out_dir_path, 'marked_imgs')

    frame_h, xmin_h, ymin_h, xmax_h, ymax_h, label_h, id_h = EVAL_SYSTEM_GT_CSV_HEADERS

    # create the logs folder
    cf.create_folder(logging_path)
    # create the output folder
    cf.create_folder(out_dir_path)
    data_list = get_mongo_data2.get_mongo_data_dicts(project_name, logging_path, colors=colors,
                                                     track_label_prefix=track_label_prefix,
                                                     allow_multi_labels=allow_multi_label,
                                                     exchange_labels_dict=exchange_labels_dict)
    # for each folder in the project we need to make csv
    # first need to divide the data_dict into sub lists
    # gt score will be 1 always
    folders_dict = defaultdict(list)
    for element in data_list:
        folder = os.path.dirname(element[0])
        # append list of dicts for all gt boxes in image
        folders_dict[folder].append(element[1])
    # now we all the data ordered by folder
    for folder, folder_data in folders_dict.items():
        # sort the folder data by frame number
        folder_data = sorted(folder_data, key=lambda x: int(os.path.splitext(x[0]['filename'])[0]))
        # flag to check if csv is empty or not
        is_empty = True
        csv_path = os.path.join(out_dir_path, folder) + '.csv'
        # check img folder or video exists in local files
        item_names, item_exts = map(list, zip(*[os.path.splitext(item) for item in os.listdir(data_path)]))
        if folder not in item_names:
            print('folder/video with name: %s does not exist in local files so will be skipped' % folder)
            continue

        height, width = None, None
        ind = item_names.index(folder)
        is_vid = False if not item_exts[ind] else True

        if is_vid:
            vid_path = os.path.join(data_path, folder + item_exts[ind])
            vid_stats = vu.get_vid_stats(vid_path)
            height = vid_stats['height']
            width = vid_stats['width']

        with open(csv_path, 'w') as f:
            writer = csv.DictWriter(f, fieldnames=EVAL_SYSTEM_GT_CSV_HEADERS)
            writer.writeheader()
            for ind, pic in enumerate(folder_data):
                if len(pic) == 0:
                    continue
                # get width and height of the pic
                pic_file_name = pic[0]['filename']
                frame_num = iu.get_frame_numbers_from_names_list([pic_file_name])[0]

                if not is_vid:
                    # img folder - get img size
                    img_path = os.path.join(data_path, folder, pic_file_name)
                    if not os.path.exists(img_path):
                        # TODO: log this
                        print("%s does not exist!" % img_path)
                        continue
                    height, width, depth = iu.get_img_size_and_depth(img_path)

                is_empty = False

                for tag_data in pic:
                    # remove ignored labels
                    label_list = tag_data['label'].split(',')
                    # copy of the label_list for deletion while iterate over the list
                    remain_labels = copy.deepcopy(label_list)

                    # remove labels
                    remain_labels = ut.filter_labels_list(remain_labels, lbls_to_remove=remove_labels_hard,
                                                          lbls_to_filter=filter_labels,use_label_prefix=False,
                                                          verbose=False)

                    # remove and keep some labels in case of conflict (multi labels)
                    # e.g remain_labels=['car','truck'] and remove_labels_soft=['car'] then we remove the car label
                    # so we will have only the truck.
                    # use this func only if there are more than 1 label
                    if len(remain_labels) > 1:
                        remain_labels = ut.filter_labels_list(remain_labels, lbls_to_remove=remove_labels_soft,
                                                              lbls_to_keep=keep_labels_soft, verbose=True)

                    if len(remain_labels) != 1:
                        print('Skipping labels %s in img %s' % (remain_labels, os.path.join(folder, pic_file_name)))
                        continue

                    label = remain_labels[0]
                    xmin = int(round(float(tag_data['xmin']) / 100 * width))
                    xmax = int(round(float(tag_data['xmax']) / 100 * width))
                    ymin = int(round(float(tag_data['ymin']) / 100 * height))
                    ymax = int(round(float(tag_data['ymax']) / 100 * height))

                    # TODO: remove after only a patch
                    # bbox=[xmin,ymin,xmax,ymax]
                    # skip,new_bbox = is_filter_tag(folder,bbox,0.2)
                    # if skip:
                    #     print('filter tag for folder %s. org_bbox: %s' % (folder,bbox))
                    #     continue
                    # if bbox!=new_bbox:
                    #     print('changed tag for folder %s. from: %s to %s'%(folder,bbox,new_bbox))
                    #     xmin,ymin,xmax,ymax = new_bbox
                    ####

                    angle = int(tag_data['angle'])
                    if angle != 0:
                        # calculate rotated rectangle and then calculate the axis align bounding box of the rotated one
                        rotated_bbox = get_rotated_rectangle_corners([xmin, ymin, xmax, ymax], angle)
                        xmin = rotated_bbox[:, 0].min()
                        xmax = rotated_bbox[:, 0].max()
                        ymin = rotated_bbox[:, 1].min()
                        ymax = rotated_bbox[:, 1].max()
                    row_dict = {}
                    row_dict[frame_h] = frame_num
                    row_dict[label_h] = label
                    row_dict[xmin_h] = str(xmin)
                    row_dict[xmax_h] = str(xmax)
                    row_dict[ymin_h] = str(ymin)
                    row_dict[ymax_h] = str(ymax)
                    row_dict[id_h] = tag_data['track_id']
                    writer.writerow(row_dict)

        if is_empty:
            os.remove(csv_path)
        elif save_marked:
            # create the saved dir
            out_img_dir = os.path.join(marked_img_dir, folder)
            cf.create_folder(out_img_dir)
            img_dir = os.path.join(data_path, folder)
            plot_tags_from_csv(img_dir, csv_path, out_img_dir)
