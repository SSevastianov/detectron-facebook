# -*- coding: utf-8 -*-

import os
import sys
from os.path import join as pjoin
import cv2
import yaml
import data_tools.DataManipulation.common_funcs as cf
from collections import defaultdict
import intervals as I
from data_tools.MongoToTFR.reporter import Reporter
import webcolors
import data_tools.MongoToTFR.general_utils as ut
import data_tools.MongoToTFR.draw_util as du
from data_tools.utils.xml_util import XmlParser, find_xml_dir, get_xml_fname
import data_tools.utils.img_util as iu
from enum import Enum
from collections import OrderedDict

# helper class for the analyzer
# define the folder type (by its content)
class FolderType(Enum):
    img = 1
    xml = 2
    other = 3


# TODO(guy): move this to more general place
APPEARANCE_LEVEL_MAP = OrderedDict({'-1': 'unknown', '0': 'easy', '1': 'moderate', '2': 'hard'})


class Analyzer:

    def __init__(self, params_dict):
        # params_dict is the dictionary from the yaml file

        # must fields
        # can also be the xmls folder
        self.data_dir = os.path.abspath(params_dict['DATA_DIR'])
        self.output_dir = os.path.abspath(params_dict['OUTPUT_DIR'])

        # optional fields
        self.save_marked_img = params_dict.get('SAVE_MARKED_IMG', True)
        self.save_type = params_dict.get('SAVE_TYPE', 'full')  # crop or full
        self.save_separation_method = params_dict.get('SAVE_SEPARATION_METHOD', 'folder')
        self.is_multi_labels = params_dict.get('MULTI_LABELS', False)
        self.crop_padding_size = params_dict.get('PAD', 0)
        self.blacklist = params_dict.get('BLACK_LIST', [])+['xmls']
        self.whitelist = params_dict.get('WHITE_LIST', None)
        self.summary_csv_filename = params_dict.get('STATS_FNAME', 'stats_log.csv')
        # make sure the statistics file is a csv
        if not self.summary_csv_filename.lower().endswith('.csv'):
            self.summary_csv_filename += '.csv'
        self.channel = params_dict.get('CHANNEL', 'N/A')
        self.bbox_min_side_len = params_dict.get('BB_MIN_SIDE_LEN', 10)  # only relevant for drawing and saving crops
        self.colors_labels = params_dict.get('COLORS_LABELS', [])
        self.correct_labels = [l.lower() for l in params_dict.get('CORRECT_LABELS', [])]
        self.label_to_color = params_dict.get('LABEL_TO_COLOR', {})
        self.exchange_labels_dict = params_dict.get('EXCHANGE_LABELS', {})
        self.use_nms = params_dict.get('USE_NMS', False)
        self.nms_iou_th = params_dict.get('NMS_IOU_THRESHOLD', 0.0)
        self.xml_struct = params_dict.get('XML_STRUCT', {XmlParser.DETECTION_KEY: 'object',
                                                         XmlParser.ID_KEY: 'id',
                                                         XmlParser.YMIN_KEY: 'bndbox/ymin',
                                                         XmlParser.XMIN_KEY: 'bndbox/xmin',
                                                         XmlParser.YMAX_KEY: 'bndbox/ymax',
                                                         XmlParser.XMAX_KEY: 'bndbox/xmax',
                                                         XmlParser.ANGLE_KEY: 'bndbox/angle',
                                                         XmlParser.LABEL_KEY: ['class', 'type', 'name'],
                                                         XmlParser.OCCLUSION_LEVEL_KEY: 'occluded',
                                                         XmlParser.APPEARANCE_LEVEL_KEY: 'difficult',
                                                         XmlParser.COLOR_KEY: 'color'})
        intervals_separation = params_dict.get('BBOX_MAX_SIDE_INTERVALS', [0, 10, 20, 30, 40, I.inf])

        # const fields
        self.base_headers = ['folder', 'filename', 'from_vid', 'org_fps', 'channel', 'has_xml', 'filtered', 'valid',
                             'width', 'height',
                             'depth', 'GSD', 'range', 'fov', 'total_tags',
                             'small_targets_dropped']  # TODO: not dynamic if changed it will throw exception
        # the label to color key for default color if label is not in the keys list
        self.default_color_key = 'default'

        # add identity map to labels in exchange dict
        for l in self.correct_labels + self.colors_labels:
            self.exchange_labels_dict[l] = l

        # change color names to BGR tuple
        for label, color_name in self.label_to_color.items():
            rgb_tuple = webcolors.name_to_rgb(color_name)
            bgr_tuple = rgb_tuple[::-1]
            self.label_to_color[label] = bgr_tuple

        # create logs dir inside the output dir
        self.log_dir = pjoin(self.output_dir, 'logs')
        cf.create_folder(self.log_dir)

        # create the correct format interval list
        intervals_list = []
        if len(intervals_separation) == 1:
            # in case user provide only 1 number then define interval from 0 to number
            intervals_separation.insert(0, 0)
        for i in range(1, len(intervals_separation)):
            start = intervals_separation[i - 1]
            end = intervals_separation[i]
            if end == 'inf':
                end = I.inf
            interval = I.closedopen(start, end)
            intervals_list.append(interval)

        self.bbox_max_side_intervals = intervals_list

        # create the csvs
        csv_names_and_headers_dict = {
            'missing_xmls_folders': ['folder', 'num_missing_xmls'],
            'small_tags': ['file', 'tag_id', 'label', 'bbox_max_side_threshold'],
        }

        # csv logger
        self.csv_reporter = Reporter(self.log_dir)
        self.csv_reporter.create_csvs(csv_names_and_headers_dict)

        # xml parser
        self.xml_parser = XmlParser(self.log_dir, self.xml_struct)

        # build the summary csv headers and assign them as class member
        max_side_intervals_per_class = [label + I.to_string(interval).replace(',', '_') for label in self.correct_labels
                                        for interval in self.bbox_max_side_intervals]
        color_per_label = [color + '_' + label for label in self.correct_labels for color in self.colors_labels]
        self.summary_csv_headers = self.base_headers + self.correct_labels + color_per_label + max_side_intervals_per_class

    def exchange(self, label):
        label_lower = label.lower()
        return self.exchange_labels_dict.get(label_lower, None)

    def add_summary_csv_entry(self, img_dict):
        data_row = [img_dict[key] for key in self.summary_csv_headers]
        self.csv_reporter.report_csv(data_row, self.summary_csv_filename, header=False)

    # DRAW FUNCS
    def plot_multi_labels_and_save(self, img_data, saving_dir):
        tags_dict = img_data['tags']
        img_fname = img_data['filename']
        img_path = img_data['img_path']

        # Making sure that saving dir exists.
        cf.create_folder(saving_dir)
        # new saved image name
        f_root, f_ext = img_fname.split('.')
        new_fname = '_'.join([f_root, 'marked']) + f_ext

        img = cv2.imread(img_path)
        if len(tags_dict.values()) != 1:
            print("Error: can not have more than 1 bbox for multi labels")
        label_names = tags_dict.values()[0][XmlParser.LABEL_KEY]
        # Saving
        if self.save_separation_method == 'folder':
            # write the labels on the image
            img = cv2.copyMakeBorder(img, 20, 0, 120, 120, cv2.BORDER_CONSTANT, value=[0, 0, 0])
            cv2.putText(img, str(label_names), (0, 10), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (255, 255, 255), 1)
            saving_path = os.path.join(saving_dir, new_fname)
            cv2.imwrite(saving_path, img)
        else:
            # class
            label_names = label_names.replace('/', '_').split(',')
            for label in label_names:
                label_dir = os.path.join(saving_dir, label)
                cf.create_folder(label_dir)
                saving_path = os.path.join(label_dir, new_fname)
                cv2.imwrite(saving_path, img)

    def save_crop_from_image(self, img_data, saving_dir):
        # Making sure that saving dir exists.
        cf.create_folder(saving_dir)
        tags_dict = img_data['tags']
        img_path = img_data['img_path']
        image = cv2.imread(img_path)

        for id, tag in tags_dict.items():
            tl_point = (int(tag[XmlParser.XMIN_KEY]), int(tag[XmlParser.YMIN_KEY]))
            br_point = (int(tag[XmlParser.XMAX_KEY]), int(tag[XmlParser.YMAX_KEY]))
            label = tag[XmlParser.LABEL_KEY]
            if label.lower() in self.correct_labels:
                continue
            dy, dx = abs(tl_point[0] - br_point[0]), abs(
                tl_point[1] - br_point[1])
            # check if bbox is too small
            if (dx < self.bbox_min_side_len) and (dy < self.bbox_min_side_len):
                continue
            # calculate the cropped image
            crop_img = image[max(0, tl_point[1] - self.crop_padding_size):br_point[1] + self.crop_padding_size,
                       max(0, tl_point[0] - self.crop_padding_size):br_point[0] + self.crop_padding_size + 1,
                       :]
            # save the image
            f_root, f_ext = os.path.splitext(img_data['filename'])
            new_fname = '_'.join([f_root, str(id)]) + f_ext
            saving_path = os.path.join(saving_dir, new_fname)
            cv2.imwrite(saving_path, crop_img)

    def test_bbox_max_side(self, img_data, tag_id):
        tag = img_data['tags'][tag_id]
        bbox_max_side = max(tag[XmlParser.XMAX_KEY] - tag[XmlParser.XMIN_KEY],
                            tag[XmlParser.YMAX_KEY] - tag[XmlParser.YMIN_KEY])
        if bbox_max_side < self.bbox_min_side_len:
            message = "file: {}, tag_id:{}, label: {}, bbox max side is smaller than {} pixels"
            message_vars = [img_data['img_path'], tag_id, tag[XmlParser.LABEL_KEY], self.bbox_min_side_len]
            self.csv_reporter.logger(message, message_vars, 'small_tags')
            # Update the img_data stats on small tags
            img_data['small_targets_dropped'] += 1

    def update_img_stats(self, img_data):
        img_tags = img_data['tags']
        # get the general statistics of the image tags, such as label,color and bbox max size counts
        img_stats = defaultdict(int)
        for tag_id, tag in img_tags.items():
            labels = tag[XmlParser.LABEL_KEY].split(',')
            color = tag[XmlParser.COLOR_KEY]
            bbox_max_side = max(tag[XmlParser.XMAX_KEY] - tag[XmlParser.XMIN_KEY],
                                tag[XmlParser.YMAX_KEY] - tag[XmlParser.YMIN_KEY])
            self.test_bbox_max_side(img_data, tag_id)
            # update the total number of tags for this class
            for label in labels:
                img_stats[label] += 1
                if color:
                    img_stats[color + '_' + label] += 1
                # update the total number of tags
                img_stats['total_tags'] += 1
                # find the correct interval
                for interval in self.bbox_max_side_intervals:
                    if bbox_max_side in interval:
                        # update also the relevant interval
                        img_stats[label + I.to_string(interval).replace(',', '_')] += 1
        img_data.update(img_stats)

    def update_img_data_tags(self, img_data):
        xml_path = img_data['xml_path']
        # get the tags data from xml
        tags = self.xml_parser.parse_xml_tags(xml_path)
        if not tags:
            return
        img_data['has_xml'] = 1
        # apply labels exchange if there is a labels dictionary
        if self.exchange_labels_dict:
            self.xml_parser.apply_labels_exchange(tags, self.exchange_labels_dict)
        # validate the tags by the correct label list
        self.xml_parser.validate_correct_labels(xml_path, tags, self.summary_csv_headers)
        valid_tags, unvalid_tags = self.xml_parser.split_tags_by_validation(tags)
        # assign the valid tags only
        img_data['tags'] = valid_tags

    def get_img_tags_dict(self, xml_path, img_path, image_size):
        img_data = {}
        # init the img_data
        for key in self.summary_csv_headers:
            img_data[key] = 0

        img_data['filename'] = os.path.basename(img_path)
        img_data['folder'] = os.path.relpath(os.path.dirname(img_path), self.data_dir)
        img_data['xml_path'] = xml_path
        img_data['img_path'] = img_path
        img_data['channel'] = self.channel
        vid_meta = self.xml_parser.parse_xml_vid_meta(xml_path)
        img_data['from_vid'] = vid_meta['from_vid']
        img_data['org_fps'] = vid_meta['org_fps']

        # image size
        if image_size:
            height, width, depth = image_size
        else:
            # get the img shape from xml file
            height, width, depth = self.xml_parser.parse_xml_img_size(xml_path)
        img_data['width'] = width
        img_data['height'] = height
        img_data['depth'] = depth

        img_data['valid'] = 1
        img_data['tags'] = {}

        try:
            # update the img tags in img data
            self.update_img_data_tags(img_data)
            # update the img_data dict with the image data statistics
            self.update_img_stats(img_data)
        except Exception as e:
            raise ValueError('Got Error in file: %s, error: %s' % (img_data['img_path'], e))

        return img_data

    def filter_by_nms(self, tags, priority_field='occluded', priority_higher_better=False):
        new_tags = {}
        keep_tag_ids = ut.filter_tags_by_nms(tags, nms_iou_th=self.nms_iou_th, priority_field=priority_field,
                                             priority_higher_better=priority_higher_better)
        # build the filtered tags (only the tags to keep after apply nms filter)
        for tag_id in keep_tag_ids:
            new_tags[tag_id] = tags[tag_id]
        return new_tags

    def filter_by_bbox_side(self, tags):
        # make 1 default interval for all classes
        default_interval_key = 'default'
        bbox_filter_interval_per_class = {default_interval_key: I.closedopen(self.bbox_min_side_len, I.inf)}
        keep_tag_ids = ut.filter_tags_by_bbox_side(tags, bbox_filter_interval_per_class, default_interval_key)
        keep_tags, drop_tags = ut.split_tags(tags, keep_tag_ids)
        return keep_tags, drop_tags

    def get_xmls_dir(self, images_rel_path, folder_data):
        images_dir = pjoin(self.data_dir, images_rel_path)
        res = find_xml_dir(images_dir)
        if res:
            # found the xmls dir
            xml_dir, xmls_flist = res
            xmls_count = len(xmls_flist)
            folder_data['xmls'] = xmls_count
            if xmls_count < folder_data['images']:
                message = "Images count in {} does not match xmls count, missing {} XMLs"
                message_vars = [images_rel_path, folder_data['images'] - xmls_count]
                self.csv_reporter.logger(message, message_vars, 'missing_xmls_folders')
            return xml_dir

        # couldn't find the xmls dir
        folder_data['xmls'] = 0
        message = "No XML subfoler found for dir {}, missing {} XMLs"
        message_vars = [images_rel_path, folder_data['images']]
        self.csv_reporter.logger(message, message_vars, 'missing_xmls_folders')
        print(message.format(*message_vars))
        self.csv_reporter.report(message.format(*message_vars), 'log.txt', self.log_dir)
        return None

    def generate_dir_dic(self):
        return {'images': 0, 'xmls': 0, 'labels': [], 'max_side': [], 'spatial_shape': set()}

    def get_folder_type(self, dir_path):
        # Check for image files or xml files
        relevant_files_list = []
        for fname in os.listdir(dir_path):
            fpath = pjoin(dir_path, fname)
            if cf.is_pic(fpath):
                # found img
                return FolderType.img
            elif cf.is_xml(fpath):
                relevant_files_list.append(FolderType.xml)
        if FolderType.xml in relevant_files_list:
            return FolderType.xml
        # folder doesn't contain images or xmls
        return FolderType.other

    def analyze_data(self):
        # create the summary excel file
        self.csv_reporter.report_csv(self.summary_csv_headers, self.summary_csv_filename, header=True)

        _folders_data = defaultdict(self.generate_dir_dic)
        for root, dirs, files in os.walk(self.data_dir):

            # Whitelist filtering
            if not ut.whitelist_filtering(self.whitelist, root):
                continue

            # Removing black-listed directories
            ut.blacklist_filtering(self.blacklist,root,dirs)

            # get the folder type
            folder_type = self.get_folder_type(root)

            if folder_type == FolderType.other:
                # skip this folder
                continue

            rel_path = '' if root == self.data_dir else os.path.relpath(root, self.data_dir)

            if folder_type == FolderType.img:
                # img folder - regular run
                relevant_fname_list = [fname for fname in files if cf.is_pic(pjoin(root, fname))]
                relevant_fname_list.sort()
                _folders_data[rel_path]['images'] = len(relevant_fname_list)
                # find the xml sub dir
                xml_dir = self.get_xmls_dir(rel_path, _folders_data[rel_path])
                if xml_dir is None:
                    # TODO: log this
                    print('folder: %s contains only image files and no xml!' % (root))
                    continue
                # remove the xml dir from the loop so it won't evaluate the tags twice
                dirs.remove(os.path.basename(xml_dir))

            else:
                # xml folder
                # no visualization is supported
                relevant_fname_list = [fname for fname in files if cf.is_xml(pjoin(root, fname))]
                relevant_fname_list.sort()
                _folders_data[rel_path]['images'] = 0
                xml_dir = root

            # define the save dir
            if self.save_separation_method == 'folder':
                saving_dir = pjoin(self.output_dir, rel_path)
            elif self.save_separation_method == 'class':
                saving_dir = self.output_dir
            else:
                raise ValueError('unknown save separation method. please choose between folder and class')

            for ind, fname in enumerate(relevant_fname_list):
                # Get matching xml file name
                xml_fname = get_xml_fname(fname)
                xml_path = pjoin(root, xml_dir, xml_fname)

                # Parse the XML and save it as a dictionary
                file_path = pjoin(root, fname)
                img_size = None
                # in case of xml file it will assign img path that doesn't exist
                img_path = os.path.splitext(file_path)[0] + iu.DEFAULT_IMG_EXT
                if cf.is_pic(file_path):
                    img_size = iu.get_img_size_and_depth(file_path)
                    img_path = file_path

                img_data = self.get_img_tags_dict(xml_path, img_path,
                                                  image_size=img_size)
                if img_data is None:
                    continue
                # filter by nms
                if self.use_nms:
                    # update the new tags
                    img_data['tags'] = self.filter_by_nms(img_data['tags'])
                    # update the img stats
                    self.update_img_stats(img_data)
                # TODO(guy): use the draw util plot funcs
                # filter small bbox tags
                tags, drop_tags = self.filter_by_bbox_side(img_data['tags'])
                # write img data to the excel file
                self.add_summary_csv_entry(img_data)

                if self.save_marked_img and len(img_data['tags']) > 0:
                    if self.is_multi_labels:
                        # classification
                        self.plot_multi_labels_and_save(img_data, saving_dir)
                    elif self.save_type == 'crop':
                        # classification
                        self.save_crop_from_image(img_data, saving_dir)
                    else:
                        # detection
                        du.plot_bboxes_and_save(img_path, tags, drop_tags, self.label_to_color, saving_dir,
                                                default_color_val=self.label_to_color[self.default_color_key],
                                                thickness=1, helper_tag_field='appearance',
                                                helper_tag_map=APPEARANCE_LEVEL_MAP)


def main(yaml_path):
    with open(yaml_path, 'r') as yaml_file:
        params_dict = yaml.load(yaml_file, Loader=yaml.SafeLoader)
    analyzer = Analyzer(params_dict)
    analyzer.analyze_data()


if __name__ == '__main__':
    # read the program arguments - put the yaml full path as a program parameter
    assert len(sys.argv) == 2
    yaml_path = sys.argv[1]
    main(yaml_path)
