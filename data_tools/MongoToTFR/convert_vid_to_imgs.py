import os
import sys
from data_tools.DataManipulation import common_funcs as cf
from data_tools.MongoToTFR.create_xml_from_mongo import move_xmls_to_pic_folder
from data_tools.MongoToTFR import get_mongo_data2 as mongo_data
import yaml
import subprocess
import numpy as np


def convert_vid_to_imgs(vid_path, frame_num_list, out_dir, fps):
    frame_num_list_str = [str(i) for i in frame_num_list]
    frame_num_list_str = ' '.join(frame_num_list_str)
    bashCommand = '''for i in {frames}; do ffmpeg -y -an -ss `echo $i/{fps} | bc -l | xargs printf "%f"` -i '{vid_path}'  -q:v 1 -frames:v 1 -start_number $i '{out_dir}/%d.jpg' ; done'''.format(
        frames=frame_num_list_str, fps=fps, vid_path=vid_path, out_dir=out_dir)
    process = subprocess.Popen(bashCommand, stdout=None, stderr=None, shell=True, executable='/bin/bash')
    return process


def create_vid_name_fps_dict(project_name):
    # helper function to convert the format of the video dictionary used by the mongo object to
    # dictionary of name:fps pairs

    # get the mongo videos dict in format 'vid_id':{'name':x,'fps':y}
    def get_mongo_vid_dict(project_name):
        mdb = mongo_data.MongoDB()
        with mdb:
            db = mdb.get_db()
            zone_id = mongo_data.get_project_zone_id(db, project_name)
            vid_dict = mongo_data.get_videos_dict(db, zone_id)
        return vid_dict

    mongo_vid_dict = get_mongo_vid_dict(project_name)
    name_fps_dict = {}
    for v in mongo_vid_dict.values():
        name_fps_dict[v['name']] = v['fps']
    return name_fps_dict


def convert_vids_dir_to_img_dirs(vid_dir, xmls_path, output_dir, project_name, max_num_processes=5):
    def wait_for_processes(process_list):
        exit_codes = np.array([p.wait() for p in process_list])
        if np.all(exit_codes != 0):
            # got problem
            raise ValueError('Not all the video conversion succeed')

    # get video name-fps dict
    vid_dict = create_vid_name_fps_dict(project_name)
    process_list = []
    for vid in os.listdir(vid_dir):
        vid_path = os.path.join(vid_dir, vid)
        vid_name, vid_ext = os.path.splitext(vid)
        if not vid_name in os.listdir(xmls_path):
            print("couldn't find xmls for video: %s" % vid)
            continue
        xml_dir = os.path.join(xmls_path, vid_name)
        frame_num_list = [int(os.path.splitext(xml_name)[0]) for xml_name in os.listdir(xml_dir)]
        out_dir = os.path.join(output_dir, vid_name)
        # create out dir
        cf.create_folder(out_dir)
        # convert_vid_to_imgs(vid_path, frame_num_list, out_dir)
        p = convert_vid_to_imgs(vid_path, frame_num_list, out_dir, vid_dict[vid_name])
        process_list.append(p)
        if len(process_list) >= max_num_processes:
            # wait for all the subprocesses of the video converting
            wait_for_processes(process_list)
            process_list = []
    # wait for all the subprocesses of the video converting
    wait_for_processes(process_list)


def main(yaml_path):
    with open(yaml_path, 'r') as yaml_file:
        data = yaml.load(yaml_file, Loader=yaml.SafeLoader)

    vid_dir = data['data_path']
    project_name = data['project_name']
    xmls_path = data['xml_path']
    output_dir = data['output_dir']

    convert_vids_dir_to_img_dirs(vid_dir, xmls_path, output_dir, project_name, max_num_processes=5)
    print('Done creating images. Now starting to move xmls to pic folders')
    # move xmls to the pics folders
    move_xmls_to_pic_folder(output_dir, xmls_path)
    print('Finished')


if __name__ == '__main__':
    # read the program arguments - put the yaml full path as a program parameter
    assert len(sys.argv) == 2
    yaml_path = sys.argv[1]
    main(yaml_path)
