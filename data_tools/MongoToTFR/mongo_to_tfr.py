import os
import sys
from data_tools.MongoToTFR import data_analysis, create_xml_from_mongo, \
    generate_TFR_from_xmls, convert_vid_to_imgs

relevant_script_flow = [{'func': create_xml_from_mongo.main, 'prefix': 'create_xml_from_mongo'},
                        {'func': convert_vid_to_imgs.main, 'prefix': 'convert_vid_to_imgs'},
                        {'func': data_analysis.main, 'prefix': 'data_analysis'},
                        {'func': generate_TFR_from_xmls.main, 'prefix': 'generate_TFR_from_xmls'}]

if __name__ == '__main__':
    # program param is a dir with yaml files
    assert len(sys.argv) == 2
    yamls_dir = sys.argv[1]
    yaml_files = os.listdir(yamls_dir)
    for script_args in relevant_script_flow:
        method = script_args['func']
        script_prefix = script_args['prefix']
        curr_yaml_files = [os.path.join(yamls_dir, f) for f in yaml_files if f.startswith(script_prefix)]
        for y_f in curr_yaml_files:
            # activate the function on the yaml path
            print('------Start %s------' % (y_f))
            method(y_f)
            print('------End %s------' % (y_f))
