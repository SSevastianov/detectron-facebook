import os


class Reporter:

    def __init__(self, log_dir):
        self.log_dir = log_dir

    def create_csvs(self, csv_names_and_headers_dict):
        for logfile_name, headers in csv_names_and_headers_dict.items():
            if not logfile_name.endswith('.csv'):
                logfile_name += '.csv'
            self.report_csv(headers, logfile_name, header=True)

    def delete_csvs(self, csv_names):
        for logfile_name in csv_names:
            if not logfile_name.endswith('.csv'):
                logfile_name += '.csv'
            file_path = os.path.join(self.log_dir, logfile_name)
            if os.path.exists(file_path):
                os.remove(file_path)

    def report(self, message, logfile_name, header=False):
        mode = 'w' if header else 'a'
        log_path = os.path.join(self.log_dir, logfile_name)
        with open(log_path, mode) as log_obj:
            log_obj.write(message + '\n')

    def report_csv(self, message_vars, logfile_name, header=False):
        message_str = ','.join(str(var).replace(',', ' | ') for var in message_vars)
        file_path = os.path.join(self.log_dir, logfile_name)
        if not logfile_name.endswith('.csv') and not os.path.exists(file_path):
            # try to add '.csv' in the end
            logfile_name += '.csv'
        self.report(message_str, logfile_name, header)

    def logger(self, message, message_vars, fname, log_csv=True, log_stdout=True):
        if log_stdout:
            print(message.format(*message_vars))
        if log_csv:
            self.report_csv(message_vars, fname)
