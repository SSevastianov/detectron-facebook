from __future__ import print_function

from pymongo import MongoClient
from sshtunnel import SSHTunnelForwarder
import os
from os.path import join as pjoin
import random
from collections import defaultdict, OrderedDict

# get mongo data with nested labels

MONGO_HOST = '206.189.141.19'
MONGO_PORT = 27017
MONGO_DB = 'olympus'
MONGO_USER = 'root'
MONGO_PASS = 'G#hG(/@9*b6"'

random.seed(1984)


def get_local_folder_list(path, subdir_whtlst=None):
    folders = [pjoin(path, new_path) for new_path in set(os.listdir(path)) if
               (os.path.isdir(pjoin(path, new_path))) and (new_path in subdir_whtlst)]
    return folders


def get_project_zone_id(db, project_name):
    cursor = db.get_collection('zones').find({'name': project_name})
    zone_id = str(cursor.next()['_id'])
    return zone_id


def get_labels_dict(db, zone_id):
    cursor = db.get_collection('labels').find({'zone_id': zone_id})
    objects = list(cursor)
    labels = {}
    labels_set = OrderedDict()

    for obj in objects:
        labels[str(obj['_id'])] = str(obj['title'])
        labels_set[str(obj['title'])] = None

    return labels, labels_set


def create_labels_stats(data_path, label_stats_by_folder, name):
    project_label_stats = defaultdict(int)
    for folder in label_stats_by_folder.keys():
        curr_folder_stats = label_stats_by_folder[folder]
        for label, count in curr_folder_stats.iteritems():
            project_label_stats[label] += count

    full_path = pjoin(data_path, 'counted_' + name + '_labels.txt')
    with open(full_path, 'w') as f:
        for label, count in project_label_stats.iteritems():
            f.write(label + ':' + str(count) + '\n')

    return full_path


def write_labels_stats(data_path, label_stats_by_folder, name):
    label_stats_path = pjoin(data_path, 'counted_' + name + '_labels.txt')
    with open(label_stats_path, 'w') as f:
        for folder in sorted(label_stats_by_folder.keys()):
            curr_folder_stats = label_stats_by_folder[folder]
            for label, count in curr_folder_stats.iteritems():
                f.write(folder + ', ' + label + ':' + str(count) + '\n')
    return label_stats_path


def create_label_map(project_name, labels_set, output_path):
    label_num = 1
    output_file = project_name + '_label_map.pbtxt'
    full_path = pjoin(output_path, output_file)
    with open(full_path, 'w') as f:
        for key in labels_set:
            line = "item {\n  id: " + str(label_num) + "\n  name: '" + key + "'\n}\n\n"
            label_num += 1
            f.write(line)
    print('Created label map, path: ', full_path)
    return full_path


def get_folder_dict(db, zone_id):
    cursor = db.get_collection('folders').find({'zone_id': zone_id})
    objects = list(cursor)
    folders = {}

    for obj in objects:
        folders[str(obj['_id'])] = str(obj['name'])

    return folders


def get_videos_dict(db, zone_id):
    # return a dict of vid_id : vid_name pairs
    cursor = db.get_collection('videos').find({'zone_id': zone_id})
    db_vid_dicts = list(cursor)
    all_vids = {}
    for vid_dict in db_vid_dicts:
        if 'fps' in vid_dict:
            all_vids[str(vid_dict['_id'])] = {'name': str(vid_dict['name']), 'fps': vid_dict['fps']}
    return all_vids


def get_images_dict(db, zone_id):
    # return a dict of dicts in format- img_id : {img file name and folder name}
    cursor = db.get_collection('images').find({'zone_id': zone_id})
    db_images_dicts = list(cursor)
    all_images = {}
    # folders is a dictionary, key = folder id, value = folder name
    folders = get_folder_dict(db, zone_id)
    print('Got list of', len(folders), 'folders from data base')

    for img_dict in db_images_dicts:
        all_images[str(img_dict['_id'])] = {'filename': str(img_dict['filename']),
                                            'foldername': folders[str(img_dict['folder_id'])]}
        # if (pjoin(data_path, folders[str(img_dict['folder_id'])]) in local_data) and\
        #         (str(img_dict['filename']) in os.listdir(pjoin(data_path, folders[str(img_dict['folder_id'])]))):
        #     local_images[str(img_dict['_id'])] = all_images[str(img_dict['_id'])]
    return all_images


def test_labels(labels_set, filekey, logging_path):
    if labels_set is None:
        # TODO: log this
        print("Error, tag_dict['attributes'] is None! file : {}".format(filekey))
        with open(pjoin(logging_path, 'files_with_bad_tags.txt'), "a") as text_file:
            text_file.write(filekey + '\n')
        return False
    if not labels_set:
        # TODO: review this.
        # -- for future logging --
        # print("The file {}/{} has no transformed labels".format(foldername, filename))
        # print("The labels are: ", [labels[label_id] for attribute in tag_dict['attributes']])
        # path = os.path.abspath(foldername + '/' + filename)
        with open(pjoin(logging_path, 'not_labeled_files.txt'), "a") as text_file:
            text_file.write(filekey + '\n')
        return False  # when attributes list is empty go to next tag_dict
    return True


def get_tags_dict(db, zone_id, labels, zone_images, logging_path, colors=None, track_label_prefix=None,
                  allow_multi_labels=False, exchange_labels_dict=None):
    '''

    :param db:
    :param zone_id:
    :param labels:
    :param all_images:
    :param local_images:
    :param logging_path:
    :param dict_exchange:
    :return:
    tags:  a default dict (list) with key = filekey (foldername/filename), value = tag dict.
    tag dict: keys = 'label','xmin','xmax','ymin','ymax'. Note that the values for 'xmin','xmax','ymin' and 'ymax'
    are strings!
    label_stats: a default dict (default dict(int)) where the keys are sub-folders names. The keys of
    the nested dict are the labels.
    label_stats_by_local_folder: a default dict (default dict(int)) where the keys are local (white listed) sub-folders
    names. The keys of the nested dict are the labels.
    '''
    # Obtaining tags, these can belong to an image in any zone sub-folder,
    cursor = db.get_collection('tags').find({'zone_id': zone_id})
    db_tags_dict = list(cursor)
    # get the video zone dict that map id to name
    zone_videos = get_videos_dict(db, zone_id)
    tags = defaultdict(list)
    # label_stats = defaultdict(lambda: defaultdict(int))

    # label_stats_by_local_folder = defaultdict(lambda: defaultdict(int)) #TODO: we should initialize the key? use ordered dict?

    # TODO: replace zone id with zone name.
    print('Found {} tags for zone {} '.format(len(db_tags_dict), zone_id))

    for tag_dict in db_tags_dict:
        vid_id = tag_dict.get('video_id', None)
        is_vid = bool(vid_id)
        img_id = tag_dict['image_id']
        if img_id is None:
            vid_dict = zone_videos.get(vid_id, None)
            vid_name = 'unknown'
            if vid_dict:
                vid_name = vid_dict['name']
            print('Got image_id as None for video: %s. Skipping this frame' % vid_name)
            continue
        foldername = zone_images[img_id]['foldername']
        filename = zone_images[img_id]['filename']
        filekey = os.path.join(foldername, filename)
        # labels mapped is a set of remapped labels
        labels_mapped = get_tag_labels(filekey, labels, tag_dict)
        if not test_labels(labels_mapped, filekey, logging_path): continue

        # exchange the labels
        if exchange_labels_dict:
            labels_mapped = [exchange_labels_dict.get(label, label) for label in labels_mapped]
            # eliminate duplication caused by the exchange
            labels_mapped = list(set(labels_mapped))
        color = ''
        if colors is not None:
            color = [label for label in labels_mapped if label in colors]
            num_colors = len(color)
            if num_colors > 0:
                # remove from labels list
                for clr in color:
                    labels_mapped.remove(clr)
                if num_colors > 1:
                    print('more than 1 color:%s for file: %s' % (color, os.path.join(foldername, filename)))
                # take the first one
                color = color[0]
            else:
                color = ''
        track_id = tag_dict.get('track_id', '')
        # below condition is for tagging track ids as a label in the tagging zone (depracated)
        if not track_id and track_label_prefix:
            track_id = [label for label in labels_mapped if label.startswith(track_label_prefix)]
            num_ids = len(track_id)
            if num_ids > 0:
                # remove from labels list
                for id in track_id:
                    labels_mapped.remove(id)
                if num_ids > 1:
                    print('more than 1 track id:%s for file: %s' % (track_id, os.path.join(foldername, filename)))
                # take the first one
                track_id = track_id[0]
            else:
                track_id = ''

        if len(labels_mapped) == 1:
            label = list(labels_mapped)[0]  # label is the label txt
        elif allow_multi_labels:
            label = ','.join(labels_mapped)
        else:
            print("The file {}/{} has more then one transformed label or no transformed label.".format(foldername,
                                                                                                       filename))
            print("The labels are: ", labels_mapped)
            continue

        angle = int(tag_dict.get('rotate', 0))

        tag = {'label': label, 'color': color, 'track_id': track_id, 'xmin': str(tag_dict['x1']),
               'xmax': str(tag_dict['x2']),
               'ymin': str(tag_dict['y1']),
               'ymax': str(tag_dict['y2']),
               'angle': angle,
               'is_vid': is_vid,
               'foldername': foldername, 'filename': filename}

        tags[filekey].append(tag)
        # if img_id in local_images:
        #     tag = {'label': label, 'xmin': str(tag_dict['x1']), 'xmax': str(tag_dict['x2']),
        #            'ymin': str(tag_dict['y1']), 'ymax': str(tag_dict['y2']),
        #            'foldername': foldername, 'filename': filename}
        #     tags[filekey].append(tag)
        #     label_stats_by_local_folder[foldername][label] += 1

    return tags


def hierarchy_labels_supression(labels_mapped, hierarchy):
    for elem in hierarchy:
        if elem in labels_mapped:
            labels_mapped_supressed = [elem]
            break
    return labels_mapped_supressed


def get_tag_labels(filekey, labels, tag_dict):
    # This function convert the attributes list to a set
    labels_mapped = set()
    if tag_dict['attributes'] is None:
        # TODO: log this
        return None
    else:
        for attribute in tag_dict['attributes']:
            label_id = str(attribute)
            if label_id not in labels:
                # TODO: add a report
                print("Error, attribute {} is not a valid label! file : {}".format(label_id, filekey))
                continue
            label_text = labels[label_id]

            labels_mapped.add(label_text.lower())
    return labels_mapped


# class to control the mongo db connection
class MongoDB:
    def __init__(self):
        self.server = SSHTunnelForwarder(
            MONGO_HOST,
            ssh_username=MONGO_USER,
            ssh_password=MONGO_PASS,
            remote_bind_address=('127.0.0.1', MONGO_PORT))
        self.db = None
        self.is_server_connected = False

    def __enter__(self):
        self.server.start()
        client = MongoClient(host='127.0.0.1', port=self.server.local_bind_port)
        self.db = client[MONGO_DB]
        self.is_server_connected = True

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.server.stop()
        self.db = None
        self.is_server_connected = False

    def get_db(self):
        if self.is_server_connected:
            return self.db
        else:
            print("Can't get database because server isn't connected")
            return None

    def is_server_connected(self):
        return self.is_server_connected


def get_mongo_data_dict(project_name, logging_path, colors=None, track_label_prefix=None, allow_multi_labels=False,
                        exchange_labels_dict=None):
    '''

    :param project_name:
    :param logging_path:
    :return:
    '''
    # TODO: add tqdm

    mdb = MongoDB()
    with mdb:
        db = mdb.get_db()
        zone_id = get_project_zone_id(db, project_name)
        print('Got zone_id:', zone_id)
        # labels - a dict where key = label_id, value = label_text
        # labels_set - an ordered dict where key = label_text, value = None
        labels, labels_set = get_labels_dict(db, zone_id)
        print('Got', len(labels_set), 'labels')
        # zone_images -  a dict where key = img_id (string), value = dict with keys:'filename' and 'foldername'
        # local_images - a dict where key = img_id (string), value = dict with keys:'filename' and 'foldername'
        zone_images = get_images_dict(db, zone_id)
        print('Got zone list of', len(zone_images), 'images')
        tags = get_tags_dict(db, zone_id, labels, zone_images, logging_path, colors=colors,
                             track_label_prefix=track_label_prefix,
                             allow_multi_labels=allow_multi_labels, exchange_labels_dict=exchange_labels_dict)
        print('got list of', len(tags), 'tags')

    # project_stats_path = create_labels_stats(data_path, label_stats, 'project')
    # local_stats_name = '_'.join(subdir_whtlst)
    # local_stats_path = write_labels_stats(data_path, label_stats_by_local_folder, 'local_'+local_stats_name)
    # print('Created project label stats at ', project_stats_path)
    # print('Created local folders label stats at ', local_stats_path)
    print('Done with mongo server')

    return tags


def get_mongo_data_dicts(project_name, logging_path, shuffle_tags_list=False, colors=None, track_label_prefix=None,
                         allow_multi_labels=False, exchange_labels_dict=None):
    '''
    :param project_name:
    :param data_path:
    :param logging_path:
    :param make_label_map:
    :param shuffle_tags_list:
    :param label_map_labels:
    :param excahnge_dict:
    :return:
    '''
    # TODO: add logging policy
    print('Getting Data from Mongo DB for zone: {}'.format(project_name))
    tags = get_mongo_data_dict(project_name, logging_path, colors=colors, track_label_prefix=track_label_prefix,
                               allow_multi_labels=allow_multi_labels, exchange_labels_dict=exchange_labels_dict)
    print('tags len', len(tags))
    tags_list = [[filekey, dict_list] for filekey, dict_list in tags.items()]
    if shuffle_tags_list:
        random.shuffle(tags_list)
    else:
        tags_list = sorted(tags_list, key=lambda tag_data: tag_data[0])  # sorting by filekey
    print('Image list length is', len(tags_list))
    print('Done getting mongo dicts')

    # if make_label_map and (label_map_labels is not None):
    #     label_map_path = create_label_map(project_name, label_map_labels, data_path)
    # else:
    #     label_map_path = None

    return tags_list
