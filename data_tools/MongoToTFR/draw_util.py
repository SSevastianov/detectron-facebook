from data_tools.utils.xml_util import XmlParser
import cv2
import data_tools.DataManipulation.common_funcs as cf
import os
from PIL import Image
import numpy as np
from data_tools.utils.box_util import get_rotated_rectangle_corners

drop_tag_color = (0, 0, 0)  # black

def get_marked_path(img_path, out_dir):
    # build path from out_dir,img filename with _marked at the end
    # img_path can be the img filename or relative path
    img_fname = os.path.basename(img_path)
    img_name, img_ext = os.path.splitext(img_fname)
    marked_fname = '_'.join([img_name, 'marked']) + img_ext
    out_path = os.path.join(out_dir, marked_fname)
    return out_path


def plot_bboxes_and_save(img_path, tags, drop_tags, label_to_color, saving_dir, default_color_val=(255, 255, 255),
                         thickness=3, helper_tag_field=None,helper_tag_map={}):
    # Plot bboxes on image
    new_img = plot_bboxes(img_path, tags, drop_tags, label_to_color, default_color_val, thickness,
                          helper_tag_field=helper_tag_field,helper_tag_map=helper_tag_map)
    # Saving
    out_img_path = get_marked_path(img_path, saving_dir)
    save_img(new_img, out_img_path)


def plot_bbox(image, tag, tag_color, thickness):
    tag_top_left_pixel = (int(tag[XmlParser.XMIN_KEY]), int(tag[XmlParser.YMIN_KEY]))
    tag_bottom_right_pixel = (int(tag[XmlParser.XMAX_KEY]), int(tag[XmlParser.YMAX_KEY]))
    angle = int(tag[XmlParser.ANGLE_KEY])
    if angle == 0:
        # regular rectangle
        cv2.rectangle(image, tag_top_left_pixel, tag_bottom_right_pixel, color=tag_color, thickness=thickness)
    else:
        # rotated rectangle
        box = list(tag_top_left_pixel)
        box.extend(tag_bottom_right_pixel)
        rotated_corners = get_rotated_rectangle_corners(box, angle)
        rotated_corners = rotated_corners.reshape((-1, 1, 2))
        cv2.polylines(image, [rotated_corners], isClosed=True, color=tag_color, thickness=thickness)


def add_helper_tag_text(image, tag, helper_field, helper_map={}):
    # helper map should be order dict
    helper_val = tag[helper_field]
    helper_text = str(helper_map.get(helper_val, helper_val))
    tag_top_left_pixel = (int(tag[XmlParser.XMIN_KEY]), int(tag[XmlParser.YMIN_KEY]))
    # get the color level
    helper_val_list = list(helper_map.keys())
    txt_color = (255,0,0)
    if helper_val in helper_val_list:
        ind = float(helper_val_list.index(helper_val))
        txt_color = (50,(1-ind/len(helper_val_list))*255,50)
    cv2.putText(image, helper_text, tag_top_left_pixel, cv2.FONT_HERSHEY_SIMPLEX, 0.5, txt_color, 2)


def plot_bboxes(img_path, tags={}, drop_tags={}, label_to_color={}, default_color_val=(255, 255, 255), thickness=3,
                helper_tag_field=None,helper_tag_map={}):
    image = cv2.imread(img_path)
    # plot tags
    for id, tag in tags.items():
        label = tag.get(XmlParser.LABEL_KEY, None)
        tag_color = label_to_color.get(label, default_color_val)
        plot_bbox(image, tag, tag_color, thickness)
        if helper_tag_field:
            add_helper_tag_text(image,tag,helper_tag_field,helper_tag_map)
    # plot drop tags
    for id, tag in drop_tags.items():
        plot_bbox(image, tag, drop_tag_color, thickness)
        if helper_tag_field:
            add_helper_tag_text(image,tag,helper_tag_field,helper_tag_map)
    # convert img from bgr to rgb
    if image is None:
        print(img_path)
    image = image[:, :, ::-1]
    return image


def save_img(img, out_path):
    # Making sure that saving dir exists.
    cf.create_folder(os.path.dirname(out_path))
    out_img = Image.fromarray(img.astype(np.uint8))
    out_img.save(out_path)
