from data_tools.utils.xml_util import XmlParser
import numpy as np
import data_tools.utils.box_util as box_util
import os
from os.path import join as pjoin


# MongoToTFR common utils

def filter_tags_by_nms(tags, nms_iou_th=0.1, priority_field='occlusion', priority_higher_better=False):
    # assume the priority field is of type int
    tag_ids = []
    boxes = []
    priority_val_list = []
    # collect the info from the tags dict
    for tag_id, tag in tags.items():
        priority_val_list.append(max(0, int(tag[priority_field])))
        boxes.append(
            [tag[XmlParser.XMIN_KEY], tag[XmlParser.YMIN_KEY], tag[XmlParser.XMAX_KEY], tag[XmlParser.YMAX_KEY]])
        # because dict is not ordered we want a list of keys so the idxs will be a reference to them
        tag_ids.append(tag_id)
    # convert to numpy array
    boxes = np.array(boxes)
    priority_val_list = np.array(priority_val_list)
    tag_ids = np.array(tag_ids)
    # sort indexes by priority
    idxs = np.argsort(priority_val_list)
    if not priority_higher_better:
        idxs = idxs[::-1]
    # apply nms on bboxes and return list of idxs to keep after nms
    pick_idxs = box_util.nms(boxes, idxs, nms_iou_th)
    picked_tag_ids = tag_ids[pick_idxs]
    # list of the tag ids to keep
    return picked_tag_ids


def filter_tags_by_none_relevant(tags, none_relevant_labels, iou_th=0.1):
    # list of the tag ids to keep
    picked_tag_ids = []
    used_classes_bboxes = []
    none_relevant_classes_bboxes = []
    # build tag_id,bbox tuples and add them to one of the 2 lists
    for tag_id, tag in tags.items():
        bbox = [tag[XmlParser.XMIN_KEY], tag[XmlParser.YMIN_KEY], tag[XmlParser.XMAX_KEY], tag[XmlParser.YMAX_KEY]]
        label = tag[XmlParser.LABEL_KEY]
        if label in none_relevant_labels:
            none_relevant_classes_bboxes.append((tag_id, bbox))
        else:
            used_classes_bboxes.append((tag_id, bbox))
    # check for each combination of used tags and none_relevant tags if the used tag should be filtered or not
    for tag_id, used_box in used_classes_bboxes:
        # bbox corrdinates (xmin,ymin,xmax,ymax)
        add_flag = True
        for _, nr_box in none_relevant_classes_bboxes:
            iou = box_util.get_iou(used_box, nr_box)
            if iou > iou_th:
                add_flag = False
                break
        if add_flag:
            picked_tag_ids.append(tag_id)
    return picked_tag_ids


def filter_tags_by_bbox_side(tags, bbox_filter_interval_per_class, default_interval_key):
    # list of the tag ids to keep
    picked_tag_ids = []
    for tag_id, tag in tags.items():
        bbox = [tag[XmlParser.XMIN_KEY], tag[XmlParser.YMIN_KEY], tag[XmlParser.XMAX_KEY], tag[XmlParser.YMAX_KEY]]
        label = tag[XmlParser.LABEL_KEY]
        bbox_w = int(bbox[2] - bbox[0])
        bbox_h = int(bbox[3] - bbox[1])
        bbox_size_filter_interval = bbox_filter_interval_per_class.get(label, bbox_filter_interval_per_class[
            default_interval_key])
        if bbox_w in bbox_size_filter_interval and bbox_h in bbox_size_filter_interval:
            picked_tag_ids.append(tag_id)
    return picked_tag_ids


def filter_tags_by_position_changes_across_frames(tags_df, classes_list=None, dist_th=5, window_size=4):
    """
    Filter only the tags which are not in the classes_list which don't move at least dist_h pixels
    accros window_size frames
    :param tags_df: tags data frame for all the frames
    :param classes_list: list of classes to apply the filter on. In case of none, will apply to ALL the classes
    :param dist_th: distance threshold in pixels
    :param window_size: number of frames between start frame and end frame to check if object has moved at least
    dist_th pixels
    :return: only the indexes of the tags_df to keep after filtering
    """
    # in case classes_list is none this mean to apply filter for all classes
    # list of indxes to keep from the df
    picked_df_indxs = []
    for track_id in set(tags_df['track']):
        relevant_tags = tags_df.loc[tags_df['track'] == track_id]
        label = relevant_tags['label'].mode()[0]
        org_indxs = relevant_tags.index

        # add tag if it is not in the classes_list
        if classes_list and label not in classes_list:
            picked_df_indxs.extend(np.array(org_indxs))
            continue

        # calculate the center points
        relevant_tags = relevant_tags.assign(
            cx=((relevant_tags['xmax'] - relevant_tags['xmin']) / 2. + relevant_tags['xmin']).astype(int))
        relevant_tags = relevant_tags.assign(
            cy=((relevant_tags['ymax'] - relevant_tags['ymin']) / 2. + relevant_tags['ymin']).astype(int))
        # calculate moving average
        relevant_tags = relevant_tags.assign(a_cx=relevant_tags['cx'].rolling(2).mean())
        relevant_tags = relevant_tags.assign(a_cy=relevant_tags['cy'].rolling(2).mean())
        # update nan value of first row becuase of the rooling to the original val
        ind_first_row = org_indxs[0]
        relevant_tags.at[ind_first_row, 'a_cx'] = relevant_tags.at[ind_first_row, 'cx']
        relevant_tags.at[ind_first_row, 'a_cy'] = relevant_tags.at[ind_first_row, 'cy']
        # caculate the diff comparing frame x and frame x+window_size
        dist = []
        for i in range(len(relevant_tags) - window_size):
            x2 = relevant_tags.at[org_indxs[i + window_size], 'a_cx']
            x1 = relevant_tags.at[org_indxs[i], 'a_cx']
            y2 = relevant_tags.at[org_indxs[i + window_size], 'a_cy']
            y1 = relevant_tags.at[org_indxs[i], 'a_cy']
            dist.append(np.sqrt(np.square(y2 - y1) + np.square(x2 - x1)))
        dist = np.array(dist)
        indxs = np.where(dist > dist_th)[0]
        # apply to all the window indxs
        new_indxs = np.array([range(i, i + 1 + window_size) for i in indxs]).reshape(-1)
        new_indxs = sorted(list(set(new_indxs)))
        # get the original indxs in df
        new_indxs = np.array(relevant_tags.index[new_indxs])
        picked_df_indxs.extend(new_indxs)

    return sorted(picked_df_indxs)


def filter_tags_by_exchange_labels(tags, exchange_labels_dict):
    if not exchange_labels_dict:
        # don't filter anything
        return tags.keys()
    picked_tag_ids = []
    for tag_id, tag in tags.items():
        # check if label after converting is recognized and add to picked_tag_ids
        label = tag[XmlParser.LABEL_KEY].lower()
        exc_label = exchange_labels_dict.get(label, None)
        if exc_label:
            picked_tag_ids.append(tag_id)
    return picked_tag_ids


def blacklist_filtering(blacklist, parent_dir_path, suspected_dir_list):
    # Removing black-listed directories
    if blacklist:
        dirs_full_paths = [pjoin(parent_dir_path, suspected_dir) for suspected_dir in suspected_dir_list]
        for dir_path in dirs_full_paths:
            for blacklisted_dir in blacklist:
                if dir_path.endswith(blacklisted_dir):
                    suspected_dir_list.remove(os.path.basename(dir_path))
                    break


def whitelist_filtering(whitelist, dir_path):
    if whitelist is not None:
        for white_dir in whitelist:
            if dir_path.endswith(white_dir):
                return True
        return False
    # If whitelist is None we return True and don't filter directories
    return True


def split_tags(tags, keep_tag_ids):
    # keep_tag_ids is a list of tag_ids to keep
    keep_tags = {}
    drop_tags = {}
    for tag_id in tags.keys():
        if tag_id in keep_tag_ids:
            keep_tags[tag_id] = tags[tag_id]
        else:
            drop_tags[tag_id] = tags[tag_id]
    return keep_tags, drop_tags


def filter_labels_list(lbl_list, lbls_to_remove=None, lbls_to_keep=None, lbls_to_filter=None,
                       use_label_prefix=False, verbose=True):
    # use_label_prefix allow match of label in lbl_list with the other label lists only by prefix and not equality

    # Check if a filer label is in the lbl_list
    if lbls_to_filter:
        for l in lbl_list:
            for l_filter in lbls_to_filter:
                if (use_label_prefix and l.startswith(l_filter)) or (l == l_filter):
                    if verbose:
                        print('Found filter label: %s in %s' % (l, lbl_list))
                    # return empty list so tag will be skipped
                    return []

    # Remove unwanted labels
    if lbls_to_remove:
        new_lbl_list = []
        for l in lbl_list:
            found=False
            for l_remove in lbls_to_remove:
                if (use_label_prefix and l.startswith(l_remove)) or (l == l_remove):
                    if verbose:
                        print('Remove %s from %s' % (l, lbl_list))
                    found=True
                    break
            if not found:
                new_lbl_list.append(l)
        lbl_list = new_lbl_list

    # Keep only the wanted labels in case there are no lbls_to_keep in lbl_list we don't change the lbl_list(!)
    if lbls_to_keep:
        new_lbl_list = []
        for l in lbl_list:
            for l_keep in lbls_to_keep:
                if (use_label_prefix and l.startswith(l_keep)) or (l == l_keep):
                    new_lbl_list.append(l)
        if new_lbl_list:
            if verbose:
                print('Keep only %s from %s' % (new_lbl_list, lbl_list))
            lbl_list = new_lbl_list

    return lbl_list
