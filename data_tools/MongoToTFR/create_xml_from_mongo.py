import shutil
import xml.etree.cElementTree as ET
from data_tools.MongoToTFR import get_mongo_data2
import os
import data_tools.DataManipulation.common_funcs as cf
import yaml
import sys
from data_tools.MongoToTFR.reporter import Reporter
import copy
import data_tools.utils.img_util as iu
import data_tools.utils.vid_util as vu
import data_tools.MongoToTFR.general_utils as ut


class XmlCreator:
    UNKNOWN_LEVEL_STR = 'unknown'

    def __init__(self, params_dict):
        self.log_dir = os.path.abspath(params_dict['logging_path'])
        self.data_path = os.path.abspath(params_dict['data_path'])
        self.xmls_path = os.path.abspath(params_dict['xml_path'])
        self.vid_desired_fps = params_dict.get('vid_desired_fps', None)
        self.label_set = set()
        # create the log dir
        cf.create_folder(self.log_dir)
        occlusion_levels = params_dict.get('occlusion_levels', [])
        # levels are from easy to hard
        appearance_levels = params_dict.get('appearance_levels', [])
        # add the unknown levels
        occlusion_levels.insert(0, XmlCreator.UNKNOWN_LEVEL_STR)
        appearance_levels.insert(0, XmlCreator.UNKNOWN_LEVEL_STR)
        self.occlusion_levels = {}
        self.appearance_levels = {}
        # convert it to dict for double mapping. unknow will map to -1
        for ind, oc_level in enumerate(occlusion_levels):
            oc_level = oc_level.lower()
            self.occlusion_levels[ind - 1] = oc_level
            self.occlusion_levels[oc_level] = ind - 1
        for ind, app_level in enumerate(appearance_levels):
            app_level = app_level.lower()
            self.appearance_levels[ind - 1] = app_level
            self.appearance_levels[app_level] = ind - 1

        project_name = params_dict['project_name']
        exchange_labels_dict = params_dict.get('exchange_labels', {})
        self.remove_labels_hard = [l.lower() for l in params_dict.get('remove_labels_hard', [])]
        self.remove_labels_soft = [l.lower() for l in params_dict.get('remove_labels_soft', ['car'])]
        self.keep_labels_soft = [l.lower() for l in params_dict.get('keep_labels_soft', ['none_relevant'])]
        self.filter_labels = [l.lower() for l in params_dict.get('filter_labels', [])]
        colors = params_dict.get('colors', None)
        track_label_prefix = params_dict.get('track_label_prefix', None)
        allow_multi_label = params_dict.get('multi_label', False)

        # extract the data from the olympus server
        self.data_dict = get_mongo_data2.get_mongo_data_dicts(project_name, self.log_dir, colors=colors,
                                                              track_label_prefix=track_label_prefix,
                                                              allow_multi_labels=allow_multi_label,
                                                              exchange_labels_dict=exchange_labels_dict)

        # create the csvs
        csv_names_and_headers_dict = {
            'only_helper_tags': ['file_path', 'tags']
        }

        # csv logger
        self.csv_reporter = Reporter(self.log_dir)
        self.csv_reporter.create_csvs(csv_names_and_headers_dict)

    def _get_split_indxs_mongo_items(self):
        # create a new list with only the folder name for each element in the data_dict
        folders_list = [os.path.split(element[0])[0] for element in self.data_dict]
        indx = []
        if len(folders_list) == 0:
            return indx
        # insert the first position of new folder we discover in the folders_list
        indx.append(0)
        curr_folder = folders_list[1]
        for i in range(1, len(folders_list)):
            if curr_folder == folders_list[i]:
                continue
            else:
                # new folder name was found so insert it to the indexes list
                indx.append(i)
                curr_folder = folders_list[i]
        return indx

    def create_xml(self, dir_path, general_meta_dict, bbox_data):
        if len(bbox_data) == 0:
            return
        # extract the general data on the img
        width, height, depth = general_meta_dict['width'], general_meta_dict['height'], general_meta_dict['depth']
        filename = general_meta_dict['filename']
        from_vid = "1" if general_meta_dict['from_vid'] else "0"
        org_fps = general_meta_dict['org_fps']
        if not org_fps:
            org_fps = 'null'

        annotation = ET.Element("annotation")
        ET.SubElement(annotation, "format").text = "VOC2012"
        ET.SubElement(annotation, "segmented").text = "0"
        ET.SubElement(annotation, "path")
        size = ET.SubElement(annotation, "size")
        ET.SubElement(size, "width").text = str(width)
        ET.SubElement(size, "height").text = str(height)
        ET.SubElement(size, "depth").text = str(depth)
        ET.SubElement(annotation, "filename").text = filename
        ET.SubElement(annotation, "from_vid").text = from_vid
        ET.SubElement(annotation, "org_fps").text = str(org_fps)

        i = 0
        write_flag = False
        occlusion_levels = [l for l in self.occlusion_levels if type(l) == str]
        appearance_levels = [l for l in self.appearance_levels if type(l) == str]
        for bbox in bbox_data:
            label = bbox["label"]
            # defaults
            occluded = str(self.occlusion_levels[XmlCreator.UNKNOWN_LEVEL_STR])
            difficult = str(self.appearance_levels[XmlCreator.UNKNOWN_LEVEL_STR])
            label_list = label.split(',')
            # set the real occlusion and appearance level
            for ll in label_list:
                if ll in self.occlusion_levels:
                    occluded = str(self.occlusion_levels[ll])
                if ll in self.appearance_levels:
                    difficult = str(self.appearance_levels[ll])
            # copy of the label_list for deletion while iterate over the list
            remain_labels = copy.deepcopy(label_list)

            # remove labels
            remain_labels = ut.filter_labels_list(remain_labels, lbls_to_remove=self.remove_labels_hard,
                                                  use_label_prefix=False, verbose=False)
            # remove the helper tags
            remain_labels = ut.filter_labels_list(remain_labels,
                                                  lbls_to_remove=occlusion_levels + appearance_levels,
                                                  use_label_prefix=True, verbose=False)

            if len(remain_labels) == 0 and len(label_list) != 0:
                # only helper tags without the class tag in the label_list
                pic_relative_path = os.path.join(dir_path, filename).replace(self.xmls_path, '')
                if pic_relative_path.startswith('/'):
                    pic_relative_path = pic_relative_path[1:]
                self.csv_reporter.logger('{} Got only helper tags: {}', [pic_relative_path, label_list],
                                         'only_helper_tags')

            # remove and keep some labels in case of conflict (multi labels)
            # e.g remain_labels=['car','truck'] and remove_labels_soft=['car'] then we remove the car label
            # so we will have only the truck.
            # use this func only if there are more than 1 label
            if len(remain_labels) > 1:
                remain_labels = ut.filter_labels_list(remain_labels, lbls_to_remove=self.remove_labels_soft,
                                                      lbls_to_keep=self.keep_labels_soft,
                                                      lbls_to_filter=self.filter_labels, verbose=True)

            label = ','.join(remain_labels)

            if not label:
                continue
            color = bbox["color"]
            if not color:
                color = 'null'

            i += 1
            track_id = bbox['track_id']
            if not track_id:
                track_id = i

            # get the bbox params
            xmin = int(round(float(bbox["xmin"]) / 100 * width))
            xmax = int(round(float(bbox["xmax"]) / 100 * width))
            ymin = int(round(float(bbox["ymin"]) / 100 * height))
            ymax = int(round(float(bbox["ymax"]) / 100 * height))
            angle = int(bbox["angle"])

            write_flag = True

            object = ET.SubElement(annotation, "object")
            ET.SubElement(object, "difficult").text = difficult
            ET.SubElement(object, "score").text = "0"
            vel = ET.SubElement(object, "velocity")
            ET.SubElement(vel, "x").text = "0"
            ET.SubElement(vel, "y").text = "0"
            bndbox = ET.SubElement(object, "bndbox")
            ET.SubElement(bndbox, "xmin").text = str(xmin)
            ET.SubElement(bndbox, "xmax").text = str(xmax)
            ET.SubElement(bndbox, "ymin").text = str(ymin)
            ET.SubElement(bndbox, "ymax").text = str(ymax)
            ET.SubElement(bndbox, "angle").text = str(angle)
            ET.SubElement(object, "occluded").text = occluded
            ET.SubElement(object, "class").text = label
            ET.SubElement(object, "color").text = color
            ET.SubElement(object, "id").text = str(track_id)
            # record the labels
            self.label_set.add(label)
        tree = ET.ElementTree(annotation)
        img_path = os.path.join(dir_path, filename)
        xml_path = os.path.splitext(img_path)[0] + '.xml'
        if write_flag:
            # write the xml only if it has tags
            tree.write(xml_path, method='html')

    def create_xmls(self):
        # create folders, inside the xml_parent_folder, for each folder in mongo
        # data_dict is the output of get_mongo_data_dicts method
        # create the xmls folder
        cf.create_folder(self.xmls_path)
        folders_ind = self._get_split_indxs_mongo_items()
        unique_folders = [os.path.dirname(self.data_dict[i][0]) for i in folders_ind]
        num_uni_folders = len(unique_folders)
        for i in range(num_uni_folders):
            folder = unique_folders[i]
            # get the sublist from data_dict
            if i == num_uni_folders - 1:
                images_data = self.data_dict[folders_ind[i]:]
            else:
                images_data = self.data_dict[folders_ind[i]:folders_ind[i + 1]]

            if len(images_data) == 0:
                # TODO: log this
                print("folder: %s doesn't have tags!")
                continue

            # relevant only for video
            skipped_frames_counter = 0

            org_fps = None
            height, width, depth = None, None, None

            # check for local file (img folder or video)
            # first check for img folder
            data_folder_path = os.path.join(self.data_path, folder)
            if os.path.exists(data_folder_path):
                is_vid = False
            else:
                # check for video if no img folder available
                vid_names, vid_ext = map(list, zip(*[os.path.splitext(v) for v in os.listdir(self.data_path)]))
                if folder not in vid_names:
                    # TODO: need to log this
                    print("%s doesn't exist in the local files so it will be skipped" % folder)
                    continue
                ind = vid_names.index(folder)
                vid_path = os.path.join(self.data_path, folder + vid_ext[ind])
                vid_stats = vu.get_vid_stats(vid_path)
                org_fps = vid_stats['fps']
                height = vid_stats['height']
                width = vid_stats['width']
                depth = vid_stats['depth']
                if not self.vid_desired_fps:
                    vid_fps_filter = 1
                else:
                    vid_fps_filter = int(org_fps / float(self.vid_desired_fps))
                is_vid = True

            # create the xml folder after deleting previous product
            xml_folder_path = os.path.join(self.xmls_path, folder)
            if os.path.exists(xml_folder_path):
                shutil.rmtree(xml_folder_path)
            cf.create_folder(xml_folder_path)

            for im_data in images_data:
                filename = os.path.basename(im_data[0])
                if is_vid:
                    # get video frame number
                    frame_num = int(os.path.splitext(filename)[0])
                    if frame_num % vid_fps_filter != 0:
                        # skip this frame
                        skipped_frames_counter += 1
                        continue
                else:
                    # frame
                    # get the image details
                    img_path = os.path.join(data_folder_path, filename)
                    if not os.path.exists(img_path):
                        print("%s doesn't exist in the local files so it will be skipped" % img_path)
                        continue
                    height, width, depth = iu.get_img_size_and_depth(img_path)

                general_meta_dict = {'filename': filename, 'height': height, 'width': width, 'depth': depth,
                                     'from_vid': is_vid, 'org_fps': org_fps}
                self.create_xml(xml_folder_path, general_meta_dict, im_data[1])
            if skipped_frames_counter > 0:
                print('skipped frames for %s is: %d' % (folder, skipped_frames_counter))


def move_xmls_to_pic_folder(data_path, xmls_path):
    # first delete xml folder in data_path
    for root, dirs, files in os.walk(data_path, topdown=True):
        for dir in dirs:
            parent_dir_name = os.path.basename(root)
            if dir == 'xml' and parent_dir_name in os.listdir(xmls_path):
                shutil.rmtree(os.path.join(root, dir))
                if not os.listdir(root):
                    shutil.rmtree(root)
    # move the xmls to pic folders
    for root, dirs, files in os.walk(xmls_path, topdown=True):
        for dir in dirs:
            src_folder = os.path.join(root, dir)
            parent_folder = os.path.join(data_path, dir)
            # check if parent folder exists
            if not os.path.exists(parent_folder):
                # assume this is probably a video that why can't find image folder
                # abort the copy of xml files for this folder
                print("Abort xml copy to: %s" % (parent_folder))
                continue
            dst_folder = os.path.join(parent_folder, 'xml')
            cf.create_folder(dst_folder)
            for file in os.listdir(src_folder):
                shutil.copyfile(os.path.join(src_folder, file), os.path.join(dst_folder, file))


def main(yaml_path):
    with open(yaml_path, 'r') as yaml_file:
        data = yaml.load(yaml_file, Loader=yaml.SafeLoader)
    # get the data from mongo server
    xml_creator = XmlCreator(data)
    # create the xmls
    xml_creator.create_xmls()
    print('Done creating xmls. Now starting to move them to pic folders')
    # move xmls to the pics folders
    move_xmls_to_pic_folder(xml_creator.data_path, xml_creator.xmls_path)
    print('The labels: %s' % xml_creator.label_set)
    print('Finished')


if __name__ == '__main__':
    # read the program arguments - put the yaml full path as a program parameter
    assert len(sys.argv) == 2
    yaml_path = sys.argv[1]
    main(yaml_path)
