# Eval Tools
This code is specific for object detection evaluation.
The main script is eval_analysis.py

## Features
- Create a csv with confusion matrix and other statistics (e.g precision, recall) for different iou and score thresholds.
- Create a csv with recall per class and size.
- Create a csv for tracker evaluation.
- Visualize matching between prediction boxes and gt boxes via full frames and cropped boxes.

## Installation

**Requirements:**
```
tensorflow-gpu
numpy
pandas
pandas_ml
logging
csv
os
ast
sklearn
argparse
recordtype
cv2
re
matplotlib
PIL
tqdm
python-intervals (imported as intervals)
```
**Tested on:**
* Ubuntu 16.04
* python 2.7

## Running
You can run the main script in 2 ways:
- To evaluate the result from the Object Detection API after you run the eval_util and got the Predictions.csv
- To evaluate the skeleton-pipeline

**Object Detection API**:

When running eval, directory with name "statistics" will be created in the eval folder and for each eval checkpoint a directory
with the checkpoint number will be created. 

Steps: 
1. Clone the data_tools and use the master branch (should be at the same level of tensorflow.models)
2. In tensorflow.models do git checkout feature/confusion_matrix
3. Fix import problems if necessary (might be wrong path to the eval_analysis or confusion_matrix_util files)
4. Run tensorflow.models/research/object_detection/legacy/eval.py
5. In case you want to also visualize the eval and not just get statistics, put "--visualize_cm" in the eval.py params.
This will save the crops and full frames so you can use them for the analyze part
6. Choose specific eval ckpt and run data_tools/EvalTools/eval_analysis.py

**Skeleton-Pipeline**:

1. Get the gt csv by running data_tools/MongoToTFR/export_to_eval_csv.py
2. Run skeleton-pipeline to get the prediction csv
3. (Optional) Run data_tools/EvalTools/visualize_helper.py script to create the gt and prediction crops and full images for using the --visualize_cm flag 
in the eval_analysis script. Note that you need to double check the created images are sync with the tags
4. Run data_tools/EvalTools/eval_analysis.py with the "--gt_csv" and "--pred_csv" arguments


Before running your own code, make sure you can run the unit tests file (eval_analysis_test.py) without errors.

## Demo results

**Configuration:**

iou_th - 0.5

score_th - 0.3


**Object Detection API**

An example of the output files generated from eval on some checkpoint x. Path will be eval_dir/statistics/x

![Statistics files](doc/od_out_files.png "Statistics files")

An example of a generated Predictions.csv file from object detection api run:

![Predictions csv example](doc/prediction_csv_example.png "Predictions csv example")

An example of the output visualization folders in eval_images:

![Eval images folders example](doc/eval_images_folders.png "Eval images folders example")

An example of a marked image in the marked_images:

![Marked full frame example](doc/marked_full_frame.png "Marked full frame example")



**Skeleton-pipeline**

An example of the output files:

![Out files](doc/pipeline_out_files.png "Pipeline out files")

An example of a generated Tracker_iou_0.50_score_0.30.csv file:

![Tracker csv example](doc/tracker_csv_example.png "Tracker csv example")


