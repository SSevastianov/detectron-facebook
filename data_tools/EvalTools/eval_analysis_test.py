import pytest
import os
import data_tools.EvalTools.eval_analysis as ea
import re
import pandas as pd
import numpy as np
from data_tools.EvalTools.confusion_matrix_util import get_confusion_matrix_stats


@pytest.fixture
def setup_77():
    gt_csv = './test_files/77/DJI_0077_gt.csv'
    pred_csv = './test_files/77/DJI_0077_preds.csv'
    label_map_path = './test_files/eval_tracker_label_map.pbtxt'
    categories, reversed_categories = ea.get_categories(label_map_path)
    results_list = ea.read_csv_to_result_list(gt_csv, pred_csv, categories=reversed_categories,
                                              complete_gt_frames=False, allowed_gt_range=2)
    return results_list, categories


@pytest.fixture
def setup_190():
    gt_csv = './test_files/190/DJI_0190_gt.csv'
    pred_csv = './test_files/190/DJI_0190_preds.csv'
    label_map_path = './test_files/eval_tracker_label_map.pbtxt'
    categories, reversed_categories = ea.get_categories(label_map_path)
    results_list = ea.read_csv_to_result_list(gt_csv, pred_csv, categories=reversed_categories,
                                              complete_gt_frames=False, allowed_gt_range=2)
    return results_list, categories


def test_matching_frames_number(setup_77, setup_190):
    def get_frame_numbers(results_list, pics_dir):
        res_frame_numbers = [int(re.findall(r'\d+', res['key'])[0]) for res in results_list]
        test_frame_numbers = [int(re.findall(r'\d+', f_n)[0]) for f_n in os.listdir(pics_dir)]
        res_frame_numbers.sort()
        test_frame_numbers.sort()
        return res_frame_numbers, test_frame_numbers

    # 77
    results_list_77, categories_77 = setup_77
    res_nums, test_nums = get_frame_numbers(results_list_77, './test_files/77/pics')
    # test the total number of matched frames
    assert 6 == len(results_list_77)
    # test the frame numbers one by one of the matched frames
    assert test_nums == res_nums

    # 190
    results_list_190, categories_190 = setup_190
    res_nums, test_nums = get_frame_numbers(results_list_190, './test_files/190/pics')
    # test the total number of matched frames
    assert 4 == len(results_list_190)
    # test the frame numbers one by one of the matched frames
    assert test_nums == res_nums


def test_cm(setup_77, setup_190):
    score_th = 0.3
    iou_th = 0.4
    extra_gt_pred_match_info_key_list = ['gt_track_id', 'pred_track_id']

    # 77
    results_list_77, categories_77 = setup_77
    # calculate the raw analysis
    analysis_lines = ea.get_analysis_lines(results_list_77, score_threshold=score_th, verbose=False)
    preds, gts = ea.get_matches_by_iou(analysis_lines, iou_th, extra_params_key_list=extra_gt_pred_match_info_key_list)
    # get the cm
    cm_stats = get_confusion_matrix_stats(preds, gts, iou_th, score_th, categories=categories_77, verbose=False)
    actual_cm = cm_stats['Confusion-Matrix'].values
    expected_cm = pd.read_csv('./test_files/77/cm_77.csv', header=None).values
    assert np.all(expected_cm) == np.all(actual_cm)

    # 190
    results_list_190, categories_190 = setup_190
    # calculate the raw analysis
    analysis_lines = ea.get_analysis_lines(results_list_190, score_threshold=score_th, verbose=False)
    preds, gts = ea.get_matches_by_iou(analysis_lines, iou_th, extra_params_key_list=extra_gt_pred_match_info_key_list)
    # get the cm
    cm_stats = get_confusion_matrix_stats(preds, gts, iou_th, score_th, categories=categories_190, verbose=False)
    actual_cm = cm_stats['Confusion-Matrix'].values
    expected_cm = pd.read_csv('./test_files/190/cm_190.csv', header=None).values
    assert np.all(expected_cm) == np.all(actual_cm)


def test_roi_eval(setup_190):
    roi_bbox = [310, 260, 910, 1120]
    score_th = 0.3
    iou_th = 0.4
    results_list_190, categories_190 = setup_190
    # calculate the raw analysis
    analysis_lines = ea.get_analysis_lines(results_list_190, score_threshold=score_th, verbose=False)
    preds, gts = ea.get_matches_by_iou(analysis_lines, iou_th, extra_params_key_list=None)
    # filter by roi
    preds, gts = ea.filter_by_roi(preds, gts, roi_bbox, overlap_th=0.2)
    # get the cm
    cm_stats = get_confusion_matrix_stats(preds, gts, iou_th, score_th, categories=categories_190, verbose=False)
    actual_cm = cm_stats['Confusion-Matrix'].values
    expected_cm = pd.read_csv('./test_files/190/cm_190_with_roi.csv', header=None).values
    assert np.all(expected_cm) == np.all(actual_cm)
