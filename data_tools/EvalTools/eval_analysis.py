import argparse
import copy
import os
from data_tools.DataManipulation import common_funcs as cf
from data_tools.utils import img_util as iu

from data_tools.EvalTools.excel_util import write_metronome_stats

from collections import defaultdict, Counter

from recordtype import recordtype
import pandas as pd
import cv2
import csv
from data_tools.EvalTools import label_map_util

# fix for using specific backend
import matplotlib

if matplotlib.get_backend().lower() != 'agg':
    matplotlib.use('agg')

from matplotlib.patches import Rectangle
import matplotlib.pyplot as plt
from operator import itemgetter
import intervals as I
import numpy as np
from PIL import Image
from tqdm import tqdm
from data_tools.EvalTools import confusion_matrix_util
from data_tools.utils import box_util

PREDS_COLORS = [
    'blue', 'orange', 'lightcoral', 'purple', 'magenta', 'cyan', 'saddlebrown', 'chartreuse', 'grey', 'gold'
]

EVAL_UTIL_VIS_CM_FLAG = '--visualize_cm'

# result_dict keys
result_pred_key_pre = 'detection'
result_gt_key_pre = 'groundtruth'
filenames_key = 'key'
gt_boxes_key = result_gt_key_pre+'_boxes'
pred_boxes_key = result_pred_key_pre+'_boxes'
gt_classes_key = result_gt_key_pre+'_classes'
pred_classes_key = result_pred_key_pre+'_classes'
pred_scores_key = result_pred_key_pre+'_scores'
gt_track_ids_key = result_gt_key_pre+'_track_ids'
pred_track_ids_key = result_pred_key_pre+'_track_ids'

# analysis keys
filename_key = 'filename'
iou_key = 'max_iou'
box_key = 'box'
class_key = 'class'
crop_key = 'crop_id'
track_key = 'track_id'
score_key = 'score'
height_key = 'height'
width_key = 'width'
xmin_key = 'xmin'
ymin_key = 'ymin'
xmax_key = 'xmax'
ymax_key = 'ymax'

empty_track_val = ''

# eval tracker keys
gt_track_id_key = 'gt_track_id'
total_track_time_sec_key = 'total_track_time_seconds'
matched_track_percent_key = 'matched_track_percent'
largest_seq_track_percent_key = 'largest_sequential_track_percent'
num_track_breaks_key = 'number_track_breaks'
matched_track_ranges_key = 'matched_track_ranges'
matched_ranges_size_key = 'matched_ranges_size'
tracker_csv_headers = [gt_track_id_key, total_track_time_sec_key, matched_track_percent_key,
                       largest_seq_track_percent_key, num_track_breaks_key]

bbox_keys = [ymin_key, xmin_key, ymax_key, xmax_key]
mutual_keys = [crop_key, track_key, class_key, height_key, width_key]
gt_key_pre = 'gt_'
pred_key_pre = 'pred_'

gt_bbox_keys = [gt_key_pre + k for k in bbox_keys]
gt_keys = [gt_key_pre + k for k in mutual_keys] + gt_bbox_keys
pred_bbox_keys = [pred_key_pre + k for k in bbox_keys]
pred_keys = [pred_key_pre + k for k in mutual_keys] + pred_bbox_keys + [score_key]

# the column headers in the analysis csv file
analysis_keys = [filename_key, iou_key] + gt_keys + pred_keys
STATISTICS_DIR_NAME = 'statistics'
PREDICTION_CSV_NAME = 'Predictions.csv'
ANALYSIS_CSV_NAME = 'Analysis.csv'
TRACKER_CSV_NAME = 'Tracker.csv'
EVAL_IMG_DIR_NAME = 'eval_images'
GT_CROPS_DIR_NAME = 'groundtruth_crops'
PRED_CROPS_DIR_NAME = 'prediction_crops'
ORG_IMGS_DIR_NAME = 'original_images'
MARKED_IMGS_DIR_NAME = 'marked_images'
CM_VISUALIZE_DIR_NAME = 'cm_visualize'
CM_VISUALIZE_DIR_NAME_PATTERN = '%s_score_%.2f_iou_%.2f'

IMG_EXTENSION = '.jpg'

EVAL_SYSTEM_CSV_HEADERS = ['real_frame_num', 'xmin', 'ymin', 'xmax', 'ymax', 'score', 'label', 'track']
# don't change this because the code assume this format
EVAL_SYSTEM_FRAME_NAME_FORMAT = 'frame_%d'


def get_crops(img, detections_boxes, gt_boxes, padding=10):
    gt_crops = []
    pred_crops = []
    for d_box in detections_boxes:
        y_min, x_min, y_max, x_max = [int(p) for p in d_box]
        y_min = np.maximum(0, y_min - padding)
        x_min = np.maximum(0, x_min - padding)
        crop = img[y_min:y_max + padding, x_min:x_max + padding]
        pred_crops.append(crop)
    for gt_box in gt_boxes:
        y_min, x_min, y_max, x_max = [int(p) for p in gt_box]
        y_min = np.maximum(0, y_min - padding)
        x_min = np.maximum(0, x_min - padding)
        crop = img[y_min:y_max + padding, x_min:x_max + padding]
        gt_crops.append(crop)
    return pred_crops, gt_crops


def estimate_tracks_in_gt_frames(df_gt, df_pred):
    # assume gt is lower fps than pred
    frame_h, xmin_h, ymin_h, xmax_h, ymax_h, score_h, label_h, id_h = EVAL_SYSTEM_CSV_HEADERS
    gt_tracks = [t for t in list(set(df_gt[id_h])) if type(t) == str and t != '']
    for gt_tr in gt_tracks:
        # get the relevant gts sorted by frame number
        relevant_gts = df_gt.loc[df_gt[id_h] == gt_tr].sort_values([frame_h])
        label = set(relevant_gts[label_h])
        if len(label) != 1:
            print('There are different labels for track id: %s. labels: %s' % (gt_tr, list(label)))
            labels_amounts = Counter(relevant_gts[label_h].values.tolist())
            print(labels_amounts)
            dominant_label = labels_amounts.most_common(1)[0][0]
            print('The dominant label is: %s' % (dominant_label))
            # remove it from list
            labels_amounts.pop(dominant_label)
            for item, count in labels_amounts.items():
                print('frames for %s: %s' % (
                    item, relevant_gts.loc[relevant_gts[label_h] == item][frame_h].values.tolist()))
            raise ValueError('more than 1 label for track id: %s' % gt_tr)
        label = label.pop()
        extra_gts = pd.DataFrame(columns=EVAL_SYSTEM_CSV_HEADERS)
        # assume the frames are in order
        start_frame = relevant_gts.iloc[0][frame_h]
        end_frame = relevant_gts.iloc[-1][frame_h]
        gt_frames_num = relevant_gts[frame_h].values.tolist()
        if len(set(gt_frames_num)) != len(gt_frames_num):
            # we have on the same frame 2 tags of the same track id
            duplicates = [item for item, count in Counter(gt_frames_num).items() if count > 1]
            raise ValueError('There are duplicate tags for track id: %s in frames: %s' % (gt_tr, duplicates))
        relevant_preds = df_pred.loc[start_frame <= df_pred[frame_h]].loc[df_pred[frame_h] <= end_frame]
        pred_frames_num = sorted(set(relevant_preds[frame_h].values.tolist()))
        gt_curr_ind = 0
        for pred_curr_ind in range(len(pred_frames_num)):
            p_frame_num = pred_frames_num[pred_curr_ind]
            g_frame_num_pre = gt_frames_num[gt_curr_ind - 1]
            g_frame_num = gt_frames_num[gt_curr_ind]
            if p_frame_num >= g_frame_num:
                # need to increase the next gt frame num
                gt_curr_ind += 1
            if p_frame_num == g_frame_num:
                # can skip this frame because already has gt frame
                continue
            # create new gt
            gt_bbox_pre = relevant_gts.iloc[gt_curr_ind - 1][[xmin_h, ymin_h, xmax_h, ymax_h]].values.tolist()
            pre_width = gt_bbox_pre[2] - gt_bbox_pre[0]
            pre_height = gt_bbox_pre[3] - gt_bbox_pre[1]
            gt_bbox_next = relevant_gts.iloc[gt_curr_ind][[xmin_h, ymin_h, xmax_h, ymax_h]].values.tolist()
            progress_percent = float(p_frame_num - g_frame_num_pre) / (g_frame_num - g_frame_num_pre)
            x_diff = gt_bbox_next[0] - gt_bbox_pre[0]
            y_diff = gt_bbox_next[1] - gt_bbox_pre[1]
            # round the pixel so we can save int and not float
            new_gt_xmin = int(round(x_diff * progress_percent + gt_bbox_pre[0]))
            new_gt_ymin = int(round(y_diff * progress_percent + gt_bbox_pre[1]))
            new_gt_xmax = new_gt_xmin + pre_width
            new_gt_ymax = new_gt_ymin + pre_height
            new_df_row = {frame_h: p_frame_num, xmin_h: new_gt_xmin, ymin_h: new_gt_ymin, xmax_h: new_gt_xmax,
                          ymax_h: new_gt_ymax, score_h: None, label_h: label, id_h: gt_tr}
            extra_gts = extra_gts.append(new_df_row, ignore_index=True)
        # add the extra rows
        df_gt = df_gt.append(extra_gts, ignore_index=True, sort=False)

    # sort by frame number
    df_gt = df_gt.sort_values([frame_h])
    return df_gt


def modify_pred_frame_nums_to_gt(df_gt, df_pred, allowed_range=2):
    frame_h, xmin_h, ymin_h, xmax_h, ymax_h, score_h, label_h, id_h = EVAL_SYSTEM_CSV_HEADERS
    gt_frame_nums = list(set(df_gt[frame_h]))
    pred_frame_nums = list(set(df_pred[frame_h]))
    gt_frame_nums.sort()
    pred_frame_nums.sort()
    gt_frame_nums = np.array(gt_frame_nums)
    pred_frame_nums = np.array(pred_frame_nums)
    # convert each gt frame into range tuple
    gt_ranges = list(zip(gt_frame_nums - allowed_range, gt_frame_nums + allowed_range))
    convert_list = []
    gt_ind = 0
    pred_ind = 0
    while gt_ind < len(gt_ranges) and pred_ind < len(pred_frame_nums):
        gt_frame = gt_frame_nums[gt_ind]
        gt_range = gt_ranges[gt_ind]
        pred_frame = pred_frame_nums[pred_ind]
        if pred_frame >= gt_range[0] and pred_frame <= gt_range[1] and pred_frame != gt_frame:
            # convert the frame number to the gt number
            convert_list.append((pred_frame, gt_frame))
        if pred_frame > gt_range[1]:
            gt_ind += 1
        else:
            pred_ind += 1
    # convert all the pred frames
    for item in convert_list:
        # mask for selecting the rows with the relevant pred frame number
        mask = df_pred[frame_h] == item[0]
        # assign the gt frame number instead
        df_pred.loc[mask, frame_h] = item[1]
    return df_pred


def read_csv_to_result_list(gt_csv, preds_csv, categories=None, complete_gt_frames=True, allowed_gt_range=2):
    """
    the 2 csvs will be in the EVAL_SYSTEM_CSV_HEADERS format
    :param gt_csv: the groundtruth data csv
    :param preds_csv: output from the pipeline of the component predictions
    :param categories: dictionary to exchange the label name to its id
    :return: list of combine data of gt and predictions per image/frame
    """
    results_list = []
    frame_h, xmin_h, ymin_h, xmax_h, ymax_h, score_h, label_h, id_h = EVAL_SYSTEM_CSV_HEADERS
    with open(gt_csv, 'r') as gt_file:
        df_gt = pd.read_csv(gt_file)
    with open(preds_csv, 'r') as pred_file:
        df_pred = pd.read_csv(pred_file)
    if complete_gt_frames:
        df_gt = estimate_tracks_in_gt_frames(df_gt, df_pred)

    # change prediction frame numbers to match gt frame numbers for a given range
    if allowed_gt_range > 0:
        df_pred = modify_pred_frame_nums_to_gt(df_gt, df_pred, allowed_range=allowed_gt_range)

    # select all the preds rows that has frame number in gt frames
    preds_in_gts = df_pred.loc[df_pred[frame_h].isin(df_gt[frame_h])]
    # select all the gts rows that has the frame number in preds frames
    gts_in_preds = df_gt.loc[df_gt[frame_h].isin(df_pred[frame_h])]

    frame_nums = list(set(gts_in_preds[frame_h]))
    frame_nums.sort()
    for f_n in frame_nums:
        # init the result dict
        result = {}
        result[filenames_key] = EVAL_SYSTEM_FRAME_NAME_FORMAT % f_n + IMG_EXTENSION
        gt_rows = gts_in_preds.loc[gts_in_preds[frame_h] == f_n]
        pred_rows = preds_in_gts.loc[preds_in_gts[frame_h] == f_n]
        result[pred_boxes_key] = pred_rows[[ymin_h, xmin_h, ymax_h, xmax_h]].values.tolist()
        result[pred_scores_key] = pred_rows[score_h].values.tolist()
        result[pred_classes_key] = pred_rows[label_h].values.tolist()
        result[pred_track_ids_key] = pred_rows[id_h].values.tolist()
        result[gt_boxes_key] = gt_rows[[ymin_h, xmin_h, ymax_h, xmax_h]].values.tolist()
        result[gt_classes_key] = gt_rows[label_h].values.tolist()
        result[gt_track_ids_key] = gt_rows[id_h].values.tolist()
        if categories:
            # change label names to ids
            result[pred_classes_key] = [categories[l.lower()] for l in result[pred_classes_key]]
            result[gt_classes_key] = [categories[l.lower()] for l in result[gt_classes_key]]
        # add the frame data to list
        results_list.append(result)

    return results_list


def filter_by_score(analysis_lines, score_thresh):
    """
    filter lines from the analysis csv file by score threshold
    :param analysis_lines: readed lines from the csv
    :param score_thresh: the score threshold
    :return: the new lines after filtering
    """
    new_lines = []
    for ind, line in enumerate(analysis_lines):
        if line[score_key] <= score_thresh:
            if line[gt_key_pre + class_key] == 0:
                # in case this is FP skip line
                continue
            else:
                # should be FN
                new_line = copy.deepcopy(line)
                for key in pred_keys:
                    new_line[key] = None
                new_line[iou_key] = 0
                new_line[pred_key_pre + class_key] = 0
                new_lines.append(new_line)
        else:
            new_lines.append(line)

    return new_lines


def bbox_dict(filename, cls=0, score=1., box=None, crop_id=None, extra_params_dict={}):
    bbox_dict = {filename_key: filename, class_key: cls, score_key: score, box_key: box, crop_key: crop_id}
    # add all the other extra params
    for k, v in extra_params_dict.items():
        bbox_dict[k] = v
    return bbox_dict


def filter_by_roi(preds, gts, roi_bbox, overlap_th=0.2):
    if roi_bbox is None:
        return preds, gts
    new_gts = []
    new_preds = []
    # convert box from y1,x1,y2,x2 to x1,y1,x2,y2
    roi_bbox = box_util.change_xy_order(roi_bbox)

    for ind in range(len(preds)):
        pred = preds[ind]
        gt = gts[ind]
        # check if false alarm
        if pred[box_key] is not None and gt[box_key] is None:
            # add the FA
            new_gts.append(gt)
            new_preds.append(pred)
            continue
        # check if gt is in roi
        # convert box from y1,x1,y2,x2 to x1,y1,x2,y2
        gt_box = box_util.change_xy_order(gt[box_key])
        overlap = box_util.get_box_overlap_precent(gt_box, roi_bbox)
        if overlap > overlap_th:
            # gt inside roi
            new_gts.append(gt)
            new_preds.append(pred)
    return new_preds, new_gts


def get_matches_by_iou(analysis_lines, iou_thresh, extra_params_key_list=None):
    """
    match predictions and gts from the analysis lines by applying iou threshold
    :param analysis_lines: the lines from the analysis csv file
    :param iou_thresh: the iou threshold
    :return: list of predictions and groundtruths correlated to each other.
    for example, preds[0] matches to gts[0]
    """
    preds, gts = [], []
    for line in analysis_lines:
        gt_box = []
        pred_box = []
        for b_key in bbox_keys:
            gt_box.append(line[gt_key_pre + b_key])
            pred_box.append(line[pred_key_pre + b_key])

        # get the extra params value and pass them in the bbox dict
        extra_params_dict = {}
        if extra_params_key_list:
            for key in extra_params_key_list:
                extra_params_dict[key] = line[key]

        filename = line[filename_key]
        if line[iou_key] < iou_thresh:
            # need to separate this match
            if line[pred_key_pre + class_key] == 0 and line[gt_key_pre + class_key] != 0:
                # FN
                pred = bbox_dict(filename, cls=0, extra_params_dict=extra_params_dict)
                gt = bbox_dict(filename, cls=line[gt_key_pre + class_key], box=gt_box,
                               crop_id=line[gt_key_pre + crop_key],
                               extra_params_dict=extra_params_dict)
                preds.append(pred)
                gts.append(gt)
            elif line[pred_key_pre + class_key] != 0 and line[gt_key_pre + class_key] == 0:
                # FP
                pred = bbox_dict(filename, cls=line[pred_key_pre + class_key], score=line[score_key], box=pred_box,
                                 crop_id=line[pred_key_pre + crop_key], extra_params_dict=extra_params_dict)
                gt = bbox_dict(filename, cls=0, extra_params_dict=extra_params_dict)
                preds.append(pred)
                gts.append(gt)
            else:
                # FN + FP
                pred = bbox_dict(filename, cls=0, extra_params_dict=extra_params_dict)
                gt = bbox_dict(filename, cls=line[gt_key_pre + class_key], box=gt_box,
                               crop_id=line[gt_key_pre + crop_key],
                               extra_params_dict=extra_params_dict)
                preds.append(pred)
                gts.append(gt)
                pred = bbox_dict(filename, cls=line[pred_key_pre + class_key], score=line[score_key], box=pred_box,
                                 crop_id=line[pred_key_pre + crop_key], extra_params_dict=extra_params_dict)
                gt = bbox_dict(filename, cls=0, extra_params_dict=extra_params_dict)
                preds.append(pred)
                gts.append(gt)

        else:
            pred = bbox_dict(filename, cls=line[pred_key_pre + class_key], score=line[score_key], box=pred_box,
                             crop_id=line[pred_key_pre + crop_key], extra_params_dict=extra_params_dict)
            gt = bbox_dict(filename, cls=line[gt_key_pre + class_key], box=gt_box, crop_id=line[gt_key_pre + crop_key],
                           extra_params_dict=extra_params_dict)
            preds.append(pred)
            gts.append(gt)

    return preds, gts


def get_box_dims(bbox):
    """
    calculate the width and height of the bbox
    :param bbox: [y1,x1,y2,x2]
    :return: width, height
    """
    y_min, x_min, y_max, x_max = bbox
    width = x_max - x_min
    height = y_max - y_min
    return width, height


def create_dict_line(example, gt_idx, pred_idx, iou=0, gt_offset=None, pred_offset=None):
    """
    create a dictionary that will be used for 1 line in the analysis csv file
    :param example: dictionary of all the lines information
    :param gt_idx: the groundtruth index. if -1 then it means False Positive (FP)
    :param pred_idx: the prediction index. if -1 then it means False Negative (FN)
    :param iou: the iou between the prediction and gt
    :return: dictionary of the csv line
    """
    # gt_idx == -1 => FP
    # pred_idx == -1 => FN

    line = {}
    # init all values to none or pass the value if key is in the analysis_keys
    for key in analysis_keys:
        if key in example.keys():
            line[key] = example[key]
        else:
            line[key] = None

    line[filename_key] = example[filenames_key]
    line[iou_key] = iou
    line[score_key] = 0

    if gt_idx != -1:
        line[gt_key_pre + class_key] = example[gt_classes_key][gt_idx]
        line[gt_key_pre + crop_key] = gt_idx + gt_offset
        gt_box = example[gt_boxes_key][gt_idx]
        line[gt_key_pre + ymin_key] = int(gt_box[0])
        line[gt_key_pre + xmin_key] = int(gt_box[1])
        line[gt_key_pre + ymax_key] = int(gt_box[2])
        line[gt_key_pre + xmax_key] = int(gt_box[3])
        width, height = get_box_dims(gt_box)
        line[gt_key_pre + width_key] = int(width)
        line[gt_key_pre + height_key] = int(height)
        # add the track id if available
        if gt_track_ids_key in example.keys():
            track_val = example[gt_track_ids_key][gt_idx]
            if str(track_val) == 'nan':
                track_val = empty_track_val
            line[gt_key_pre + track_key] = track_val
    else:
        # FP
        line[gt_key_pre + class_key] = 0  # background
        if gt_track_ids_key in example.keys():
            line[gt_key_pre + track_key] = empty_track_val

    if pred_idx != -1:
        line[score_key] = example[pred_scores_key][pred_idx]
        line[pred_key_pre + class_key] = example[pred_classes_key][pred_idx]
        line[pred_key_pre + crop_key] = pred_idx + pred_offset
        pred_box = example[pred_boxes_key][pred_idx]
        line[pred_key_pre + ymin_key] = int(pred_box[0])
        line[pred_key_pre + xmin_key] = int(pred_box[1])
        line[pred_key_pre + ymax_key] = int(pred_box[2])
        line[pred_key_pre + xmax_key] = int(pred_box[3])
        width, height = get_box_dims(pred_box)
        line[pred_key_pre + width_key] = int(width)
        line[pred_key_pre + height_key] = int(height)
        # add the track id if available
        if pred_track_ids_key in example.keys():
            track_val = example[pred_track_ids_key][pred_idx]
            if str(track_val) == 'nan':
                track_val = empty_track_val
            line[pred_key_pre + track_key] = track_val
    else:
        # FN
        line[pred_key_pre + class_key] = 0  # background
        if pred_track_ids_key in example.keys():
            line[pred_key_pre + track_key] = empty_track_val

    return line


def filter_example_by_score(example, score_th):
    pred_boxes = example[pred_boxes_key]
    pred_classes = example[pred_classes_key]
    pred_scores = example[pred_scores_key]
    new_boxes = []
    new_classes = []
    new_scores = []
    removed_indexes = []
    for i, score in enumerate(pred_scores):
        if score >= score_th:
            new_boxes.append(pred_boxes[i])
            new_classes.append(pred_classes[i])
            new_scores.append(pred_scores[i])
        else:
            removed_indexes.append(i)
    example[pred_boxes_key] = new_boxes
    example[pred_classes_key] = new_classes
    example[pred_scores_key] = new_scores
    return example, removed_indexes

def filter_example_by_background(example):
    # remove the gt or pred with background
    new_example = {}
    for k,v in example.items():
        if k==filenames_key:
            new_example[k]=v
        else:
            new_example[k]=[]

    removed_indexes_pred = []
    removed_indexes_gt = []
    for i, pred_cls in enumerate(example[pred_classes_key]):
        if pred_cls == 0:
            # got background
            removed_indexes_pred.append(i)
        else:
            # add the pred details
            for k in example.keys():
                if k.startswith(result_pred_key_pre):
                    new_example[k].append(example[k][i])

    for i, gt_cls in enumerate(example[gt_classes_key]):
        if gt_cls == 0:
            # got background
            removed_indexes_gt.append(i)
        else:
            # add the gt details
            for k in example.keys():
                if k.startswith(result_gt_key_pre):
                    new_example[k].append(example[k][i])

    return new_example, removed_indexes_pred, removed_indexes_gt

def map_new_to_old_indexes(num_indexes_before, removed_indexes):
    # for example new_to_old_indexes[0] = 4 means that the old index that was 4 is now at position 0
    num_indexes_after = num_indexes_before - len(removed_indexes)
    new_to_old_indexes = np.zeros(num_indexes_after).astype(np.int32)
    new_pos = 0
    for old_pos in range(num_indexes_before):
        if old_pos not in removed_indexes:
            new_to_old_indexes[new_pos] = old_pos
            new_pos += 1
    return new_to_old_indexes


def single_image_to_lines(example, gt_offset=None, pred_offset=None, score_threshold=0.0):
    """
    get the csv lines of 1 image in the eval set
    :param example: the image dictionary with all the information
    :return: the csv lines (list of dictionaries)
    """
    result_lines = []
    # first remove all predictions which below score threshold
    num_preds_before_filter = len(example[pred_classes_key])
    example, removed_indexes = filter_example_by_score(example, score_threshold)
    # map the new index to old index of the predictions
    new_to_old_indexes = map_new_to_old_indexes(num_preds_before_filter, removed_indexes)
    # TODO(guy): need to filter out background gts and test it!!!
    gts = example[gt_boxes_key]
    preds = example[pred_boxes_key]

    # use linear assignment to match gt to prediction
    matches, unmatched_gts, unmatched_preds = confusion_matrix_util.associate_gts_to_predictions(gts, preds)
    # add all FPs
    for g in unmatched_gts:
        line = create_dict_line(example, g, -1, gt_offset=gt_offset)
        result_lines.append(line)
    # add all FNs
    for p in unmatched_preds:
        new_pred_offset = pred_offset + new_to_old_indexes[p] - p
        line = create_dict_line(example, -1, p, pred_offset=new_pred_offset)
        result_lines.append(line)
    # add all matches
    for m in matches:
        g, p = m
        iou = box_util.get_iou(gts[g], preds[p])
        new_pred_offset = pred_offset + new_to_old_indexes[p] - p
        line = create_dict_line(example, g, p, iou, gt_offset, new_pred_offset)
        result_lines.append(line)

    return result_lines


def get_analysis_lines(results_list, score_threshold=0.0, verbose=False):
    """
    get all the lines for the analysis csv file
    :param results_list: list of dictionaries that each one is an image dictionary in the eval set
    :return: the analysis lines
    """
    all_lines = []
    g_stats = defaultdict(int)
    p_stats = defaultdict(int)
    total_gts = 0
    total_preds = 0
    gt_offset = 0
    pred_offset = 0
    for result in results_list:
        # save the number of gts and preds before filtering by score
        num_gts_before_score_filter = len(result[gt_classes_key])
        num_preds_before_score_filter = len(result[pred_classes_key])
        # result is changed inside the function!
        result_lines = single_image_to_lines(result, gt_offset=gt_offset,
                                             pred_offset=pred_offset,
                                             score_threshold=score_threshold)  # every line is a dict so it's easier to write it to csv
        gt_offset += num_gts_before_score_filter
        pred_offset += num_preds_before_score_filter

        all_lines += result_lines

        gts = result[gt_classes_key]
        preds = result[pred_classes_key]
        for g in gts:
            g_stats[g] += 1
            total_gts += 1
        for p in preds:
            p_stats[p] += 1
            total_preds += 1
    if verbose:
        print('ground truth: %s' % g_stats)
        print('predictions: %s' % p_stats)
        print('total gts: %d' % total_gts)
        print('total preds: %d' % total_preds)
    return all_lines


def load_results_list_from_predictions_csv(stats_dir):
    input_csv_name = PREDICTION_CSV_NAME
    # get the data from input csv
    input_csv_path = os.path.join(stats_dir, input_csv_name)
    results_list = confusion_matrix_util.read_csv_to_dict(input_csv_path)
    add_img_extension_to_filenames(results_list)
    return results_list


def get_vis_full_frames_params(input_dict):
    params = {}
    params['gt_color_class_match'] = input_dict.get('gt_color_class_match', 'green')
    params['gt_color_class_unmatch'] = input_dict.get('gt_color_class_unmatch', 'red')
    params['thickness'] = input_dict.get('thickness', 2)
    params['show_scores'] = input_dict.get('show_scores', True)
    params['font_size'] = input_dict.get('font_size', 5)
    params['text_color'] = input_dict.get('text_color', 'white')
    params['text_bbox_color'] = input_dict.get('text_bbox_color', 'black')
    params['x_text_offset'] = input_dict.get('x_text_offset', 0)
    params['y_text_offset'] = input_dict.get('y_text_offset', 5)
    return params


def visualize_full_frames(gts_df, preds_df, eval_img_dir_path, score_th, iou_th,
                          **kwargs):
    # get all the parameters for this function. use defaults if user didn't provide some
    params = get_vis_full_frames_params(kwargs)
    original_img_dir_path = os.path.join(eval_img_dir_path, ORG_IMGS_DIR_NAME)
    cm_vis_dir_path = os.path.join(eval_img_dir_path,
                                   CM_VISUALIZE_DIR_NAME_PATTERN % (CM_VISUALIZE_DIR_NAME, score_th, iou_th))
    marked_img_dir_path = os.path.join(cm_vis_dir_path, MARKED_IMGS_DIR_NAME)
    # create the marked images dir
    cf.create_folder(marked_img_dir_path)

    org_image_names = os.listdir(original_img_dir_path)
    for idx in tqdm(range(len(org_image_names)), ascii=True, desc='visualize_full_frames'):
        img_name = org_image_names[idx]
        org_img_path = os.path.join(original_img_dir_path, img_name)
        org_img = plt.imread(org_img_path)
        # make a copy to draw on it
        img = copy.deepcopy(org_img)
        img_h, img_w = img.shape[:2]
        dpi = 1.5 * 96
        # first filter by filename
        relevant_gts = gts_df.loc[gts_df[filename_key] == img_name]
        relevant_preds = preds_df.loc[preds_df[filename_key] == img_name]
        # number of relevant matches
        relevant_size = relevant_gts.shape[0]
        # create new figure
        fig, ax = plt.subplots(1, figsize=(img_w * 1.5 / dpi, img_h * 1.5 / dpi), dpi=dpi)
        # hide grid lines
        ax.grid(False)
        # connect the image
        ax.imshow(img)
        # first draw gts
        for ind in range(relevant_size):
            gt_class = relevant_gts.iloc[ind][class_key]
            pred_class = relevant_preds.iloc[ind][class_key]
            # check if gt is background
            if gt_class == 0:
                continue
            # check if gt has prediction match (bbox)
            linestyle = '--'
            if pred_class != 0:
                linestyle = '-'
            # check if gt and pred classes are match
            color = params['gt_color_class_match']
            if gt_class != pred_class:
                color = params['gt_color_class_unmatch']
            y_min, x_min, y_max, x_max = relevant_gts.iloc[ind][box_key]
            xy = (x_min, y_min)
            box_w = np.abs(x_max - x_min)
            box_h = np.abs(y_max - y_min)
            rect = Rectangle(xy, box_w, box_h, color=color, fill=False, linestyle=linestyle,
                             linewidth=params['thickness'] + 1)
            # Add the rectangle
            ax.add_patch(rect)
        # draw predictions
        for ind in range(relevant_size):
            gt_class = relevant_gts.iloc[ind][class_key]
            pred_class = relevant_preds.iloc[ind][class_key]
            # check if pred is background
            if pred_class == 0:
                continue
            # check if pred has gt match (bbox)
            linestyle = '--'
            if gt_class != 0:
                linestyle = '-'
            color = PREDS_COLORS[pred_class - 1]
            score = int(relevant_preds.iloc[ind][score_key] * 100)
            y_min, x_min, y_max, x_max = relevant_preds.iloc[ind][box_key]
            xy = (x_min, y_min)
            box_w = np.abs(x_max - x_min)
            box_h = np.abs(y_max - y_min)
            rect = Rectangle(xy, box_w, box_h, color=color, fill=False, linestyle=linestyle,
                             linewidth=params['thickness'])
            # Add the rectangle
            ax.add_patch(rect)
            if params['show_scores']:
                # Add the score of bbox
                x_text = max(0, xy[0] + params['x_text_offset'])
                y_text = max(0, xy[1] - params['font_size'] + params['y_text_offset'])
                ax.text(x_text, y_text, ('%d' % score).zfill(2) + '%',
                        bbox=dict(facecolor=params['text_bbox_color'], pad=0., alpha=0.9), wrap=True,
                        color=params['text_color'], fontsize=params['font_size'])

        # save the marked image
        out_img_path = os.path.join(marked_img_dir_path, img_name)
        fig.canvas.draw()
        # convert to a numpy array.
        img = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='')
        img = img.reshape(fig.canvas.get_width_height()[::-1] + (3,))
        out_img = Image.fromarray(img)
        out_img.save(out_img_path)
        # plt.savefig(os.path.join(marked_img_dir_path, img_name))
        plt.close()


def visualize_cm_crops(eval_img_dir_path, gts, preds, score_th, iou_th, num_classes=None, categories=None):
    # both dict lists need to be in the same size
    assert len(gts) == len(preds)
    pred_crops_dir_path = os.path.join(eval_img_dir_path, PRED_CROPS_DIR_NAME)
    gt_crops_dir_path = os.path.join(eval_img_dir_path, GT_CROPS_DIR_NAME)
    gt_pred_dir_format = 'gt_%s_pr_%s'
    if not num_classes:
        # need to calculate the number of classes
        classes_set = set()
        for i in range(len(gts)):
            classes_set.add(gts[i][class_key])
            classes_set.add(preds[i][class_key])

        num_classes = max(classes_set)
    if not categories:
        # create id to id dictionary so we don't have to use 'if' in the function
        categories = {}
        for i in range(num_classes + 1):
            categories[i] = str(i)
    # create all the subfolders of groundtruth and prediction couples
    cm_vis_dir_path = os.path.join(eval_img_dir_path,
                                   CM_VISUALIZE_DIR_NAME_PATTERN % (CM_VISUALIZE_DIR_NAME, score_th, iou_th))
    cf.create_folder(cm_vis_dir_path)
    for i in range(num_classes + 1):
        for j in range(num_classes + 1):
            gt_pred_vis_dir_path = os.path.join(cm_vis_dir_path, gt_pred_dir_format % (categories[i], categories[j]))
            cf.create_folder(gt_pred_vis_dir_path)
    # now pass on the gts and predictions and softlink/copy the images to the correct sub folder
    for ind in range(len(gts)):
        gt_crop_ind = gts[ind][crop_key]
        gt_class = gts[ind][class_key]
        gt_crop_path = os.path.join(gt_crops_dir_path, str(gt_crop_ind) + IMG_EXTENSION)
        pred_crop_ind = preds[ind][crop_key]
        pred_class = preds[ind][class_key]
        pred_crop_path = os.path.join(pred_crops_dir_path, str(pred_crop_ind) + IMG_EXTENSION)
        # where to copy the image to
        dst_path = os.path.join(cm_vis_dir_path, gt_pred_dir_format % (categories[gt_class], categories[pred_class]))
        if gt_crop_ind is None or pred_crop_ind is None:
            # need only 1 crop
            if gt_crop_ind is None:
                # pred crop
                src_path = pred_crop_path
            else:
                # gt crop
                src_path = gt_crop_path
            # soft link the crop
            dst_path = os.path.join(dst_path, os.path.basename(src_path))
            if not os.path.exists(dst_path):
                os.symlink(src_path, dst_path)
            # copy crop
            # shutil.copy(src_path, dst_path)
        else:
            # combine 2 crops to 1 image and save it
            dst_path = os.path.join(dst_path, 'g_id_%d_p_id_%d%s' % (gt_crop_ind, pred_crop_ind, IMG_EXTENSION))
            pred_img_crop = cv2.imread(pred_crop_path)
            gt_img_crop = cv2.imread(gt_crop_path)
            if pred_img_crop is None or gt_img_crop is None:
                raise ValueError(
                    "couldn't find the pred or gt crop files. please check that you have %s and %s folders. "
                    "if not you might wanna run visualize_helper.py" % (GT_CROPS_DIR_NAME, PRED_CROPS_DIR_NAME))
            p_h, p_w = pred_img_crop.shape[:2]
            g_h, g_w = gt_img_crop.shape[:2]
            divider_size = 10
            new_h = np.maximum(p_h, g_h)
            new_w = p_w + g_w + divider_size
            new_img = np.zeros((new_h, new_w, 3))
            # compose new image by putting gt crop on the left and pred on the right
            new_img[:g_h, :g_w, :] = gt_img_crop
            new_img[:p_h, g_w + divider_size:, :] = pred_img_crop
            cv2.imwrite(dst_path, new_img)


def remove_analysis_lines_with_gt_touch_img_borders(analysis_lines, imgs_height, imgs_width):
    filtered_analysis_lines = []
    gt_count = 0
    gt_removed = 0
    for line in analysis_lines:
        # add all the lines which the gt bbox not touch the borders
        if line[gt_key_pre + class_key] == 0:
            filtered_analysis_lines.append(line)
            continue
        # has gt box
        gt_count += 1

        gt_xcoords = [line[gt_key_pre + xmax_key], line[gt_key_pre + xmin_key]]
        gt_ycoords = [line[gt_key_pre + ymax_key], line[gt_key_pre + ymin_key]]
        add_flag = True
        for x in gt_xcoords:
            if x == 0 or x == imgs_width:
                # drop this line
                add_flag = False
        for y in gt_ycoords:
            if y == 0 or y == imgs_height:
                # drop this line
                add_flag = False
        # else add this
        if add_flag:
            filtered_analysis_lines.append(line)
        else:
            gt_removed += 1
    print('After gt filtering: %d/%d' % (gt_count - gt_removed, gt_count))
    return filtered_analysis_lines


def update_analysis_lines_to_have_only_filename(analysis_lines):
    new_lines = copy.deepcopy(analysis_lines)
    for i in range(len(analysis_lines)):
        new_lines[i][filename_key] = os.path.basename(str(new_lines[i][filename_key]))
    return new_lines


def write_tracker_eval_results(metrics_per_track_dict, csv_path):
    with open(csv_path, "w") as outfile:
        outfile.write('Tracker evaluation:\n')
        writer = csv.writer(outfile)
        # write the headers
        writer.writerow(tracker_csv_headers)
        # write the metrics per track id
        for track_id, metrics in metrics_per_track_dict.items():
            val_list = [track_id] + [metrics[header] for header in tracker_csv_headers[1:]]
            writer.writerow(val_list)

    print('Done writing the tracker evaluation results in %s' % csv_path)


def eval_tracker(gts_df, preds_df, orig_video_fps=25):
    if gts_df.empty:
        raise ValueError('No matched gts!')
    # some of the gt boxes won't have track_id!
    track_ids = set(gts_df.loc[gts_df[gt_key_pre + track_key].values != None][gt_key_pre + track_key])
    # remove the empty track_id value from set
    if empty_track_val in track_ids:
        track_ids.remove(empty_track_val)
    metrics_per_track_dict = {}

    # define the namedtuple for track range
    track_frame_range = recordtype('track_frame_range', ['start', 'end', 'id'])
    for track_id in track_ids:
        # get the relevant gt and preds
        relevant_gts = gts_df.loc[gts_df[gt_key_pre + track_key] == track_id]

        frame_num_list = relevant_gts[filename_key].values.tolist()
        # extract the frame number from the string frame number format using regex
        frame_num_list = iu.get_frame_numbers_from_names_list(frame_num_list)

        sort_helper_list = [(i, num) for i, num in enumerate(frame_num_list)]
        # sort by the frame number
        sort_helper_list.sort(key=itemgetter(1))
        sorted_indexes = [t[0] for t in sort_helper_list]
        relevant_gts = relevant_gts.iloc[sorted_indexes]

        relevant_preds = preds_df.iloc[relevant_gts.index]

        # list of track_frame_range. if the id is None it means FN (gt but no prediction)
        pred_track_frame_ranges = []
        pred_ranges_size = []
        pred_class_list = relevant_preds[class_key].values.tolist()
        pred_track_list = relevant_preds[pred_key_pre + track_key].values.tolist()
        pred_frame_num_list = relevant_preds[filename_key].values.tolist()
        # extract the frame number from the string frame number format using regex
        pred_frame_num_list = iu.get_frame_numbers_from_names_list(pred_frame_num_list)
        # convert vmd match without track to None (it is because of num hits that the tracker doesn't put
        # track id to vmd match)
        for ind, t in enumerate(pred_track_list):
            if not t:
                pred_track_list[ind] = None
        assert len(pred_track_list) == len(pred_frame_num_list) == len(pred_class_list)
        if pred_track_list:
            # init
            pre_track = pred_track_list[0]
            t_range = track_frame_range(start=pred_frame_num_list[0], end=None, id=pre_track)
            pred_track_frame_ranges.append(t_range)
            for ind in range(1, len(pred_track_list)):
                curr_track = pred_track_list[ind]
                if curr_track != pre_track:
                    # close the previous sequence
                    last_t_range = pred_track_frame_ranges[-1]
                    last_t_range.end = pred_frame_num_list[ind - 1]
                    # calculate the range size
                    if last_t_range.id:
                        # has prediction match
                        range_size = last_t_range.end + 1 - last_t_range.start
                        pred_ranges_size.append(range_size)
                    # open new sequence
                    t_range = track_frame_range(start=pred_frame_num_list[ind], end=None, id=curr_track)
                    pred_track_frame_ranges.append(t_range)
                # update the previous track value
                pre_track = curr_track

            # calculate the last one
            # in case only 1 frame in the list
            if ind == len(pred_track_list):
                ind -= 1
            last_t_range = pred_track_frame_ranges[-1]
            last_t_range.end = pred_frame_num_list[ind]
            # calculate the range size
            if last_t_range.id:
                range_size = last_t_range.end + 1 - last_t_range.start
                pred_ranges_size.append(range_size)

        # summarize the metrics
        num_largest_seq_frames = max([0] + pred_ranges_size)
        num_track_breaks = max(0, len(pred_track_frame_ranges) - 1)
        # in case the first range with id==None
        if pred_track_frame_ranges[0].id == None and num_track_breaks > 0:
            num_track_breaks -= 1
        # calc the number of matched frames
        num_matched_frames = 0
        is_seq = False
        seq_start = 0
        seq_end = 0
        for t_range in pred_track_frame_ranges:
            if t_range.id:
                if is_seq:
                    seq_end = t_range.end
                else:
                    # need to start count the seq
                    seq_start = t_range.start
                    seq_end = t_range.end
                    is_seq = True
            else:
                if is_seq:
                    # need to add the seq to sum
                    num_matched_frames += seq_end + 1 - seq_start
                    is_seq = False
        # add the last one
        if is_seq:
            num_matched_frames += seq_end + 1 - seq_start
        num_total_frames = pred_track_frame_ranges[-1].end + 1 - pred_track_frame_ranges[0].start
        total_track_time_sec = float('%.5f' % (num_total_frames / float(orig_video_fps)))
        matched_track_percent = float('%.5f' % (num_matched_frames / float(num_total_frames)))
        largest_seq_track_percent = float('%.5f' % (num_largest_seq_frames / float(num_total_frames)))
        # build the metrics dict for this track id
        metrics_per_track_dict[track_id] = {total_track_time_sec_key: total_track_time_sec,
                                            matched_track_percent_key: matched_track_percent,
                                            largest_seq_track_percent_key: largest_seq_track_percent,
                                            num_track_breaks_key: num_track_breaks,
                                            matched_track_ranges_key: pred_track_frame_ranges,
                                            matched_ranges_size_key: pred_ranges_size}

    return metrics_per_track_dict


def calculate_analysis(results_list, save_dir, intervals_separation, iou_th=0.1, score_th=0.3, tp_fp_flag=True,
                       num_classes=None, visualize_cm=False, categories=None, verbose=False,
                       extra_gt_pred_match_info_key_list=None, original_video_fps=25, roi_bbox=None):
    """
    analysis calculation complete flow
    :param results_list: predictions and groundtruth data for all the images.
    what we saved in predictions.csv file. the bbox data is: [ymin,xmin,ymax,xmax]
    :param save_dir: where to read and write all the analysis files
    :param intervals_separation: the intervals list for the recall stats
    :param iou_th: iou threshold
    :param score_th: score threshold
    :param num_classes: number of classes scalar
    :param visualize_cm: should we visualize the confusion matrix?
    :param categories: dictionary of key=class_id and value=class_name
    :param verbose: should print debug messages?
    :param extra_gt_pred_match_info_key_list: extra values to store when matching gt to prediction (used for tracker eval)
    :param roi_bbox: list of bbox coords in format y1,x1,y2,x2 that define region of interest in the image or none
    for taking the whole image into account
    :return: none
    """

    # create the correct format interval list
    intervals_list = []
    for i in range(1, len(intervals_separation)):
        interval = I.closedopen(intervals_separation[i - 1], intervals_separation[i])
        intervals_list.append(interval)

    num_frames = len(results_list)

    # calculate the raw analysis
    analysis_lines = get_analysis_lines(results_list, score_threshold=score_th, verbose=verbose)

    # remove full path from filename field
    analysis_lines = update_analysis_lines_to_have_only_filename(analysis_lines)

    # write the raw analysis
    analysis_file_name = ANALYSIS_CSV_NAME.replace('.csv', '_score_%.2f.csv' % score_th)
    output_csv_path = os.path.join(save_dir, analysis_file_name)
    confusion_matrix_util.write_results_to_csv(analysis_lines, output_csv_path)

    # match iou by iou threshold
    preds, gts = get_matches_by_iou(analysis_lines, iou_th, extra_params_key_list=extra_gt_pred_match_info_key_list)

    # ROI filter
    if roi_bbox:
        preds, gts = filter_by_roi(preds, gts, roi_bbox, overlap_th=0.2)

    # convert to data frmes
    gts_df = pd.DataFrame(gts)
    preds_df = pd.DataFrame(preds)

    if visualize_cm:
        # visualize the cm with crops
        eval_img_dir_path = os.path.join(save_dir, EVAL_IMG_DIR_NAME)
        visualize_cm_crops(eval_img_dir_path, gts, preds, score_th, iou_th, num_classes=num_classes,
                           categories=categories)
        # this function can have kwargs. see 'get_vis_full_frames_params' for more details
        visualize_full_frames(gts_df, preds_df, eval_img_dir_path, score_th, iou_th, show_scores=True)

    # tracking evaluation
    if extra_gt_pred_match_info_key_list and gt_key_pre + track_key in extra_gt_pred_match_info_key_list:
        metrics_per_track_dict = eval_tracker(gts_df, preds_df, orig_video_fps=original_video_fps)
        # now write eval results to csv
        tracker_file_name = TRACKER_CSV_NAME.replace('.csv', '_iou_%.2f_score_%.2f.csv' % (iou_th, score_th))
        output_csv_path = os.path.join(save_dir, tracker_file_name)
        write_tracker_eval_results(metrics_per_track_dict, output_csv_path)

    # cm stats
    cm_stats = confusion_matrix_util.get_confusion_matrix_stats(preds, gts, categories=categories, verbose=verbose,
                                                                iou_thresh=iou_th, score_thresh=score_th,
                                                                num_frames=num_frames)
    confusion_matrix_util.write_cm_stats_to_csv(cm_stats, save_dir, iou_thresh=iou_th, score_thresh=score_th)
    # recall stats
    recall_stats = confusion_matrix_util.get_recall_stats(preds, gts, intervals_list=intervals_list,
                                                          num_classes=num_classes)
    confusion_matrix_util.write_recall_stats_to_csv(recall_stats, save_dir, iou_thresh=iou_th,
                                                    score_thresh=score_th)
    return cm_stats
    # roc curve
    # TODO: has bug so for now comment this
    # get_roc_detection_class_agnostic(preds, gts, save_dir, iou_thresh=iou_th, tp_fp_flag=tp_fp_flag, verbose=verbose)


def get_categories(label_map_path):
    categories_list = label_map_util.load_labelmap_categories(label_map_path)
    # change the format to dictionary of key-name pairs
    categories_dict = {}
    # also build reverse dictionary of name-key pairs
    categories_reversed_dict = {}
    # insert background
    categories_dict[0] = 'background'
    categories_reversed_dict['background'] = 0
    for category in categories_list:
        categories_dict[category['id']] = category['name']
        categories_reversed_dict[category['name']] = category['id']
    return categories_dict, categories_reversed_dict


def convert_arg_str_to_num_list(st_list):
    num_list = []
    for st in st_list:
        if st == 'inf':
            num_list.append(I.inf)
        else:
            # assume number
            num_list.append(int(st))
    return num_list


def add_prefix_to_filenames(prefix, results_list):
    for res in results_list:
        res[filenames_key] = prefix + '_' + res[filenames_key]


def add_img_extension_to_filenames(results_list):
    for res in results_list:
        if not str(res[filenames_key]).endswith(IMG_EXTENSION):
            res[filenames_key] = str(res[filenames_key]) + IMG_EXTENSION


def run(args):
    # define intervals
    intervals_separation = convert_arg_str_to_num_list(args.intervals)

    # get the mandatory arguments
    stats_dir = args.stats_dir
    label_map_path = args.label_map_path
    iou_th = args.iou_th
    score_th = args.score_th

    # get the optional arguments
    gt_csvs = args.gt_csv
    pred_csvs = args.pred_csv
    vis_cm = args.visualize_cm
    org_fps = args.original_video_fps
    roi_bbox = args.roi_bbox
    metro_template_name = args.metro_template_name

    if roi_bbox is not None:
        roi_bbox = convert_arg_str_to_num_list(roi_bbox)

    categories, reversed_categories = get_categories(label_map_path)
    extra_gt_pred_match_info_key_list = None

    if gt_csvs and pred_csvs:
        # eval tracker
        results_list = []
        assert len(gt_csvs) == len(pred_csvs)
        for ind in range(len(gt_csvs)):
            gt_csv = gt_csvs[ind]
            pred_csv = pred_csvs[ind]
            prefix = os.path.splitext(os.path.basename(gt_csv))[0]
            tmp_results_list = read_csv_to_result_list(gt_csv, pred_csv, categories=reversed_categories,
                                                       complete_gt_frames=False, allowed_gt_range=args.gt_range)
            add_prefix_to_filenames(prefix, tmp_results_list)
            # accumulate the result list for all the csv files
            results_list.extend(tmp_results_list)

        extra_gt_pred_match_info_key_list = ['gt_track_id', 'pred_track_id']
    else:
        # regular run
        results_list = load_results_list_from_predictions_csv(stats_dir)

    num_classes = len(categories) - 1

    # create output folder
    cf.create_folder(stats_dir)

    # calculate and write the analysis
    cm_stats = calculate_analysis(results_list, stats_dir, intervals_separation, iou_th=iou_th, score_th=score_th,
                                  num_classes=num_classes, categories=categories, visualize_cm=vis_cm, verbose=False,
                                  extra_gt_pred_match_info_key_list=extra_gt_pred_match_info_key_list,
                                  original_video_fps=org_fps, roi_bbox=roi_bbox)

    # TODO: INCOMPLETE CODE
    # write stats to the metronome excel
    save_path = os.path.join(stats_dir, 'metronome.xlsx')
    template_xlsx_path = os.path.join(os.path.abspath(''), 'doc', metro_template_name)
    write_metronome_stats(template_xlsx_path, save_path, cm_stats, categories)


def parse_args():
    """
    parse command line arguments
    :return:
    """
    parser = argparse.ArgumentParser(description="Eval Analysis")
    parser.add_argument(
        "--stats_dir", help="Path the statistics directory", required=True)
    parser.add_argument(
        "--label_map_path", help="Path to object detection label map.", required=True)
    parser.add_argument("--iou_th", help="iou threshold", default=0.5, type=float)
    parser.add_argument("--score_th", help="score threshold", default=0.3, type=float)
    parser.add_argument("--intervals", nargs="+", default=[0, 10, 20, 30, 40, 'inf'], required=False)
    parser.add_argument("--gt_csv", nargs="+", help="List of paths to the gts csv")
    parser.add_argument("--pred_csv", nargs="+", help="List of paths to the predictions csv")
    parser.add_argument("--gt_range",
                        help="Allowed range for prediction frame number to convert to the nearest gt frame number",
                        default=0, type=int)
    parser.add_argument("--visualize_cm", help="Should we try to visualize the confusion matrix?", action='store_true')
    parser.add_argument("--original_video_fps", help="The pipeline video original fps (only for tracker statistics)",
                        default=25, type=int)
    parser.add_argument("--roi_bbox", nargs="+", default=None, required=False)
    parser.add_argument("--metro_template_name", help="The metronome template file name located in the doc folder",
                        default="Metronome_Report_template.xlsx", required=False)

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    run(args)
