# label map as defined in tensorflow object detection api


def load_labelmap_categories(path):
    with open(path, 'r') as label_file:
        lines = label_file.readlines()
        # remove new line ('\n') and empty lines
        lines = [l.strip() for l in lines if l.strip()]
    categories = []
    category = {}
    for line in lines:
        if line.find('}') != -1:
            if category:
                categories.append(category)
                category = {}
        if line.find(':') != -1:
            key, val = line.split(':')
            key = key.strip()
            val = val.strip()
            if key == 'id':
                val = int(val)
            if key == 'name':
                val = val.replace("'", "")
            category[key] = val
    return categories
