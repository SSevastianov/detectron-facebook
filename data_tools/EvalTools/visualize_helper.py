import argparse
import os
from data_tools.DataManipulation import common_funcs as cf
import imageio
import data_tools.EvalTools.eval_analysis as ea
from data_tools.utils import img_util as iu
import numpy as np
from tqdm import tqdm
from PIL import Image


def parse_args():
    """
    parse command line arguments
    :return:
    """
    parser = argparse.ArgumentParser(description="Visualize pipeline helper")
    parser.add_argument("--vid_path", help="Path to the video file", required=True)
    parser.add_argument(
        "--stats_dir", help="Path the statistics directory", required=True)
    parser.add_argument(
        "--label_map_path", help="Path to object detection label map.", required=True)
    parser.add_argument("--gt_csv", help="Path to the gt csv")
    parser.add_argument("--pred_csv", help="Path to the prediction csv")
    parser.add_argument("--gt_range",
                        help="Allowed range for prediction frame number to convert to the nearest gt frame number",
                        default=0, type=int)

    return parser.parse_args()

# Use this script to create the crops and full images for visualize_cm in the eval_analysis pipeline mode!
if __name__ == '__main__':
    args = parse_args()

    pred_id_counter = 0
    gt_id_counter = 0
    categories, reversed_categories = ea.get_categories(args.label_map_path)
    results_list = ea.read_csv_to_result_list(args.gt_csv, args.pred_csv, categories=reversed_categories,
                                              complete_gt_frames=False, allowed_gt_range=args.gt_range)
    # create the output folders
    eval_images_dir_path = os.path.join(args.stats_dir, ea.EVAL_IMG_DIR_NAME)
    gt_crops_dir_path = os.path.join(eval_images_dir_path, ea.GT_CROPS_DIR_NAME)
    cf.create_folder(gt_crops_dir_path)
    pred_crops_dir_path = os.path.join(eval_images_dir_path, ea.PRED_CROPS_DIR_NAME)
    cf.create_folder(pred_crops_dir_path)
    org_images_dir_path = os.path.join(eval_images_dir_path, ea.ORG_IMGS_DIR_NAME)
    cf.create_folder(org_images_dir_path)

    # get the frame numbers
    frame_names = [res[ea.filenames_key] for res in results_list]
    frame_nums = iu.get_frame_numbers_from_names_list(frame_names)
    # for checking if some frames are missing
    processed_frames = []
    ###-------- IMPORTANT NOTE---------###
    # for sync purpose - the 2 lines below are patch that worked for me. use it carefully!!!
    skip_frames = 0
    start_num = 1
    ###--------------------------------###
    # read the frames from video and write them
    vid_reader = imageio.get_reader(args.vid_path, 'ffmpeg')
    for frame_id, im_frame in enumerate(tqdm(vid_reader)):
        if frame_id < skip_frames:
            continue
        # need to increase by start_num because this is the real frame number
        curr_frame_num = frame_id + start_num - skip_frames
        if curr_frame_num in frame_nums:
            processed_frames.append(curr_frame_num)
            res_dict = results_list[frame_nums.index(curr_frame_num)]
            # save full frame
            img_path = os.path.join(org_images_dir_path, os.path.splitext(os.path.basename(args.gt_csv))[
                0] + '_' + ea.EVAL_SYSTEM_FRAME_NAME_FORMAT % curr_frame_num + ea.IMG_EXTENSION)
            imageio.imwrite(img_path, im_frame)
            # now get the crops
            pred_crops, gt_crops = ea.get_crops(im_frame, res_dict[ea.pred_boxes_key],
                                                res_dict[ea.gt_boxes_key], padding=10)
            # save pred crops
            for crop in pred_crops:
                out_img_path = os.path.join(pred_crops_dir_path, str(pred_id_counter) + ea.IMG_EXTENSION)
                out_img = Image.fromarray((crop).astype(np.uint8))
                out_img.save(out_img_path)
                pred_id_counter += 1
            # save gt crops
            for crop in gt_crops:
                out_img_path = os.path.join(gt_crops_dir_path, str(gt_id_counter) + ea.IMG_EXTENSION)
                out_img = Image.fromarray((crop).astype(np.uint8))
                out_img.save(out_img_path)
                gt_id_counter += 1

    diff = set(frame_nums) - set(processed_frames)
    if len(diff) > 0:
        print('Warning! csv has data on frames that are not sync: %s' % list(diff))
