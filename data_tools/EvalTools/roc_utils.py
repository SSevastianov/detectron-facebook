# TODO: need to fix this!
import numpy as np
from sklearn.metrics import roc_curve, auc
import matplotlib
import tensorflow as tf

# matplotlib.use("Agg")
import matplotlib.pyplot as plt  # CR - PEP8 warning


def roc_image_summary(executioner, writer, summary_name, fpr, tpr, roc_auc, sens_at_spec09):
    roc_fig = get_figure()
    roc_image = plot_roc(fpr, tpr, roc_auc, sens_at_spec09, roc_fig)

    graph = tf.Graph()
    with graph.as_default():
        roc_placeholder = tf.placeholder(tf.uint8, fig2rgb_array(roc_fig).shape)
        with tf.name_scope(executioner.core_graph.get_graph_scope_name()):
            roc_summary = tf.summary.image(summary_name, roc_placeholder)
        feed_dict = {roc_placeholder: roc_image}

        with tf.Session() as sess:
            roc_out = sess.run(roc_summary, feed_dict=feed_dict)
        writer.add_summary(roc_out)


def calculate_roc_stats(labels, probabilities):
    fpr, tpr, _ = roc_curve(labels, probabilities)
    roc_auc = auc(fpr, tpr)
    sens_at_spec09 = np.interp(0.1, fpr, tpr)
    return fpr, tpr, roc_auc, sens_at_spec09


def plot_roc(xr, yr, roc_auc, figure, sens_at_spec09=None, tp_fp_flag=True):
    '''
    :param xr: horizontal axis variable
    :param yr: vertical axis variable
    :param roc_auc: AreaUnderCurve
    :param figure: pointer to output figure
    :param sens_at_spec09: sensitivity at specificity 0.9
    :param tp_fp_flag: if True: plots tp/fp ROC. else: plots FDR/RECALL graph
    :return: ROC curve image
    '''
    figure.clf()
    plt.plot([0, 1], [0, 1], "k--", lw=1)
    plt.plot([0.1, 0.1], [0, 1.05], "g-", lw=2)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    if tp_fp_flag:
        plt.plot(xr, yr, lw=2, color="r", label="ROC curve (area = {:.3f})".format(roc_auc))
        plt.xlabel("False Positive Rate")
        plt.ylabel("True Positive Rate")
    else:
        plt.plot(xr, yr, lw=2, color="r", label="FDR-Recall curve")
        plt.xlabel("FDR Rate")
        plt.ylabel("Recall Rate")
    # plt.title("Receiver operating characteristic")
    plt.legend(loc="lower right")
    if sens_at_spec09:
        s = "Sensitivity = {:.4f}, at\nSpecificity = {:.2f}".format(sens_at_spec09, 0.9)
        plt.text(0.6, 0.2, s, bbox=dict(facecolor="red", alpha=0.5))

    image = fig2rgb_array(figure)

    return image


# code is adapted from:
# https://stackoverflow.com/questions/38543850

def get_figure():
    fig = plt.figure(num=0, figsize=(6, 4), dpi=300)
    fig.clf()
    return fig


def fig2rgb_array(fig, expand=True):
    fig.canvas.draw()
    buf = fig.canvas.tostring_rgb()
    ncols, nrows = fig.canvas.get_width_height()
    shape = (nrows, ncols, 3) if not expand else (1, nrows, ncols, 3)
    return np.fromstring(buf, dtype=np.uint8).reshape(shape)

# def usage_example():
#     probs = recipe_predictions_prob[indices_to_use]
#
#     labels = all_labels[indices_to_use]
#     flat_labels = dataset_utils.flatten_labels(labels)
#
#     fpr, tpr, roc_auc, sens_at_spec09 = roc_utils.calculate_roc_stats(flat_labels,
#                                                                       probs)
#
#     roc_writer = self._recipe_exec.core_graph.get_fold_roc_writer()
#     roc_utils.roc_image_summary(self._recipe_exec,
#                                 roc_writer,
#                                 "{}_{}".format("segment_roc", scope_suffix),
#                                 fpr, tpr,
#                                 roc_auc,
#                                 sens_at_spec09)
