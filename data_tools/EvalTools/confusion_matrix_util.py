import pandas as pd
from pandas_ml.confusion_matrix import LabeledConfusionMatrix
import logging
import csv
import os
import ast
import numpy as np
from collections import defaultdict, OrderedDict
from scipy.optimize import linear_sum_assignment
from data_tools.utils.box_util import get_iou

# from sklearn.utils.linear_assignment_ import linear_assignment

# determine the logging level
logging.basicConfig(level=logging.INFO)

BACKGROUND_KEY = 'background'
VMD_KEY = 'vmd'


def convert_str_to_correct_type(example_vals_str):
    """
    :param example_vals_str: input string readed from csv file (e.g Predictions.csv)
    :return: converted string to it original type (list/number etc)

    removing multiple spaces, removing last space before ] and replacing spaces with commas for using liter_eval
    literal_eval converts string to list
    """
    example_vals = []
    for val in example_vals_str:
        if '[' in val:
            example_vals.append(
                ast.literal_eval(' '.join(val.split()).replace(' ]', ']').replace('[ ', '[').replace(' ', ',')))
        else:
            # in case of number or string
            try:
                example_vals.append(ast.literal_eval(val))
            except:
                example_vals.append(val)
    return example_vals


def read_csv_to_dict(csv_path):
    """
    read a csv line to a dictionary and return the list of lines/dictionaries
    :param csv_path: the full path to the csv file
    :return: list of dictionaries
    """
    readed_dicts_list = []
    with open(csv_path, 'rb') as csv_file:
        reader = csv.reader(csv_file)
        list_from_csv = list(reader)
    dict_keys = list_from_csv[0]
    examples_list = list_from_csv[1:]
    for example_vals_str in examples_list:
        # change values to correct type
        example_vals = convert_str_to_correct_type(example_vals_str)
        example_dict = dict(zip(dict_keys, example_vals))
        readed_dicts_list.append(example_dict)

    print('finished reading predictions from csv: {}'.format(csv_path))
    return readed_dicts_list


def get_class_agnostic_cm(cm_data):
    #  0 class is background, and the rest classes grouped into targets class.
    class_ag_data = [[cm_data[0, 0], sum(cm_data[0, 1:])], [sum(cm_data[1:, 0]), sum(sum(cm_data[1:, 1:]))]]
    class_ag_labels = ['Background', 'Target']
    class_agnostic_cm = pd.DataFrame(data=class_ag_data, index=class_ag_labels, columns=class_ag_labels)
    return class_agnostic_cm


def get_class_agnostic_stats(cm_data, classes):
    cls_agnostic_stats = pd.DataFrame(columns=classes)
    num_cls = len(classes)
    gt_sums = np.sum(cm_data, axis=1)
    # fp agnostic
    false_positives = cm_data[0, :].copy()
    # background calculation update
    false_positives[0] = np.sum(cm_data[1:, 0])
    # tp agnostic
    true_positives = gt_sums - cm_data[:, 0]
    # background calculation update
    true_positives[0] = cm_data[0, 0]
    # fn agnostic
    false_negatives = cm_data[:, 0].copy()
    # background calculation update
    false_negatives[0] = np.sum(cm_data[0, 1:])
    # tn agnostic
    true_negatives = np.repeat(cm_data[0, 0], num_cls)
    # background calculation update
    true_negatives[0] = np.sum(cm_data[1:, 1:])

    # metrics
    precision = true_positives.astype(np.float32) / (true_positives + false_positives)
    recall = true_positives.astype(np.float32) / (true_positives + false_negatives)
    fdr = false_positives.astype(np.float32) / (false_positives + true_positives)

    # add all the info to dataframe
    cls_agnostic_stats.loc['TP: True Positive'] = true_positives
    cls_agnostic_stats.loc['TN: True Negative'] = true_negatives
    cls_agnostic_stats.loc['FP: False Positive'] = false_positives
    cls_agnostic_stats.loc['FN: False Negative'] = false_negatives
    cls_agnostic_stats.loc['Precision'] = precision
    cls_agnostic_stats.loc['Recall'] = recall
    cls_agnostic_stats.loc['False Discovery Rate'] = fdr

    return cls_agnostic_stats


def write_recall_stats_to_csv(recall_stats, path, iou_thresh=0.5, score_thresh=0.3):
    """
    write the recall stats to a csv
    :param recall_stats: the stats object
    :param path: the csv folder full path
    :param iou_thresh: the iou threshold used for calculating the recall stats
    :param score_thresh: the score threshold used for calculating the recall stats
    :return: none
    """
    file_path = os.path.join(path, "recall_stats_iou{}_score{}.csv".format(iou_thresh, score_thresh))
    if len(recall_stats) == 0:
        print('Error in writing the recall stats')
        return
    with open(file_path, "w") as outfile:
        outfile.write('Recall per class and per interval:\n')
        writer = csv.writer(outfile)
        writer.writerow(['class/interval'] + list(recall_stats.keys()))
        recall_by_class = {}
        num_classes = len(list(recall_stats.values())[0].keys())
        # init the recall_by_class object
        for cls_num in range(1, num_classes + 1):
            recall_by_class[cls_num] = []
        # assume the recall_stats is an OrderedDict
        for interval_dict in recall_stats.values():
            for cls_num in range(1, num_classes + 1):
                recall_calc_str = '%d / %d = %.3f' % (
                    interval_dict[cls_num]['correct'], interval_dict[cls_num]['total'],
                    interval_dict[cls_num]['recall'])
                recall_by_class[cls_num].append(recall_calc_str)

        for cls_num in range(1, num_classes + 1):
            writer.writerow([cls_num] + recall_by_class[cls_num])


def get_confusion_matrix_stats(preds, gts, iou_thresh, score_thresh, categories=None, verbose=False, **kwargs):
    """
    calculate the confusion matrix stats
    :param preds: the predictions
    :param gts: the groundtruths
    :param iou_thresh: the iou threshold
    :param score_thresh: the score threshold
    :param categories: dictionary of label categories id:name
    :param verbose: should we print to console more info?
    :return: cm stats
    """
    all_preds = [p["class"] for p in preds]
    all_gt = [g["class"] for g in gts]
    if set(all_preds) == set(all_gt) and len(set(all_preds)) < 2:
        if not all_preds:
            raise ValueError('confusion matrix calculation failed due to 0 predictions!')
        # the first element addition is to prevent a bug where we have only 1 class in the 2 vectors
        all_preds = [0] + all_preds
        all_gt = [0] + all_gt
    labels = None
    if categories:
        labels = list(categories.values())

    # this should fix the overflow when calc the cm stats
    all_gt = list(map(np.uint64, all_gt))
    all_preds = list(map(np.uint64, all_preds))

    cm = LabeledConfusionMatrix(all_gt, all_preds, labels=labels)
    cm_stats = cm.stats()
    if verbose:
        cm.print_stats()
        print('Predictions: ' + str(all_preds))
        print('Groundtruth: ' + str(all_gt))

    # collect more info
    cm_key, overall_key, class_key = ['cm', 'overall', 'class']
    # get the cm classes list
    cm_classes = list(cm_stats[class_key].columns)
    # columns to remove in some stats
    cols_to_remove = []
    if categories:
        # add background and vmd
        cols_to_remove.append(BACKGROUND_KEY)
        if VMD_KEY in cm_classes:
            cols_to_remove.append(VMD_KEY)
    else:
        # add only background
        cols_to_remove.append(0)

    cm_np = cm_stats[cm_key].to_array()

    TP_per_class = cm_stats[class_key].iloc[5]
    FP_per_class = cm_stats[class_key].iloc[7]
    FN_per_class = cm_stats[class_key].iloc[8]

    # add vmd to TP_per_class and delete the cols from the per class counters
    if VMD_KEY in cols_to_remove:
        # add vmd
        vmd_col_ind = cm_classes.index(VMD_KEY)
        TP_per_class[1:] += cm_np[1:, vmd_col_ind]
        FN_per_class[1:] -= cm_np[1:, vmd_col_ind]
    for counter in [TP_per_class, FP_per_class, FN_per_class]:
        counter.drop(cols_to_remove, inplace=True)

    recall_per_class = TP_per_class.divide(TP_per_class.add(FN_per_class), fill_value=0)
    precision_per_class = TP_per_class.divide(TP_per_class.add(FP_per_class), fill_value=0)
    fdr_per_class = 1 - precision_per_class

    total_gts = np.sum(cm_np) - np.sum(cm_np[0])

    if cm_stats[overall_key]['Kappa'] == 1.:
        # for case where there is no background because all gt and predictions are 100% match
        new_cm_size = cm_np.shape[0] + 1
        new_cm_np = np.zeros((new_cm_size, new_cm_size))
        new_cm_np[1:, 1:] = cm_np
        cm_np = new_cm_np
    class_agnostic_cm_dataframe = get_class_agnostic_cm(cm_np)

    overall_data = []
    if 'num_frames' in kwargs.keys():
        overall_data.append(['GT Frames:', kwargs['num_frames']])
    overall_data.append(['GT Boxes:', total_gts])
    for key, value in cm_stats[overall_key].items():
        overall_data.append([key, value])

    epsilon = 1e-8  # add epsilon so we won't divide by zero and get exception
    gt_t_pred_t = class_agnostic_cm_dataframe.at['Target', 'Target']
    gt_b_pred_t = class_agnostic_cm_dataframe.at['Background', 'Target']
    gt_t_pred_b = class_agnostic_cm_dataframe.at['Target', 'Background']
    class_agnostic_precision = 1.0 * gt_t_pred_t / (gt_b_pred_t + gt_t_pred_t + epsilon)
    class_agnostic_recall = 1.0 * gt_t_pred_t / (gt_t_pred_b + gt_t_pred_t + epsilon)
    class_agnostic_fdr = 1 - class_agnostic_precision
    naive_mAR = recall_per_class.mean()
    naive_mAP = precision_per_class.mean()
    naive_mFDR = fdr_per_class.mean()

    avg_data = [['Class-Agnostic Precision:', class_agnostic_precision],
                ['Class-Agnostic Recall:', class_agnostic_recall], ['Class-Agnostic FDR:', class_agnostic_fdr],
                ['Naive mAP:', naive_mAP], ['Naive mAR:', naive_mAR], ['Naive mFDR:', naive_mFDR]]

    class_agnostic_stats = get_class_agnostic_stats(cm_np, list(cm_stats[class_key]))
    # remove background and vmd columns
    class_agnostic_stats.drop(cols_to_remove, axis=1, inplace=True)

    # accumulate all the stats
    all_stats = OrderedDict()

    # thresholds
    all_stats['Thresholds'] = pd.Series(data=[iou_thresh, score_thresh], index=['iou_thresh:', 'score_thresh:'])
    # class
    all_stats['Class'] = cm_stats[class_key]
    # class agnostic
    all_stats['Class Agnostic'] = class_agnostic_stats
    # confusion matrix
    all_stats['Confusion-Matrix'] = cm_stats[cm_key].to_dataframe(calc_sum=True)
    all_stats['Confusion-Matrix'].index.name = 'Actual / Predicted'
    # overall
    over_key, over_val = zip(*overall_data)
    all_stats['Overall'] = pd.Series(data=over_val, index=over_key)
    # Class-Agnostic Confusion-Matrix
    all_stats['Class-Agnostic Confusion-Matrix'] = class_agnostic_cm_dataframe
    # Precision per class
    all_stats['Precision per class'] = precision_per_class
    # Recall per class
    all_stats['Recall per class'] = recall_per_class
    # FDR per class
    all_stats['FDR per class'] = fdr_per_class
    # Averages
    avg_key, avg_val = zip(*avg_data)
    all_stats['Averages'] = pd.Series(data=avg_val, index=avg_key)
    return all_stats


def write_cm_stats_to_csv(cm_stats, dir_path, iou_thresh=0.5, score_thresh=0.3):
    """
    write the confusion matrix stats
    :param cm_stats: the confusion matrix stats object
    :param dir_path: the csv folder full path
    :param iou_thresh: the iou threshold used for calculating the recall stats
    :param score_thresh: the score threshold used for calculating the recall stats
    :return: none
    """
    file_path = os.path.join(dir_path, "cm_stats_iou{}_score{}.csv".format(iou_thresh, score_thresh))

    with open(file_path, "w") as outfile:
        writer = csv.writer(outfile)
        for title, data in cm_stats.items():
            is_header = isinstance(data, pd.DataFrame)
            writer.writerow([title + ':'])
            data.to_csv(outfile, header=is_header)
            writer.writerow([''])  # insert blank row

    logging.info('Wrote CM stats to: {}'.format(file_path))


def write_results_to_csv(results_list, file_path):
    """
    write list of dictionaries to a csv
    :param results_list: list of dictionaries
    :param file_path: the csv full path
    :return: none
    """
    all_keys = sorted(
        set().union(*(d.keys() for d in results_list[:100])))  # hoping that all keys appear in the first k items

    with open(file_path, 'w') as output_file:
        dict_writer = csv.DictWriter(output_file, all_keys)
        dict_writer.writeheader()
        dict_writer.writerows(results_list)
    logging.info("results csv file recorded at: {}".format(file_path))


def clean_result_dict(result_dict):
    # removes unneeded parts for CM. helps for RAM consumption and better CSV exporting
    new_result_dict = result_dict.copy()
    # remove unneeded keys
    relevant_keys = ["detection_scores", "detection_boxes", "detection_classes", "groundtruth_classes",
                     "groundtruth_boxes", "key"]  # key is filename
    remove_elements = [key for key in result_dict.keys() if key not in relevant_keys]
    for key in remove_elements:
        del new_result_dict[key]

    # remove 0 score predictions
    relevant_boxes_idx = get_index_below_thresh(new_result_dict["detection_scores"], 0)
    if relevant_boxes_idx:
        new_result_dict["detection_boxes"] = new_result_dict["detection_boxes"][:relevant_boxes_idx]
        new_result_dict["detection_classes"] = new_result_dict["detection_classes"][:relevant_boxes_idx]
        new_result_dict["detection_scores"] = new_result_dict["detection_scores"][:relevant_boxes_idx]
    return new_result_dict


def get_index_below_thresh(myList, thresh):
    '''
    returns the index of the first element in a decending ordered list that is below the specified threshold.
    '''
    return next((i for i in range(len(myList)) if myList[i] <= thresh), len(myList))


# taken from sort.py -> associate_detections_to_tracker and modified it
def associate_gts_to_predictions(gts, preds):
    """
    Assigns groundtruths to predictions objects (both represented as bounding boxes)

    Returns 3 lists of matches, unmatched_gts and unmatched_preds (values are indices)
    """
    if len(preds) == 0:
        return np.empty((0, 2), dtype=int), np.arange(len(gts)), np.empty((0, 5), dtype=int)
    iou_matrix = np.zeros((len(gts), len(preds)), dtype=np.float32)

    for g, gt in enumerate(gts):
        for p, pred in enumerate(preds):
            iou_matrix[g, p] = get_iou(gt, pred)
    matched_indices = linear_sum_assignment(-iou_matrix)
    matched_indices = np.stack(matched_indices, axis=1)

    unmatched_gts = []
    for g, gt in enumerate(gts):
        if g not in matched_indices[:, 0]:
            unmatched_gts.append(g)
    unmatched_preds = []
    for p, pred in enumerate(preds):
        if p not in matched_indices[:, 1]:
            unmatched_preds.append(p)

    matches = []
    for m in matched_indices:
        g = m[0]
        p = m[1]
        if iou_matrix[g, p] == 0:
            # iou == 0
            # divide this into unmatched gt and unmatched pred
            unmatched_gts.append(g)
            unmatched_preds.append(p)
        else:
            # iou > 0
            matches.append(m.reshape(1, 2))
    if len(matches) == 0:
        matches = np.empty((0, 2), dtype=int)
    else:
        matches = np.concatenate(matches, axis=0)

    return matches, np.array(unmatched_gts), np.array(unmatched_preds)


def is_box_axis_in_interval(box, interval, is_long_axis=True):
    # box is list of: xmin, ymin, xmax, ymax
    dx = box[2] - box[0]
    dy = box[3] - box[1]
    if is_long_axis:
        axis_length = max(dx, dy)
        logging.debug('axis length for recall stats is LONG AXIS')
    else:
        axis_length = min(dx, dy)
        logging.debug('axis length for recall stats is SHORT AXIS')
    return axis_length in interval


def get_recall_per_class_by_size_interval(preds, gts, size_interval, classes, is_long_axis=True):
    """
    calculate the recall for each class after filter out all the bboxes that are not in the size_interval
    :param preds: the predictions
    :param gts: the groundtruths
    :param size_interval: the interval to filter the bboxes
    :param classes: the classes
    :param is_long_axis: should we filter by long or short axis
    :return: recall per class
    """
    assert len(preds) == len(gts)
    total_preds = defaultdict(float)
    correct_preds = defaultdict(float)
    for ind in range(len(preds)):
        pred = preds[ind]
        gt = gts[ind]
        if gt["class"] == 0:
            # don't count false alarm examples
            continue
        # check if the gt box's long axis is in the size interval
        if not is_box_axis_in_interval(gt["box"], size_interval, is_long_axis=is_long_axis):
            # don't count gt box examples that are not in the correct filter size
            continue
        total_preds[gt["class"]] += 1
        # check if pred is correct
        if pred["class"] == gt["class"]:
            correct_preds[gt["class"]] += 1
    # calculate recall per class
    recall_per_class = {}
    for cls in classes:
        if cls == 0:
            # skip background class
            continue
        recall = float('NaN') if total_preds[cls] == 0 else float(correct_preds[cls]) / total_preds[cls]
        recall_per_class[cls] = {'recall': recall, 'total': total_preds[cls], 'correct': correct_preds[cls]}
    return recall_per_class


def get_recall_stats(preds, gts, intervals_list=None, num_classes=None):
    """
    calculate the dictionary of recall per interval and inside there is also separation per class
    :param preds: the predictions
    :param gts: the groundtruths
    :param intervals_list: the intervals
    :return: recall stats
    """
    if not intervals_list:
        logging.info('No intervals for recall stats, skipping...')
        return {}
    if num_classes is None:
        # try to infer the number of classes
        num_classes = max([g["class"] for g in gts])
    # create the list of classes_ids
    classes = list(range(1, 1 + num_classes))
    recall_stats = OrderedDict()
    for interval in intervals_list:
        recall_stats[interval] = get_recall_per_class_by_size_interval(preds, gts, interval, classes)
    return recall_stats


if __name__ == "__main__":
    pass
