# TODO: need to fix this!
from roc_utils import calculate_roc_stats, plot_roc
from confusion_matrix_util import get_iou, read_csv_to_dict
from sklearn.metrics import precision_recall_curve

import matplotlib
# matplotlib.use("Agg")
import matplotlib.pyplot as plt
import os


def get_roc_detection_class_agnostic(preds, gts, save_dir, iou_thresh=0.1, tp_fp_flag=True, verbose=False):
    '''
    :param preds, gts: iou thresholded predictions and groundtruths
    :param save_dir: directory to save plot image
    :param iou_thresh: used only for titles
    :param tp_fp_flag: True: plots tp/fp ROC. False: plots FDR/RECALL graph
    :return: nothing. saves image locally
    '''

    # get labels (agnostic->0 vs all) and scores (using iou thershing and post processing)
    # 0 considered as background (negative) and the rest of the labels are targets (positive)
    all_labels, all_scores = [], []
    for i in range(len(preds)):
        if preds[i]['class'] == 0:  # FN aren't interesting
            continue
        if gts[i]['class'] == 0:  # FP
            all_labels.append(0)
            all_scores.append(preds[i]['score'])
        else:  # TP
            all_labels.append(1)
            all_scores.append(preds[i]['score'])
    if verbose:
        print('IOU Threshold: ' + str(iou_thresh))
        print('Groundtruths ({}): '.format(len(all_labels)) + str(all_labels))
        print('Scores ({}): '.format(len(all_scores)) + str(all_scores))

    fig = plt.figure()
    if tp_fp_flag:
        fpr, tpr, roc_auc, sens_at_spec09 = calculate_roc_stats(all_labels, all_scores)
        roc_image = plot_roc(fpr, tpr, roc_auc, fig, sens_at_spec09=sens_at_spec09, tp_fp_flag=tp_fp_flag)
        plt.title('ROC_curve-tpfp-IOU_thresh={}.png'.format(iou_thresh))
        fig.savefig(os.path.join(save_dir, 'ROC_curve-tpfp-IOU_thresh={}.png'.format(iou_thresh)))
        print('ROC curve of tp/fp saved to: {}'.format(save_dir))
    else:
        Precisionr, Recallr, threshs = precision_recall_curve(all_labels, all_scores)
        FDRr = [1 - i for i in Precisionr]
        # FDRr = Precisionr # test to see

        # roc_auc = auc(FDRr, Recallr)
        roc_auc = 0.5  # will be ignored
        roc_image = plot_roc(Recallr, FDRr, roc_auc, fig, tp_fp_flag=tp_fp_flag)
        title = 'RecallFDR_curve-IOU_thresh={}.png'.format(iou_thresh)
        plt.title(title)
        fig.savefig(os.path.join(save_dir, title))
        print('{} saved to: {}'.format(title, save_dir))

    # plt.imshow(roc_image[0])
    # plt.show()


def test():
    prediction_csv_filepath = '/hdd/lee/CIMS/exp15/statistics/exp15_train_dir-genset10_20180912-113234/Predictions.csv'
    # prediction_csv_filepath = '/home/guest/PycharmProjects/cims-tfmodels/statistics/exp11_model_20180809-114452/Predictions.csv'
    # prediction_csv_filepath = '/home/guest/PycharmProjects/cims-tfmodels/statistics/exp5_model_20180809-113409/Predictions.csv'
    iou_thresh = 0.6
    tf_fp_flag = True
    # path = os.getcwd()

    path = os.path.dirname(prediction_csv_filepath)
    results_list = read_csv_to_dict(prediction_csv_filepath)
    get_roc_detection_class_agnostic(results_list, path, iou_thresh=iou_thresh, tp_fp_flag=tf_fp_flag)


def test_2():
    from eval_analysis import get_analysis_lines, get_matches_by_iou, filter_by_score

    prediction_csv_filepath = '/home/guest/PycharmProjects/cims-tfmodels/statistics/Predictions_with_filename.csv'
    # prediction_csv_filepath = '/home/guest/PycharmProjects/cims-tfmodels/statistics/exp11_model_20180809-114452/Predictions.csv'
    # prediction_csv_filepath = '/home/guest/PycharmProjects/cims-tfmodels/statistics/exp5_model_20180809-113409/Predictions.csv'
    iou_thresh = 0.1
    tp_fp_flag = True
    # path = os.getcwd()

    path = os.path.dirname(prediction_csv_filepath)
    results_list = read_csv_to_dict(prediction_csv_filepath)
    analysis_lines = get_analysis_lines(results_list)

    # filter by score=0 (no score thresh is needed for ROC)
    analysis_lines_roc = filter_by_score(analysis_lines, 0)
    # match iou by iou threshold
    preds, gts = get_matches_by_iou(analysis_lines_roc, iou_thresh)
    get_roc_detection_class_agnostic(preds, gts, path, iou_thresh=iou_thresh, tp_fp_flag=tp_fp_flag)


if __name__ == "__main__":
    test_2()
