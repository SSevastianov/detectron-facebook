import openpyxl
from openpyxl.utils.dataframe import dataframe_to_rows
import numpy as np
import pandas as pd
import os


class MetronomeReport:
    METRONOME_SHEET_NAME = 'End-to-end metrics'
    CM_SHEET_NAME = 'Detection metrics'

    def __init__(self, template_path):
        # Open an Excel workbook
        self.workbook = openpyxl.load_workbook(filename=template_path)

    def write_data_general(self, sheet_name, rows, start_row=1, start_col=1):
        if sheet_name not in self.workbook.sheetnames:
            raise ValueError('%s sheet is not available in excel' % sheet_name)
        ws = self.workbook[sheet_name]
        for r_idx, row in enumerate(rows, start_row):
            for c_idx, value in enumerate(row, start_col):
                ws.cell(row=r_idx, column=c_idx, value=value)

    def write_data_metro_sheet(self, data_list, info_box_grid_shape, num_vals_in_row=1, start_row=1,
                               start_col=1,
                               row_space=1, col_space=2):
        # info_box_grid_shape format is: (rows,cols)
        ws = self.workbook[MetronomeReport.METRONOME_SHEET_NAME]
        # check if we have something to write
        if len(data_list) == 0:
            print('Called write excel func with no data!')
            return
        num_boxes_rows, num_boxes_cols = info_box_grid_shape

        row_offset = start_row
        for box_info_row in range(num_boxes_rows):
            col_offset = start_col
            for box_info_col in range(num_boxes_cols):
                ind = box_info_row * num_boxes_cols + box_info_col
                info_box = data_list[ind]
                # first element is the title value
                ws.cell(row_offset, col_offset).value = info_box[0]
                # now fill all the other values in the info box
                n_rows_in_box = int(np.ceil((len(info_box) - 1) / float(num_vals_in_row)))
                item_counter = 1
                for r_in_box in range(n_rows_in_box):
                    for c_in_box in range(num_vals_in_row):
                        if item_counter >= len(info_box):
                            # end of info box values
                            break
                        # for each row
                        ws.cell(row_offset + r_in_box + 1, col_offset + 1 + (c_in_box * 2)).value = info_box[
                            item_counter]
                        item_counter += 1
                col_offset += col_space + 1
            # get the number of rows in the info box
            ind = box_info_row * num_boxes_cols
            info_box = data_list[ind]
            num_info_box_rows = len(info_box)
            row_offset += num_info_box_rows + row_space

    def save(self, save_path):
        self.workbook.save(filename=save_path)


def write_metronome_stats(template_xlsx_path, save_path, cm_stats, categories=None):
    # IMPORTANT NOTE: this is specific to the current format!
    met = MetronomeReport(template_xlsx_path)
    # confusion matrix
    cm = cm_stats['Confusion-Matrix']
    rows = list(dataframe_to_rows(cm))
    # set the rows/columns definition header
    rows[0][0] = 'Actual / Predicted'
    del rows[1]
    met.write_data_general(MetronomeReport.CM_SHEET_NAME, rows, start_row=2, start_col=1)

    # multi class and class agnostic
    data = cm_stats['Averages']
    data_list = [['Multi-class', data['Naive mAR:'], data['Naive mAP:']],
                 ['Class-agnostic', data['Class-Agnostic Recall:'], data['Class-Agnostic Precision:']]]
    met.write_data_metro_sheet(data_list, (1, len(data_list)), num_vals_in_row=1,
                               start_row=9, start_col=9)
    # tracker
    # TODO: replace with real values
    data_list = [['Tracker'] + list(range(11))]
    met.write_data_metro_sheet(data_list, (1, len(data_list)), num_vals_in_row=2,
                               start_row=9
                               , start_col=15)

    # cls stats
    data = cm_stats['Class']
    data_list = []
    start_row = 17
    start_col = 9
    num_cls_in_row = 3  # hard coded according to the format
    counter = 0
    all_cls_with_data = []
    for c in data.columns:
        counter += 1
        all_cls_with_data.append(c)
        cls_details = data[c]
        tp = cls_details['TP: True Positive']
        tn = cls_details['TN: True Negative']
        fp = cls_details['FP: False Positive']
        fn = cls_details['FN: False Negative']
        recall = cls_details['TPR: (Sensitivity, hit rate, recall)']
        precision = cls_details['PPV: Pos Pred Value (Precision)']
        vals = np.array([tp, tn, fp, fn, recall, precision])
        # convert nan to zeros
        mask = np.isnan(vals)
        vals[mask] = 0
        data_list.append([c] + list(vals))
        if counter == num_cls_in_row:
            # write data
            met.write_data_metro_sheet(data_list, (1, len(data_list)),
                                       start_row=start_row,
                                       start_col=start_col)
            counter = 0
            start_row += len(data_list[0]) + 1
            data_list = []

    # add the missing classes
    miss_cls = list(set(categories.values()) - set(all_cls_with_data))
    num_nan = 6  # hard coded the number of values for each class
    for c in miss_cls:
        counter += 1
        data_list.append([c] + ['nan'] * num_nan)
        if counter == num_cls_in_row:
            # write data
            met.write_data_metro_sheet(data_list, (1, len(data_list)),
                                       start_row=start_row,
                                       start_col=start_col)
            counter = 0
            start_row += len(data_list[0]) + 1
            data_list = []
    # write the rest of the cls data
    if counter != 0:
        met.write_data_metro_sheet(data_list, (1, len(data_list)),
                                   start_row=start_row,
                                   start_col=start_col)

    # (optional) write the Test configuration in metronome sheet
    pipeline_config_csv_path = os.path.join(os.path.dirname(save_path),'pipeline_config.csv')
    try:
        df_pipeline = pd.read_csv(pipeline_config_csv_path)
    except:
        df_pipeline = None
    if df_pipeline is not None:
        # add test configuration in metronome sheet
        pipeline_items_list = df_pipeline.to_numpy()
        # insert the iou matching threshold first
        pipeline_items_list = np.insert(pipeline_items_list,0,['IOU Threshold',cm_stats['Thresholds'][0]]).reshape((-1,2))
        met.write_data_general(MetronomeReport.METRONOME_SHEET_NAME,pipeline_items_list,start_row=25,start_col=2)

    met.save(save_path)
    print('Finish saving metronome')


if __name__ == '__main__':
    save_path = '/home/rocket/dict_to_excel.xlsx'
    template_xlsx_path = '/home/rocket/PycharmProjects/data_tools/data_tools/EvalTools/doc/Metronome_Report_template.xlsx'

    met = MetronomeReport(template_xlsx_path)
    val_list = [str(i) for i in range(1, 7)]
    data_list = [
        ['c1'] + val_list,
        ['c2'] + val_list,
        ['c3'] + val_list,
        ['mc', 'a', 'b'],
        ['ca', 'a', 'b'],
        ['tt', 'a', 'b']
    ]
    met.write_data_metro_sheet(data_list, (2, 3), start_row=27, start_col=8)
    met.save(save_path)
