from data_tools.MongoToTFR.reporter import Reporter
import xml.etree.ElementTree as ET
import os
import data_tools.DataManipulation.common_funcs as cf

"""
important note: when you parse xml the tags format is tags[tag_id]={xmin:20,xmax:40,label:person....}
                the xml writer should support the same format with extra fields
"""


def find_xml_dir(root_dir):
    # return the xml_dir and list of xml filenames, in case couldn't find xml dir return none
    xml_dir = root_dir.replace('/train_images', '/train_xmls')
    for curr_dir, dirs, files in os.walk(xml_dir):
        xmls_flist = [fname for fname in files if cf.get_fname_ext(fname) == 'xml']
        if xmls_flist:
            return [curr_dir, xmls_flist]
    return None


def find_xml_dir_org(root_dir):
    # return the xml_dir and list of xml filenames, in case couldn't find xml dir return none
    # xml_dir = root_dir.replace('train_images', 'train_xmls')
    for curr_dir, dirs, files in os.walk(root_dir):
        xmls_flist = [fname for fname in files if cf.get_fname_ext(fname) == 'xml']
        if xmls_flist:
            return [curr_dir, xmls_flist]
    return None


def get_xml_fname(fname):
    # replace the file extension with '.xml' and if the filename ends with
    # 'marked' it assume this is result img from data analysis
    fname_root = os.path.splitext(fname)[0]
    fname_root_list = fname_root.split('_')
    if fname_root_list[-1] == 'marked':
        fname_root = '_'.join(fname_root_list[:-1])
    return fname_root + '.' + 'xml'

def filter_xml_tags(tags, filter_dict):
    # filter_dict format is tag_key:[tag_val1,tag_val2...]
    new_tags = {}
    for tag_id, tag_dict in tags.items():
        drop_flag = False
        for tag_key, tag_val in tag_dict.items():
            if tag_key not in filter_dict.keys():
                continue
            # convert filter_dict values to strings
            str_vals = [str(val) for val in filter_dict[tag_key]]
            if str(tag_val) in str_vals:
                # need to drop this tag
                drop_flag = True
                break
        if not drop_flag:
            new_tags[tag_id] = tag_dict
    return new_tags


class XmlParser:
    # size keys
    SIZE = 'size'
    HEIGHT = 'height'
    WIDTH = 'width'
    DEPTH = 'depth'

    # vid meta keys
    # flag to indicate if frame taken from video
    FROM_VID = 'from_vid'
    ORG_FPS = 'org_fps'

    # tag dict keys
    ID_KEY = 'id'
    DETECTION_KEY = 'detection'
    COLOR_KEY = 'color'
    LABEL_KEY = 'label'
    XMIN_KEY = 'xmin'
    XMAX_KEY = 'xmax'
    YMIN_KEY = 'ymin'
    YMAX_KEY = 'ymax'
    ANGLE_KEY = 'angle'
    OCCLUSION_LEVEL_KEY = 'occlusion'
    APPEARANCE_LEVEL_KEY = 'appearance'
    VALID_KEY = 'valid'

    COLOR_ILLEGAL_VALS = ['null', 'none']
    COLOR_DEFAULT_VAL = ''
    LABEL_DEFAULT_VAL = 'empty_string'

    def __init__(self, log_dir, xml_struct):
        # create the log dir
        cf.create_folder(log_dir)
        # create the csvs
        csv_names_and_headers_dict = {
            'unfound_xmls': ['xml_path'],
            'unreadable_xmls': ['xml_path'],
            'null_tags': ['xml_path'],
            'bbox_error': ['xml_path', 'tag_id', 'problem'],
            'repeating_ids': ['xml_path', 'tag_id'],
            'unrecognized_labels': ['xml_path', 'label', 'tag_id'],
            'raw_tag_sizes': ['xml_path', 'tag_id', 'width', 'height', 'area']
        }
        self.csv_reporter = Reporter(log_dir)
        self.csv_reporter.create_csvs(csv_names_and_headers_dict)
        # dictionary that map xml parser tag dict keys to the xml tags
        self.xml_struct = xml_struct
        # run check that we have all the necessary xml struct keys
        self.test_xml_struct()

    def test_xml_struct(self):
        mandatory_keys = [XmlParser.DETECTION_KEY,
                          XmlParser.ID_KEY,
                          XmlParser.YMIN_KEY,
                          XmlParser.XMIN_KEY,
                          XmlParser.YMAX_KEY,
                          XmlParser.XMAX_KEY,
                          XmlParser.ANGLE_KEY,
                          XmlParser.LABEL_KEY,
                          XmlParser.OCCLUSION_LEVEL_KEY,
                          XmlParser.APPEARANCE_LEVEL_KEY,
                          XmlParser.COLOR_KEY]
        for key in mandatory_keys:
            if key not in self.xml_struct.keys():
                raise ValueError('Please supply all the xml_struct fields. The missing key is %s' % key)

    def test_tag_id(self, xml_path, tag_id):
        if tag_id == 'null':
            message = "xml file: {}, has a null tag"
            message_vars = [xml_path]
            self.csv_reporter.logger(message, message_vars, 'null_tags')

    def test_int_coordinates(self, bb_xmax, bb_xmin, bb_ymax, bb_ymin, xml_path, tag_id):
        if not (bb_ymin.is_integer() and bb_xmin.is_integer() and bb_ymax.is_integer() and bb_xmax.is_integer()):
            message = "xml file: {}, tag_id :{} has {}"
            message_vars = [xml_path, tag_id, 'non_integer_coordiantes']
            self.csv_reporter.logger(message, message_vars, 'bbox_error')

    def assure_unique_tag_id(self, xml_path, tags, tag_id):
        if tag_id in tags.keys():
            message = "xml file: {} contains repeating values for id: {}"
            message_vars = [xml_path, tag_id]
            self.csv_reporter.logger(message, message_vars, 'repeating_ids')
            id_count = len([key for key in tags.keys() if key.startswith(tag_id + '_')])
            # we will save the tag with a slightly different name
            tag_id = tag_id + '_' + str(id_count + 1)
        return tag_id

    def get_parsed_xml_object(self, xml_path):
        try:
            xml_obj = open(xml_path)
        except:
            message = "xml file: {} wasn't found"
            message_vars = [xml_path]
            self.csv_reporter.logger(message, message_vars, 'unfound_xmls')
            return None
        # Parse file
        try:
            doc = ET.parse(xml_obj)
        except:
            message = "parsing xml file: {} failed!"
            message_vars = [xml_path]
            self.csv_reporter.logger(message, message_vars, 'unreadable_xmls')
            return None
        return doc

    def parse_xml_img_size(self, xml_path):
        doc = self.get_parsed_xml_object(xml_path)
        if not doc:
            return None
        size = doc.find(XmlParser.SIZE)
        height = int(size.findtext(XmlParser.HEIGHT))
        width = int(size.findtext(XmlParser.WIDTH))
        depth = int(size.findtext(XmlParser.DEPTH))
        return height, width, depth

    def parse_xml_filename(self,xml_path):
        doc = self.get_parsed_xml_object(xml_path)
        if not doc:
            return None
        filename = doc.findtext(XmlWriter.FILENAME_KEY)
        return filename

    def parse_xml_vid_meta(self, xml_path):
        vid_meta = {'from_vid': 0, 'org_fps': -1}
        doc = self.get_parsed_xml_object(xml_path)
        if not doc:
            return vid_meta
        from_vid = doc.findtext(XmlParser.FROM_VID)
        if from_vid is not None:
            from_vid = int(from_vid)
            if from_vid == 1:
                vid_meta['from_vid'] = 1
        org_fps = doc.findtext(XmlParser.ORG_FPS)
        if org_fps is not None and org_fps != 'null':
            vid_meta['org_fps'] = int(float(org_fps))
        return vid_meta

    def parse_xml_tags(self, xml_path):
        # xml tags
        tags = {}
        tags_counter = 0
        doc = self.get_parsed_xml_object(xml_path)
        # check if we succeed to parse the xml
        if not doc:
            return None
        for item in doc.iterfind(self.xml_struct[XmlParser.DETECTION_KEY]):
            tag_dict = {}
            id = item.findtext(self.xml_struct[XmlParser.ID_KEY])
            if not id:
                tag_id = tags_counter
            else:
                tag_id = id
            # Getting bbox coordiantes
            try:
                bb_ymin = float(item.findtext(self.xml_struct[XmlParser.YMIN_KEY]))
                bb_xmin = float(item.findtext(self.xml_struct[XmlParser.XMIN_KEY]))
                bb_xmax = float(item.findtext(self.xml_struct[XmlParser.XMAX_KEY]))
                bb_ymax = float(item.findtext(self.xml_struct[XmlParser.YMAX_KEY]))
            except:
                message = "xml file: {}, in tag_id: {} problem: {} bbox coordinates"
                message_vars = [xml_path, tag_id, 'unreadable']
                self.csv_reporter.logger(message, message_vars, 'bbox_error')
                continue
            # angle
            bb_angle = item.findtext(self.xml_struct[XmlParser.ANGLE_KEY])
            if bb_angle is None:
                bb_angle = 0
            bb_angle = int(bb_angle)

            # bbox
            tag_dict[XmlParser.XMIN_KEY] = bb_xmin
            tag_dict[XmlParser.XMAX_KEY] = bb_xmax
            tag_dict[XmlParser.YMIN_KEY] = bb_ymin
            tag_dict[XmlParser.YMAX_KEY] = bb_ymax
            tag_dict[XmlParser.ANGLE_KEY] = bb_angle
            # occlusion level
            tag_dict[XmlParser.OCCLUSION_LEVEL_KEY] = item.findtext(self.xml_struct[XmlParser.OCCLUSION_LEVEL_KEY])
            # appearance level
            tag_dict[XmlParser.APPEARANCE_LEVEL_KEY] = item.findtext(self.xml_struct[XmlParser.APPEARANCE_LEVEL_KEY])
            # color
            tag_dict[XmlParser.COLOR_KEY] = item.findtext(self.xml_struct[XmlParser.COLOR_KEY])
            # check that we didn't get 'null' or 'none' as strings
            if tag_dict[XmlParser.COLOR_KEY] is None:
                tag_dict[XmlParser.COLOR_KEY] = XmlParser.COLOR_DEFAULT_VAL
            else:
                # convert it to lower case
                tag_dict[XmlParser.COLOR_KEY] = tag_dict[XmlParser.COLOR_KEY].lower()
                # check if color has illegal val and change it
                if tag_dict[XmlParser.COLOR_KEY] in XmlParser.COLOR_ILLEGAL_VALS:
                    tag_dict[XmlParser.COLOR_KEY] = XmlParser.COLOR_DEFAULT_VAL

            # label
            label = None
            for label_str in self.xml_struct[XmlParser.LABEL_KEY]:
                label = item.findtext(label_str)
                if label:
                    # convert it to lower case
                    label = label.lower()
                    break
            if label is None:
                label = XmlParser.LABEL_DEFAULT_VAL
            tag_dict[XmlParser.LABEL_KEY] = label

            tag_dict[XmlParser.VALID_KEY] = True

            # Test tag
            # Testing coordinates type (should be integer) and log if got illegal coordinate
            self.test_int_coordinates(bb_xmax, bb_xmin, bb_ymax, bb_ymin, xml_path, tag_id)
            # check tag_id is not null
            self.test_tag_id(xml_path, tag_id)

            # get a unique id (only change it if it wasn't unique)
            tag_id = self.assure_unique_tag_id(xml_path, tags, tag_id)

            # log the tag size
            tag_w = bb_xmax - bb_xmin
            tag_h = bb_ymax - bb_ymin
            area = tag_w * tag_h
            self.csv_reporter.report_csv([xml_path, tag_id, tag_w, tag_h, area], 'raw_tag_sizes')
            # Updating img dictionary
            tags[tag_id] = tag_dict
            # increase the tag counter so every tag will be unique
            tags_counter += 1

        return tags

    def apply_labels_exchange(self, tags, exchange_label_dict):
        # after applying this function we will set labels to the default label val for problematic labels
        for tag_id, tag in tags.items():
            label = tag[XmlParser.LABEL_KEY].lower()
            new_label = exchange_label_dict.get(label, label).lower()
            tags[tag_id][XmlParser.LABEL_KEY] = new_label

    def validate_correct_labels(self, xml_path, tags, correct_labels_list):
        # log the csv and change the valid_key field for incorrect labels
        for tag_id, tag in tags.items():
            labels = tag[XmlParser.LABEL_KEY].split(',')
            for l in labels:
                if l not in correct_labels_list:
                    # unrecognized label
                    message = "xml file: {}. Got unrecognized label: {} in tag_id: {}."
                    message_vars = [xml_path, tag[XmlParser.LABEL_KEY], tag_id]
                    self.csv_reporter.logger(message, message_vars, 'unrecognized_labels')
                    tags[tag_id][XmlParser.VALID_KEY] = False
                    break

    def split_tags_by_validation(self, tags):
        valid_tags = {}
        unvalid_tags = {}
        for tag_id, tag in tags.items():
            if tag[XmlParser.VALID_KEY]:
                # valid tag
                valid_tags[tag_id] = tag
            else:
                # unvalid tag
                unvalid_tags[tag_id] = tag
        return valid_tags, unvalid_tags

    def parse_xml_data(self, xml_path):
        xml_data = {}
        height, width, depth = self.parse_xml_img_size(xml_path)
        tags = self.parse_xml_tags(xml_path)
        filename = self.parse_xml_filename(xml_path)
        xml_data[XmlWriter.WIDTH_KEY] = width
        xml_data[XmlWriter.HEIGHT_KEY] = height
        xml_data[XmlWriter.DEPTH_KEY] = depth
        xml_data[XmlWriter.FILENAME_KEY] = filename
        xml_data[XmlWriter.TAGS_KEY] = tags
        return xml_data

class XmlWriter:
    UNKNOWN_LEVEL_VAL = '-1'
    DEFAULT_COLOR_STR = 'null'
    # keys for the xml_data dictionary used in create_xml method
    WIDTH_KEY = 'width'
    HEIGHT_KEY = 'height'
    DEPTH_KEY = 'depth'
    FILENAME_KEY = 'filename'
    TAGS_KEY = 'tags'

    def __init__(self):
        pass

    def create_xml(self, xml_path, xml_data):
        annotation = ET.Element("annotation")
        ET.SubElement(annotation, "format").text = "VOC2012"
        ET.SubElement(annotation, "segmented").text = "0"
        ET.SubElement(annotation, "path")
        size = ET.SubElement(annotation, "size")
        ET.SubElement(size, XmlWriter.WIDTH_KEY).text = str(xml_data[XmlWriter.WIDTH_KEY])
        ET.SubElement(size, XmlWriter.HEIGHT_KEY).text = str(xml_data[XmlWriter.HEIGHT_KEY])
        ET.SubElement(size, XmlWriter.DEPTH_KEY).text = str(xml_data[XmlWriter.DEPTH_KEY])
        ET.SubElement(annotation, XmlWriter.FILENAME_KEY).text = xml_data[XmlWriter.FILENAME_KEY]

        for id, tag in xml_data[XmlWriter.TAGS_KEY].items():
            object = ET.SubElement(annotation, "object")
            ET.SubElement(object, "difficult").text = str(
                tag.get(XmlParser.APPEARANCE_LEVEL_KEY, XmlWriter.UNKNOWN_LEVEL_VAL))
            ET.SubElement(object, "score").text = "0"
            vel = ET.SubElement(object, "velocity")
            ET.SubElement(vel, "x").text = "0"
            ET.SubElement(vel, "y").text = "0"
            bndbox = ET.SubElement(object, "bndbox")
            ET.SubElement(bndbox, "xmin").text = str(tag[XmlParser.XMIN_KEY])
            ET.SubElement(bndbox, "xmax").text = str(tag[XmlParser.XMAX_KEY])
            ET.SubElement(bndbox, "ymin").text = str(tag[XmlParser.YMIN_KEY])
            ET.SubElement(bndbox, "ymax").text = str(tag[XmlParser.YMAX_KEY])
            ET.SubElement(bndbox, "angle").text = str(tag[XmlParser.ANGLE_KEY])
            ET.SubElement(object, "occluded").text = str(
                tag.get(XmlParser.OCCLUSION_LEVEL_KEY, XmlWriter.UNKNOWN_LEVEL_VAL))
            ET.SubElement(object, "class").text = tag[XmlParser.LABEL_KEY]
            ET.SubElement(object, "color").text = tag.get(XmlParser.COLOR_KEY, XmlWriter.DEFAULT_COLOR_STR)
            ET.SubElement(object, "id").text = str(id)
        tree = ET.ElementTree(annotation)
        tree.write(xml_path, method='html')


if __name__ == '__main__':
    xml_path = '/hdd/python_projects/spatial-transformer-GAN-data/synt_guy/synthasized_imgs/annotations/88_poisson.xml'
    log_dir = '/hdd/istar_data/horizontal_payload_data/coaps/logs'
    xml_struct = {XmlParser.DETECTION_KEY: 'object',
                  XmlParser.ID_KEY: 'id',
                  XmlParser.YMIN_KEY: 'bndbox/ymin',
                  XmlParser.XMIN_KEY: 'bndbox/xmin',
                  XmlParser.YMAX_KEY: 'bndbox/ymax',
                  XmlParser.XMAX_KEY: 'bndbox/xmax',
                  XmlParser.ANGLE_KEY: 'bndbox/angle',
                  XmlParser.LABEL_KEY: ['class', 'type', 'name'],
                  XmlParser.OCCLUSION_LEVEL_KEY: 'occluded',
                  XmlParser.APPEARANCE_LEVEL_KEY: 'difficult',
                  XmlParser.COLOR_KEY: 'color'}
    parser = XmlParser(log_dir, xml_struct)
    writer = XmlWriter()

    data_dir='/hdd/istar_data/horizontal_payload_data/coaps/coaps_2_ir_filtered_qa'
    filter_dict = {'appearance':[2]}
    for root,dirs,files in os.walk(data_dir):
        if files and files[0].endswith('.xml'):
            # found xml dir apply filter here
            for file in files:
                xml_path = os.path.join(root,file)
                xml_data = parser.parse_xml_data(xml_path)
                xml_data['tags'] = filter_xml_tags(xml_data['tags'],filter_dict)
                writer.create_xml(xml_path, xml_data)
