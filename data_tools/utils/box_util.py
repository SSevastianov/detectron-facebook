import numpy as np


def change_xy_order(box):
    # get bbox in form of x,y,x,y or y,x,y,x and change the x,y locations
    new_box = np.array(box)[[1, 0, 3, 2]]
    return new_box


def get_box_area(box):
    # assume box in format of x1,y1,x2,y2 or y1,x1,y2,x2
    return float(max(0, box[2] - box[0]) * max(0, box[3] - box[1]))


def get_iou(boxA, boxB):
    '''
    calculates intersection over union between 2 boxes
    :param box: tl and br points of the box
    :return: iou value
    '''
    ### box template is [x1, y1, x2, y2] (also works the same for [y1, x1, y2, x2])

    ### makes sure (x1, y1) is TL and (x2, y2) is BR
    assert boxA[0] <= boxA[2]
    assert boxA[1] <= boxA[3]
    assert boxB[0] <= boxB[2]
    assert boxB[1] <= boxB[3]

    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    # compute the area of intersection rectangle
    interArea = get_box_area([xA, yA, xB, yB])

    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = get_box_area(boxA)
    boxBArea = get_box_area(boxB)

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)

    assert iou >= 0.0 and iou <= 1.0
    # return the intersection over union value
    return iou


def get_intersect_box(boxA, boxB):
    ### box template is [x1, y1, x2, y2]
    ### return the intersection box in the box template or none if there is no intersection
    ### makes sure (x1, y1) is TL and (x2, y2) is BR
    assert boxA[0] <= boxA[2]
    assert boxA[1] <= boxA[3]
    assert boxB[0] <= boxB[2]
    assert boxB[1] <= boxB[3]

    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    # compute the area of intersection rectangle
    interArea = get_box_area([xA, yA, xB, yB])
    if interArea == 0:
        return None
    return [xA, yA, xB, yB]


def get_box_overlap_precent(box, other_box):
    # calculate the precent of box that overlap with other_box
    inter_box = get_intersect_box(box, other_box)
    if inter_box is None:
        return 0.0
    return get_box_area(inter_box) / get_box_area(box)


def get_rotated_rectangle_corners(box, angle):
    ### box template is [x1, y1, x2, y2]
    ### angle in degrees
    # calculate the center of rectangle
    x1, y1, x2, y2 = box
    corners = [x1, y1, x2, y1, x2, y2, x1, y2]
    new_corners = []
    cx = int(x1 + (x2 - x1) / 2.)
    cy = int(y1 + (y2 - y1) / 2.)
    rad_angle = np.deg2rad(angle)
    for i in range(4):
        # translate points to origin
        temp_x = corners[i * 2] - cx
        temp_y = corners[i * 2 + 1] - cy
        # apply rotation
        rotated_x = int(temp_x * np.cos(rad_angle) - temp_y * np.sin(rad_angle))
        rotated_y = int(temp_x * np.sin(rad_angle) + temp_y * np.cos(rad_angle))
        # translate back
        rotated_x += cx
        rotated_y += cy
        new_corners.append([rotated_x, rotated_y])

    return np.array(new_corners, dtype=np.int32)


def get_iou(boxA, boxB):
    '''
    calculates intersection over union between 2 boxes
    :param box: tl and br points of the box
    :return: iou value
    '''
    ### box template is [x1, y1, x2, y2] (also works the same for [y1, x1, y2, x2])

    inter_box = get_intersect_box(boxA, boxB)
    if not inter_box:
        # in case of no intersection the iou is 0
        return 0.0

    # compute the area of intersection rectangle
    interArea = get_box_area(inter_box)

    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = get_box_area(boxA)
    boxBArea = get_box_area(boxB)

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)

    assert iou >= 0.0 and iou <= 1.0
    # return the intersection over union value
    return iou


def nms(boxes, idxs, iou_th):
    # assume last index in idxs has the highest priority
    # if there are no boxes, return an empty list
    if len(boxes) == 0:
        return []

    # initialize the list of picked indexes
    pick = []

    # keep looping while some indexes still remain in the indexes
    # list
    while len(idxs) > 0:
        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)
        other_boxes = boxes[idxs[:last]]
        ious = np.array([get_iou(boxes[i], b) for b in other_boxes])
        # delete all indexes from the index list that have
        idxs = np.delete(idxs, np.concatenate(([last],
                                               np.where(ious > iou_th)[0])))
    pick = np.array(pick)
    return pick
