from scipy.ndimage import rotate
import scipy.misc as mi
import os
import cv2
from PIL import Image
import data_tools.DataManipulation.common_funcs as cf
import numpy as np
import re

tif_ext = ['tif', 'tiff']
jpg_ext = 'jpg'
DEFAULT_IMG_EXT = '.' + jpg_ext
IMAGE_TYPES = ['jpeg', 'png', 'bmp'] + [jpg_ext] + tif_ext


def get_img_size_and_depth(img_path=None, img=None):
    # TODO: maybe change for a faster alternative code
    if img_path and not cf.is_pic(img_path):
        # not an img
        return None
    if img is None:
        img = cv2.imread(img_path)
    shape = np.shape(img)
    if len(shape) == 2:
        return shape, 1
    elif len(shape) == 3:
        return shape
    else:
        raise ValueError('wrong image size in %s. got shape: %s' % (img_path, str(shape)))

def get_img_files_list(dir_path):
    pic_list = [file for file in os.listdir(dir_path) if cf.is_pic(os.path.join(dir_path,file))]
    return pic_list

def get_frame_numbers_from_names_list(frame_names_list):
    # convert to list if input is string
    if type(frame_names_list) == str:
        frame_names_list = [frame_names_list]
    # get the last number in the frame name
    frame_num_list = [int(re.findall(r'\d+', frame_num_str)[-1]) for frame_num_str in frame_names_list]
    return frame_num_list


def get_img_range_without_black_border(img, border_val=0):
    """
    This func will return the start and end offsets for y and x axis of the img without the black border
    For example, if border_val=20 then every pixel which doesn't have value above 20 will not include in the img range
    :param img: The image
    :param border_val: Maximum pixel value of the black border
    :return: y and x ranges. Ranges are in [,) format (exclude the end)
    """
    h, w, _ = get_img_size_and_depth(img=img)
    y_list, x_list, c_list = np.where(img > border_val)
    if len(y_list) == 0:
        raise ValueError('image has only border pixels!')
    # the +1 is for excluding the end point for each axis
    return (min(y_list), max(y_list) + 1), (min(x_list), max(x_list) + 1)


def rotate_img(in_file_path, out_file_path, deg=-90):
    if not cf.is_pic(in_file_path):
        # file is not an image
        print('file: ' + in_file_path + ' is not an image. abort')
        return
    img = mi.imread(in_file_path)
    rot_img = rotate(img, deg)
    mi.imsave(out_file_path, rot_img)


def split_img(in_file_path, out_file_path, num_hor_slices=1, num_vert_slices=0, delete_origin=False):
    img = cv2.imread(in_file_path)
    height, width, depth = img.shape
    rows = num_hor_slices + 1
    cols = num_vert_slices + 1
    row_slice_size = int(height / float(rows))
    col_slice_size = int(width / float(cols))
    for i in range(rows):
        for j in range(cols):
            img_num = i * cols + j
            start_height = i * row_slice_size
            end_height = start_height + row_slice_size
            start_width = j * col_slice_size
            end_width = start_width + col_slice_size
            sub_img = img[start_height:end_height, start_width:end_width, :]
            img_name = os.path.basename(out_file_path)
            new_img_name = img_name.replace('.', '_' + str(img_num) + '.')
            new_img_path = out_file_path.replace(img_name, new_img_name)
            cv2.imwrite(new_img_path, sub_img)
    if delete_origin:
        os.remove(in_file_path)


def convert_tif_to_jpg(in_file_path, out_file_path):
    ext = cf.get_fname_ext(in_file_path)
    if ext in tif_ext:
        out_file_path = out_file_path.replace(ext, jpg_ext)
        if os.path.isfile(out_file_path):
            print("A jpeg file already exists for %s" % out_file_path)
        # If a jpeg is *NOT* present, create one from the tiff.
        else:
            try:
                im = Image.open(in_file_path)
                print("Generating jpeg for %s" % in_file_path)
                im.thumbnail(im.size)
                im.save(out_file_path, "JPEG", quality=100)
            except Exception as e:
                try:
                    im.mode = 'I'
                    im.point(lambda i: i * (1. / 256)).convert('L').save(out_file_path)
                except Exception as er:
                    print('Got exception: %s. After trying to convert a 16 bit, got exception: %s' % (e, er))


# [103.939, 116.779, 123.68] imagenet average
def pad_img(in_file_path, out_file_path, dst_height=1080, dst_width=1920, pad_color_value=[103.939, 116.779, 123.68]):
    if not cf.is_pic(in_file_path):
        # file is not an image
        print('file: ' + in_file_path + ' is not an image. abort padding')
        return
    img = cv2.imread(in_file_path)
    height, width, depth = get_img_size_and_depth(img=img)
    diff_y = dst_height - height
    if diff_y < 0:
        raise ValueError(
            'Failed padding image: %s. img height: %d , dst img height: %d' % (in_file_path, height, dst_height))
    diff_x = dst_width - width
    if diff_x < 0:
        raise ValueError(
            'Failed padding image: %s. img width: %d , dst img width: %d' % (in_file_path, width, dst_width))
    if diff_y == 0 and diff_x == 0:
        # no need to change the image size
        print("no need to change image: %s" % in_file_path)

    if diff_y % 2. == 0:
        # even
        top = bottom = diff_y / 2
    else:
        # odd
        top = diff_y / 2
        bottom = top + 1
    if diff_x % 2. == 0:
        # even
        left = right = diff_x / 2
    else:
        # odd
        left = diff_x / 2
        right = left + 1
    pad_image = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=pad_color_value)
    # save the padded image
    cv2.imwrite(out_file_path, pad_image)


def crop_img(in_file_path, out_file_path, from_h=840, to_h=1920, from_w=0, to_w=1080):
    if not cf.is_pic(in_file_path):
        # file is not an image
        print('file: ' + in_file_path + ' is not an image. abort')
        return
    im = Image.open(in_file_path)
    im.crop((from_w, from_h, to_w, to_h)).save(out_file_path)

def is_grey_scale(img_path, max_diff=5, percent_th=0.5):
    """
    Check if img is grey scale or rgb
    :param img_path: Image full path
    :param max_diff: Usually in grey scale image we assume all channels have the same value.
    In practice they might have slightly different values. In order to handle this,
    we define the maximum difference between channel's value to consider them the same.
    :param percent_th: For significance difference between the number of grey and color pixels, we calculate
    p=lower_counter/higher_counter and check it doesn't pass the precent_th.
    :return: True for grey scale img, False for color img or None(!!!) for unknown img
    """
    img = Image.open(img_path).convert('RGB')
    # Important to convert to int32 so we can do the diff safely
    img = np.array(img).astype(np.int32)
    r_g = np.abs(img[:,:,0]-img[:,:,1])<=max_diff
    r_b = np.abs(img[:,:,0]-img[:,:,2])<=max_diff
    g_b = np.abs(img[:,:,1]-img[:,:,2])<=max_diff

    is_same_pix_val = r_g & r_b & g_b
    grey_counter = np.where(is_same_pix_val==True)[0].shape[0]
    color_counter = np.where(is_same_pix_val==False)[0].shape[0]

    # for debug
    # print('grey pixels: %d'%grey_counter)
    # print('color pixels: %d'%color_counter)
    if color_counter > grey_counter and grey_counter / float(color_counter) < percent_th:
        # Color
        return False
    elif color_counter / float(grey_counter) < percent_th:
        # Grey
        return True
    else:
        # Unknown
        return None


if __name__ == '__main__':
    data_path = '/hdd/datasets/FLIR_ADAS/video/Data'
    out_path = '/hdd/datasets/FLIR_ADAS/video/Data_guy'
    cf.manipulate_files(data_path, out_path, [convert_tif_to_jpg])
