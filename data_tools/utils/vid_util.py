import data_tools.DataManipulation.common_funcs as cf
import data_tools.utils.img_util as iu
import cv2
import subprocess
from MediaInfo import MediaInfo
import os

VIDEO_TYPES = ['avi', 'mp4', 'mkv', 'mov']


def convert_vid_codec(src_vid_path, dst_vid_path, codec='libx264'):
    clean_flag = False
    if src_vid_path == dst_vid_path:
        parent_dir, vid_name = os.path.split(src_vid_path)
        dst_dir = os.path.join(parent_dir, 'tmp_vid')
        cf.create_folder(dst_dir)
        dst_vid_path = os.path.join(dst_dir, vid_name)
        clean_flag = True
    bashCommand = "ffmpeg -i %s -vcodec %s %s" % (src_vid_path, codec, dst_vid_path)
    process = subprocess.Popen(bashCommand.split(), stdout=None)
    process.wait()
    if clean_flag:
        os.remove(src_vid_path)
        os.rename(dst_vid_path, src_vid_path)
        os.rmdir(dst_dir)


def get_ffmpeg_stats(vid_path):
    vid_stats = {}
    # mediainfo, assume the backend is ffmpeg
    media = MediaInfo(filename=vid_path)
    media_info = media.getInfo()
    vid_stats['codec_profile'] = str(media_info['videoCodecProfile'])
    vid_stats['codec'] = str(media_info['videoCodec'])
    # parse the fps in the format x/y
    fps_comp = [float(i) for i in media_info['videoFrameRate'].split('/')]
    # calculate the fps
    vid_stats['fps'] = fps_comp[0] / fps_comp[1]
    vid_stats['num_frames'] = int(media_info['videoFrameCount'])
    vid_stats['duration'] = int(float(media_info['videoDuration']))
    return vid_stats


def get_cv2_stats(vid_path):
    vid_stats = {}
    # cv2
    vid = cv2.VideoCapture(vid_path)
    vid_stats['fps'] = vid.get(cv2.CAP_PROP_FPS)
    vid_stats['num_frames'] = int(vid.get(cv2.CAP_PROP_FRAME_COUNT))
    # the duration is in seconds
    vid_stats['duration'] = int(vid_stats['num_frames'] / vid_stats['fps'])
    # get the video resolution according to the first frame (assume the video resolution doesn't change)
    _, img = vid.read()
    if img is None:
        print('Problem in reading video: %s' % vid_path)
        return None
    height, width, depth = iu.get_img_size_and_depth(img=img)
    vid_stats['width'] = int(width)
    vid_stats['height'] = int(height)
    vid_stats['depth'] = int(depth)
    return vid_stats


def get_vid_stats(vid_path, use_ffmpeg=False):
    if not cf.is_video(vid_path):
        print(vid_path + ': is not a video!')
        return None
    vid_stats = {}
    cv2_stats = get_cv2_stats(vid_path)
    # cv2 values
    vid_stats['width'] = cv2_stats['width']
    vid_stats['height'] = cv2_stats['height']
    vid_stats['depth'] = cv2_stats['depth']
    vid_stats['fps'] = cv2_stats['fps']
    vid_stats['num_frames'] = cv2_stats['num_frames']
    vid_stats['duration'] = cv2_stats['duration']

    if use_ffmpeg:
        ffmpeg_stats = get_ffmpeg_stats(vid_path)
        # mediainfo values
        vid_stats['codec_profile'] = ffmpeg_stats['codec_profile']
        vid_stats['codec'] = ffmpeg_stats['codec']
        # common values
        vid_stats['fps'] = ffmpeg_stats['fps']
        vid_stats['num_frames'] = ffmpeg_stats['num_frames']
        vid_stats['duration'] = ffmpeg_stats['duration']

    return vid_stats


if __name__ == '__main__':
    vid_path = '/hdd/istar_data/carmel/videos/olympus/first_iter/vehicle_road_driving_vid_7.mp4'
    vid_path2 = '/hdd/istar_data/carmel/videos/29_1_19_vid_3_fix.mp4'
    s = get_vid_stats(vid_path)
    print(s)
    # convert_vid_codec(vid_path, vid_path2)
