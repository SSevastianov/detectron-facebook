# Some basic setup
# Setup detectron2 logger
from detectron2.engine import DefaultPredictor
from detectron2.utils.logger import setup_logger
setup_logger()
import pandas as pd
# import some common libraries
import cv2
import torch
import numpy as np
import matplotlib.pyplot as plt
import os
# import some common detectron2 utilities
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog
import numpy as np
import imageio





person_width_ratio = [0.155, 0.66]
car_width_ratio = [0.65, 2.65]

def imread(fname):
	return imageio.imread(fname)/255.0

def imsave(fname,array):
	imageio.imsave(fname,array.astype(np.uint8))

def get_annotation_from_mask_file(mask_file, scale=1.0, normalized_mask= False, inverted_mask=False):
  '''Given a mask file and scale, return the bounding box annotations

  Args:
      mask_file(string): Path of the mask file
  Returns:
      tuple: Bounding box annotation (xmin, xmax, ymin, ymax)
  '''
  mask = mask_file
  if inverted_mask:
    mask = 255 - mask
  if normalized_mask:
    mask = mask*255
  rows = np.any(mask, axis=1)
  cols = np.any(mask, axis=0)
  if len(np.where(rows)[0]) > 0:
    ymin, ymax = np.where(rows)[0][[0, -1]]
    xmin, xmax = np.where(cols)[0][[0, -1]]
    return int(scale * xmin), int(scale * xmax), int(scale * ymin), int(scale * ymax)
  else:
    return -1, -1, -1, -1

def filter_masks_dict(masks_dict, L, R):
    filtered_masks_dict = {}
    count = 0
    for i in range(len(masks_dict)):
      mask = masks_dict[str(i)]
      xmin, xmax, ymin, ymax = get_annotation_from_mask_file(mask)
      width = xmax - xmin
      height = ymax - ymin
      if L <= width/height <= R:
        filtered_masks_dict[count] = mask
        count += 1
    return filtered_masks_dict

def main():

  FILTERING_OBJECT = 'person'
  dir_path = '/hdd/Detectron/segmentations_mask_rcnn_R_101_FPN_3x/NEW_TF_segmentations_mask_rcnn_R_101_FPN_3x/VIS/{}'.format(FILTERING_OBJECT)

  just_copy = True
  count = 0
  # all_person_backgrounds_special = [os.path.join(dir_path, '{}'.format(i)) for i in os.listdir(dir_path) if'Blackmagick'in i ]
  all_person_backgrounds = sorted([os.path.join(dir_path, '{}'.format(i)) for i in os.listdir(dir_path) if '.pbm' not in i])
  # all_person_backgrounds = [i for i in all_person_backgrounds_special if 'Blackmagic' in i]

  new_masks_path ='/hdd/Detectron/segmentations_mask_rcnn_R_101_FPN_3x/NEW_TF_segmentations_mask_rcnn_R_101_FPN_3x/VIS/For_G_Drive_small_parts/{}'.format(FILTERING_OBJECT)
  num_of_segments_in_each_dir = 5000
  if not os.path.exists(new_masks_path):
    for i in range(len(all_person_backgrounds)//num_of_segments_in_each_dir + 1):
      os.makedirs(new_masks_path + '/{}'.format(i))


  for path in all_person_backgrounds:
    mask_path = path.replace('.jpg', '.pbm')
    count += 1
    print('frame number: ', count)
    frame = cv2.imread(path)

    mask = cv2.imread(mask_path)

    if just_copy:
      if mask is not None and frame is not None:
        new_image_path = path.replace(dir_path, new_masks_path + '/{}'.format(count//num_of_segments_in_each_dir))
        new_mask_path = new_image_path.replace('.jpg', '.jpeg')
        cv2.imwrite(new_image_path, frame)
        imsave(new_mask_path, mask)


if __name__ == '__main__':
    main()