# Some basic setup
# Setup detectron2 logger
from detectron2.engine import DefaultPredictor
from detectron2.utils.logger import setup_logger
setup_logger()
import pandas as pd
# import some common libraries
import cv2
import torch
import numpy as np
import matplotlib.pyplot as plt
import os
# import some common detectron2 utilities
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog
from glob import glob
from data_tools.utils.xml_util import XmlWriter, XmlParser, get_xml_fname, find_xml_dir
import data_tools.DataManipulation.common_funcs as cf



torch.cuda.set_device(0)
# yaml_path = "/hdd/detectron_installation/detectron2/configs/COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"
yaml_path = "/hdd/detectron_installation/detectron2/configs/COCO-InstanceSegmentation/mask_rcnn_R_101_FPN_3x.yaml"
cfg = get_cfg()
cfg.merge_from_file(yaml_path)
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.9  # set threshold for this model
# cfg.MODEL.WEIGHTS = "detectron2://COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x/137849600/model_final_f10217.pkl"
cfg.MODEL.WEIGHTS = "detectron2://COCO-InstanceSegmentation/mask_rcnn_R_101_FPN_3x/138205316/model_final_a3ec72.pkl"

predictor = DefaultPredictor(cfg)


LOG_DIR = '/hdd/python_projects/spatial-transformer-GAN-data/synt_guy/logs'
XML_STRUCT = {XmlParser.DETECTION_KEY: 'object',
              XmlParser.ID_KEY: 'id',
              XmlParser.YMIN_KEY: 'bndbox/ymin',
              XmlParser.XMIN_KEY: 'bndbox/xmin',
              XmlParser.YMAX_KEY: 'bndbox/ymax',
              XmlParser.XMAX_KEY: 'bndbox/xmax',
              XmlParser.ANGLE_KEY: 'bndbox/angle',
              XmlParser.LABEL_KEY: ['class', 'type', 'name'],
              XmlParser.OCCLUSION_LEVEL_KEY: 'occluded',
              XmlParser.APPEARANCE_LEVEL_KEY: 'difficult',
              XmlParser.COLOR_KEY: 'color'}

xml_parser = XmlParser(LOG_DIR, XML_STRUCT)
xml_writer = XmlWriter()


def bool_to_mask(masks_dict, combine_masks_to_one=False):
  person_masks = masks_dict['person']
  car_masks = masks_dict['vehicle']
  motorcycle_masks = masks_dict['motorcycle']
  car_masks_dict = {}
  person_masks_dict = {}
  motorcycle_masks_dict = {}

  if len(person_masks) > 0:
    person_combined_mask = np.zeros([masks_dict['person'][0].shape[0], masks_dict['person'][0].shape[1]])
    for i in range(len(person_masks)):
      bool_mask = np.asarray(masks_dict['person'][i])
      ones_mask = np.ones([bool_mask.shape[0], bool_mask.shape[1]])
      mask = ones_mask*bool_mask*255.0
      person_combined_mask = person_combined_mask + mask
      person_masks_dict[str(i)] = mask
  else:
    person_combined_mask = None
    person_masks_dict = None

  if len(car_masks) > 0:
    car_combined_mask = np.zeros([masks_dict['vehicle'][0].shape[0], masks_dict['vehicle'][0].shape[1]])
    for i in range(len(car_masks)):
      bool_mask = np.asarray(masks_dict['vehicle'][i])
      ones_mask = np.ones([bool_mask.shape[0], bool_mask.shape[1]])
      mask = ones_mask*bool_mask*255.0
      car_combined_mask = car_combined_mask + mask
      car_masks_dict[str(i)] = mask
  else:
    car_combined_mask = None
    car_masks_dict = None

  if len(motorcycle_masks) > 0:
    motorcycle_combined_mask = np.zeros([masks_dict['motorcycle'][0].shape[0], masks_dict['motorcycle'][0].shape[1]])
    for i in range(len(motorcycle_masks)):
      bool_mask = np.asarray(masks_dict['motorcycle'][i])
      ones_mask = np.ones([bool_mask.shape[0], bool_mask.shape[1]])
      mask = ones_mask*bool_mask*255.0
      motorcycle_combined_mask = motorcycle_combined_mask + mask
      motorcycle_masks_dict[str(i)] = mask
  else:
    motorcycle_combined_mask = None
    motorcycle_masks_dict = None

  if combine_masks_to_one:
    return person_combined_mask, car_combined_mask, motorcycle_combined_mask
  else: return person_masks_dict, car_masks_dict, motorcycle_masks_dict

def filter_by_classes(outputs, classes=['car', 'bus', 'truck']):
    metadata = MetadataCatalog.get(cfg.DATASETS.TRAIN[0])
    vehicle_ids = np.where(np.isin(metadata.thing_classes, np.array(classes)))[0]
    x = outputs['instances'].pred_classes.cpu()
    filtered_ids = np.where(np.isin(x, vehicle_ids))[0]
    return outputs['instances'][filtered_ids]


def predict(frame):
    outputs = predictor(frame)
    vehicles_outputs = filter_by_classes(outputs, ['car', 'bus', 'truck'])
    motorcycle_outputs = filter_by_classes(outputs, ['motorcycle'])
    people_outputs = filter_by_classes(outputs, ['person'])

    bbox_dict = {
        'vehicle': vehicles_outputs.pred_boxes.tensor.cpu(),
        'person': people_outputs.pred_boxes.tensor.cpu(),
        'motorcycle': motorcycle_outputs.pred_boxes.tensor.cpu()
    }

    masks_dict = {
      'vehicle': vehicles_outputs.pred_masks.cpu(),
      'person': people_outputs.pred_masks.cpu(),
      'motorcycle': motorcycle_outputs.pred_masks.cpu()
    }
    return outputs, bbox_dict, masks_dict

def get_annotation_from_mask_file(mask_file, scale=1.0, normalized_mask= False, inverted_mask=False):
  '''Given a mask file and scale, return the bounding box annotations

  Args:
      mask_file(string): Path of the mask file
  Returns:
      tuple: Bounding box annotation (xmin, xmax, ymin, ymax)
  '''
  mask = mask_file
  if inverted_mask:
    mask = 255 - mask
  if normalized_mask:
    mask = mask*255
  rows = np.any(mask, axis=1)
  cols = np.any(mask, axis=0)
  if len(np.where(rows)[0]) > 0:
    ymin, ymax = np.where(rows)[0][[0, -1]]
    xmin, xmax = np.where(cols)[0][[0, -1]]
    return int(scale * xmin), int(scale * xmax), int(scale * ymin), int(scale * ymax)
  else:
    return -1, -1, -1, -1

def filter_masks(masks_dict, L, R):
    filtered_masks_dict = {}
    count = 0
    for i in range(len(masks_dict)):
      mask = masks_dict[str(i)]
      xmin, xmax, ymin, ymax = get_annotation_from_mask_file(mask)
      width = xmax - xmin
      height = ymax - ymin

      if L <= width/height <= R:
        filtered_masks_dict[count] = mask
        count += 1

    return filtered_masks_dict


def get_background_img_paths(BACKGROUND_DIR_LIST, BACKGROUND_GLOB_STRING, BLACK_LIST, check_if_in_black_list=False):
  background_files = []
  for bg_root in BACKGROUND_DIR_LIST:
    for root, dirs, files in os.walk(bg_root, topdown=True):
      # check if folder has files
      if not files:
        continue
      # check if folder is in the black list
      if check_if_in_black_list:
        skip_flag = False
        for black_dir in BLACK_LIST:
          if root.endswith(black_dir):
            skip_flag = True
            break
        if skip_flag:
          continue
      # check if files are relevant
      relevant_files = glob(root + '/' + BACKGROUND_GLOB_STRING)
      if relevant_files:
        background_files += relevant_files
  return background_files

def get_tags(bg_file_path):
  dir, fname = os.path.split(bg_file_path)
  xml_dir_path = find_xml_dir(dir)[0]
  xml_fname = get_xml_fname(fname)
  xml_path = os.path.join(xml_dir_path, xml_fname)
  tags = xml_parser.parse_xml_tags(xml_path)
  return tags

def find_xml_dir(root_dir):
    # return the xml_dir and list of xml filenames, in case couldn't find xml dir return none
    xml_dir = root_dir.replace('/train_images', '/train_xmls')
    for curr_dir, dirs, files in os.walk(xml_dir):
        xmls_flist = [fname for fname in files if cf.get_fname_ext(fname) == 'xml']
        if xmls_flist:
            return [curr_dir, xmls_flist]
    return None


def get_xml_fname(fname):
  # replace the file extension with '.xml' and if the filename ends with
  # 'marked' it assume this is result img from data analysis
  fname_root = os.path.splitext(fname)[0]
  fname_root_list = fname_root.split('_')
  if fname_root_list[-1] == 'marked':
    fname_root = '_'.join(fname_root_list[:-1])
  return fname_root + '.' + 'xml'

def main():
  channel = 'VIS'
  '''
  path to directory containing all desired_inference iages, all the tags need to be in 'train_xmls' folder with same structure
  '''
  path = '/hdd/noga_data_new/vis/train_images'
  BACKGROUND_GLOB_STRING = '*.jpg'
  BACKGROUND_DIR_LIST = [os.path.join(path, i) for i in os.listdir(path)]
  BLACK_LIST = ['']

  background_files = get_background_img_paths(BACKGROUND_DIR_LIST, BACKGROUND_GLOB_STRING, BLACK_LIST)
  print("Number of background images : %s" % len(background_files))
  for index, bg_file in enumerate(background_files):
    print(index)
    tags_list = []
    tags = get_tags(bg_file)
    image = cv2.imread(bg_file)
    for tag in tags.values():
        tags_list.append(
            [tag[XmlParser.XMIN_KEY], tag[XmlParser.YMIN_KEY], tag[XmlParser.XMAX_KEY], tag[XmlParser.YMAX_KEY]])

    for n in range(len(tags_list)):
      bbox = tags_list[n]
      image_crop = image[max(int(bbox[1]*0.85), 0):min(int(bbox[3]*1.15), image.shape[0]), max(int(bbox[0]*0.85), 0):min(int(bbox[2]*1.15), image.shape[1])]
      # cv2.imshow('df', image_crop)
      # cv2.waitKey(0)

      frame = image_crop

      outputs, bbox_dict, masks_dict = predict(frame)

      v = Visualizer(frame[:, :, ::-1], MetadataCatalog.get(cfg.DATASETS.TRAIN[0]), scale=1.2)
      v = v.draw_instance_predictions(outputs["instances"].to("cpu"))
      final_img = v.get_image()[:, :, ::-1]
      output_path = '/hdd/Detectron/segmentations_mask_rcnn_R_101_FPN_3x/check_{}/{}'.format(yaml_path.split('/')[-1].split('.')[0], channel)
      person_segmentation_path = os.path.join(output_path, 'person')
      car_segmentation_path = os.path.join(output_path, 'car')
      motorcycle_segmentation_path = os.path.join(output_path, 'animal')

      if not os.path.exists(person_segmentation_path):
        os.makedirs(person_segmentation_path)
      if not os.path.exists(car_segmentation_path):
        os.makedirs(car_segmentation_path)
      if not os.path.exists(motorcycle_segmentation_path):
        os.makedirs(motorcycle_segmentation_path)

      person_masks, vehicle_masks, motorcycle_masks = bool_to_mask(masks_dict)


      if person_masks is not None:
        # cv2.imshow('person_mask', person_mask)
        for m in range(len(person_masks)):
          mask = person_masks[str(m)]
          image_path = os.path.join(person_segmentation_path, '{}{}'.format(n, m) + bg_file.split('/')[-2] + bg_file.split('/')[-1])
          mask_path = image_path.replace('.jpg', '.jpeg')
          cv2.imwrite(image_path, frame)
          cv2.imwrite(mask_path, mask)

      if vehicle_masks is not None:
        # cv2.imshow('car_mask', vehicle_mask)
        for m in range(len(vehicle_masks)):
          mask = vehicle_masks[str(m)]
          image_path = os.path.join(car_segmentation_path, '{}{}'.format(n,m) + bg_file.split('/')[-2] + bg_file.split('/')[-1])
          mask_path = image_path.replace('.jpg', '.jpeg')
          cv2.imwrite(image_path, frame)
          cv2.imwrite(mask_path, mask)


      if motorcycle_masks is not None:
        # cv2.imshow('car_mask', vehicle_mask)
        for m in range(len(motorcycle_masks)):
          mask = motorcycle_masks[str(m)]
          image_path = os.path.join(motorcycle_segmentation_path, '{}{}'.format(n,m) + bg_file.split('/')[-2] + bg_file.split('/')[-1])
          mask_path = image_path.replace('.jpg', '.jpeg')
          cv2.imwrite(image_path, frame)
          cv2.imwrite(mask_path, mask)

        # cv2.imshow('image', final_img)
        # cv2.waitKey(0)


main()