# Some basic setup
# Setup detectron2 logger
from detectron2.engine import DefaultPredictor
from detectron2.utils.logger import setup_logger
setup_logger()
import pandas as pd
# import some common libraries
import cv2
import torch
import numpy as np
import matplotlib.pyplot as plt
import os
# import some common detectron2 utilities
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog

torch.cuda.set_device(0)

# yaml_path = "/hdd/detectron_installation/detectron2/configs/COCO-Detection/retinanet_R_101_FPN_3x.yaml"
# yaml_path = "/hdd/detectron_installation/detectron2/configs/COCO-Detection/faster_rcnn_R_101_FPN_3x.yaml"
yaml_path = "/hdd/detectron_installation/detectron2/configs/COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x.yaml"
cfg = get_cfg()
cfg.merge_from_file(yaml_path)
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5  # set threshold for this model
# cfg.MODEL.WEIGHTS = "https://dl.fbaipublicfiles.com/detectron2/COCO-Detection/faster_rcnn_R_101_FPN_3x/137851257/model_final_f6e8b1.pkl"
cfg.MODEL.WEIGHTS = "https://dl.fbaipublicfiles.com/detectron2/COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x/139173657/model_final_68b088.pkl"

predictor = DefaultPredictor(cfg)


def filter_by_classes(outputs, classes=['car', 'bus', 'truck', 'motorcycle']):
    metadata = MetadataCatalog.get(cfg.DATASETS.TRAIN[0])
    vehicle_ids = np.where(np.isin(metadata.thing_classes, np.array(classes)))[0]
    x = outputs['instances'].pred_classes.cpu()
    filtered_ids = np.where(np.isin(x, vehicle_ids))[0]
    return outputs['instances'][filtered_ids]


def predict(frame):
    outputs = predictor(frame)
    vehicles_outputs = filter_by_classes(outputs, ['car', 'bus', 'truck', 'motorcycle'])
    people_outputs = filter_by_classes(outputs, ['person'])

    bbox_dict = {
        'vehicle': vehicles_outputs.pred_boxes.tensor.cpu(),
        'person': people_outputs.pred_boxes.tensor.cpu()
    }

    return outputs, bbox_dict

def get_annotation_from_mask_file(mask_file, scale=1.0, normalized_mask= False, inverted_mask=False):
  '''Given a mask file and scale, return the bounding box annotations

  Args:
      mask_file(string): Path of the mask file
  Returns:
      tuple: Bounding box annotation (xmin, xmax, ymin, ymax)
  '''
  mask = mask_file
  if inverted_mask:
    mask = 255 - mask
  if normalized_mask:
    mask = mask*255
  rows = np.any(mask, axis=1)
  cols = np.any(mask, axis=0)
  if len(np.where(rows)[0]) > 0:
    ymin, ymax = np.where(rows)[0][[0, -1]]
    xmin, xmax = np.where(cols)[0][[0, -1]]
    return int(scale * xmin), int(scale * xmax), int(scale * ymin), int(scale * ymax)
  else:
    return -1, -1, -1, -1

def filter_masks(masks_dict, L, R):
    filtered_masks_dict = {}
    count = 0
    for i in range(len(masks_dict)):
      mask = masks_dict[str(i)]
      xmin, xmax, ymin, ymax = get_annotation_from_mask_file(mask)
      width = xmax - xmin
      height = ymax - ymin

      if L <= width/height <= R:
        filtered_masks_dict[count] = mask
        count += 1

    return filtered_masks_dict





def main():
  #     cap = cv2.VideoCapture('/home/work/allVid/PEDESTRIANS_11_13.25-13.45.avi.1')
  path = '/hdd/flir_repair/val'
  count = 0
  channel = 'VIS'
  color = (0, 0, 255)
  all_frame_paths = [os.path.join(path,'{}'.format(i)) for i in os.listdir(path)]




  for index, img_path in enumerate(sorted(all_frame_paths)):
    frame = cv2.imread(img_path)
    outputs, bbox_dict = predict(frame)
    # print (bbox_dict)
    # for key in bbox_dict.keys():
    #   all_key_bboxes = bbox_dict[str(key)]
    #   for bbox in all_key_bboxes:
    #     bbox = np.asarray(bbox)
    #     x = bbox[0]
    #     x2 = bbox[2]
    #     y = bbox[1]
    #     y2 = bbox[3]
    #     int_color = tuple([int(x) for x in color])
    #     image = cv2.rectangle(frame, (int(x), int(y)), (int((x2)), int(y2)), int_color, 2)
      # car_label = result['label'][n]
      # car_label =3
      # color_label = result['detection_score'][n]
      # cv2.putText(image, "{} {}".format(color_label, car_label), (int(x[n]), int(y[n])), 1, color=int_color, fontScale=1.4,
      #           thickness=2)

    v = Visualizer(frame[:, :, ::-1], MetadataCatalog.get(cfg.DATASETS.TRAIN[0]), scale=1.2)
    v = v.draw_instance_predictions(outputs["instances"].to("cpu"))
    final_img = v.get_image()[:, :, ::-1]

    cv2.imshow("yad2-detections", final_img)
    cv2.waitKey(0)



main()